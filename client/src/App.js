import React, { Suspense, Fragment, lazy, useState, useContext, useEffect } from 'react';

import { BrowserRouter as Router, Route, Switch, useHistory, useLocation, withRouter } from "react-router-dom";
// import { icon } from '@material-ui/icons';
// import Drawer from './components/Drawer';
import MiniD from './components/MiniDrawer';
import ProtectedRoute from './components/ProtectedRoute';
import LoginProtected from './components/LoginProtected';
import routes from './components/Routes/Routes';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SocketContext, { socket, socketAuth } from './components/utils/SocketConnection';
import { BackContext } from './components/utils/LocationContext';
import { useCallback } from 'react';
// import Users from './components/Users';
// import Ads from './components/Ads';
// const Users = lazy(() => import("./components/Users"));
// const Ads = lazy(() => import("./components/Ads"));
//import BeforeLoginComponent from './components/Login';
//import LoggedInComponent from './components/Dashboard';
const BeforeLoginComponent = lazy(() => import("./components/Login"));
//const LoggedInComponent = lazy(() => import("./components/Dashboard"));
//const BrowserHistory = require('react-router-dom')

function App() {

  const context = useContext(BackContext)

  const history = useHistory()

  const [locationKeys, setLocationKeys] = useState([])

  //  useEffect(() => {
  //   console.log('App history Value Before Pop', history);
  //   setLocationKeys([history.location.pathname])
  //  }
  // , [locationKeys])

  // useEffect(() => {
  //   return history.listen(location => {

  //     console.log('App history Valu', location)
  //     if (history.action === 'PUSH') {
  //       setLocationKeys([ location.key ])
  //     }

  //     if (history.action === 'POP') {
  //       if (locationKeys[1] === location.key) {
  //         setLocationKeys(([ _, ...keys ]) => keys)

  //         // Handle forward event

  //       } else {
  //         setLocationKeys((keys) => [ location.key, ...keys ])

  //         // Handle back event

  //       }
  //     }
  //   })
  // }, [ locationKeys, ])


  const Back = useCallback((e) => {
    e.preventDefault()
    console.log('App Back Work ******* Click')
    console.log('App Back Value Before Pop', context)
    // value.nav.pop()
    // history.push(value.nav.pop())
    console.log('App Back Value after 2 pop', context.value)
  }, [context.value.length])

  useEffect(() => {

    console.log('App Change Use Effect', context)
  }, [context.value.length])

  useEffect(() => {

    window.addEventListener('popstate', Back)

    return () => { window.removeEventListener('popstate', Back) }
  }, [])


  return (
    <div>
      <ToastContainer />
      <Router>
        <Suspense fallback={<Fragment />}>
          {/* <Switch> */}
          {/* <Route exact path="/"><BeforeLoginComponent /></Route> */}
          <LoginProtected exact path="/" component={BeforeLoginComponent}></LoginProtected>
          {/* <Route  path="/dashboard"><LoggedInComponent /></Route> */}
          {/* <Route exact path="/dashboard/"> <MiniD><Home /></MiniD></Route>
          <Route exact path="/dashboard/user"><MiniD><Users /></MiniD></Route>
          <Route exact path="/dashboard/ads" ><MiniD><Ads /></MiniD></Route> */}
          {routes.map((route) => {
            return (<SocketContext.Provider key={route.key} value={{ socket, socketAuth }} ><ProtectedRoute exact={true} key={route.key} path={route.path} component={(props => { return <MiniD><route.component /></MiniD> })}></ProtectedRoute></SocketContext.Provider>)
          })}
          {/* <ProtectedRoute exact path="/dashboard/user" component={MiniD}></ProtectedRoute>
          <ProtectedRoute exact path="/dashboard/ads" component={MiniD}></ProtectedRoute> */}
          {/* </Switch> */}
        </Suspense>
      </Router>


    </div>
  );
}

export default withRouter(App);
