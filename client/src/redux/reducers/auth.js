import setAuthorizationToken from '../../components/utils/setAuthorizationToken';
import { VIDEO_LIST, ADS_LIST, SET_USER, ADS_TYPE_LIST, AGE_LIST, SINGLE_ADS_LIST, SINGLE_AUDIENCE, SINGLE_BUDGET, SINGLE_MOBILE_USER_LIST, SINGLE_REWARD_LIST, SINGLE_VIDEO_LIST, SINGLE_WEB_USER_LIST, CAMPAIGN_LIST, CAMPAIGN_TYPE_LIST, CREATED, DASHBOARD_LIST, GENDER_LIST, INTEREST_LIST, LANGUAGE_LIST, MOBILE_USER_LIST, OCCUPATION_LIST, REWARD_LIST, WEB_USER_LIST, LOGOUT, STAT_LIST, ALL_TRANSACTION, ALL_LANDINGS, ALL_EXCHANGES, ALL_DAILY_RANKS, ALL_CALLING_RANKS, STATUS_LIST, FAILED, CALLING_RANK_UPDATE, DAILY_RANK_UPDATE, VIDEO_UPDATED, SCHEDULE_LIST, SOCKET_CONNECT, NOTIFICATION_LIST, WEB_WORK_LIST, WEB_ONLINE_LIST, NETWORK_LINK_LIST, FOOTER_LIST, ISSUE_TYPE_LIST, USER_CONTACT_LIST, SUCCESS, SUCCESS_REDIRECT, LOADING, ALL_COMBINATIONS, CALLING_REQUEST, DAILY_REQUEST, CAMPAIGN_REQUEST, TARGET_LIST, WEBUSER_UPDATE_LIST, CAMPAIGN_DETAIL_UPDATE, CAMPAIGN_REQUEST_UPDATE, ADMIN_SETTINGS, USER_STATUS_SOCKET, MOBILE_USER_UPDATE, WEB_USER_UPDATE } from '../actions/types';

const initialState = {
  isAuthenticated: false,
  user: {},
  videoDetail: [],
  videoTotal: null,
  singleVideoDetail: {},
  adsDetail: [],
  adsTotal: null,
  singleAdsDetail: {},
  campaignDetail: [],
  dashboardCount: {},
  webUserDetail: [],
  webTotal: null,
  mobileUserDetail: [],
  mobileTotal: null,
  singleWebUser: {},
  singleMobileUser: {},
  rewardDetail: [],
  rewardTotal: null,
  singleReward: {},
  ageDetail: [],
  ageTotal: null,
  adsTypeDetail: [],
  adsTypeTotal: null,
  campaignTypeDetail: [],
  campaignTypeTotal: null,
  occupationDetail: [],
  occupationTotal: null,
  genderDetail: [],
  genderTotal: null,
  languageDetail: [],
  languageTotal: null,
  interestDetail: [],
  interestTotal: null,
  budgetDetail: {},
  audienceDetail: {},
  statDetail: [],
  allTrasaction: [],
  allScore: [],
  scoreTotal: null,
  allExchanges: [],
  exchangeTotal: null,
  allDailyRanks: [],
  dailyTotal: null,
  allCallingRanks: [],
  callingTotal: null,
  statusDetail: [],
  scheduleDetail: [],
  scheduleTotal: null,
  socket_info: false,
  notificationDetail: [],
  notificationTotal: null,
  onlineDetail: {},
  onlineTotal: null,
  workDetail: {},
  workTotal: null,
  networkDetail: {},
  networkTotal: null,
  footerDetail: {},
  footerTotal: null,
  issueDetail: {},
  issueTotal: null,
  contactDetail: {},
  contactTotal: null,
  loader: false,
  created: false,
  updated: false,
  combinationDetail: {},
  callingTable: {},
  callingTableTotal: null,
  dailyTable: {},
  dailyTableTotal: null,
  campaignTable: [],
  targetDetail: {},
  targetTotal: null,
  webUserUpdate: [],
  adminSettingData: {},
  userSocketStatus: {},
  userSocketTotal: null
}

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SET_USER: {
      return {
        ...state,
        isAuthenticated: true,
        user: payload,
      }
    }
    case VIDEO_LIST: {
      let arr2 = state.videoDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        videoDetail: state.videoDetail.length == 0 ? payload.data.data : arr2,
        videoTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false
      }
    }
    case ADS_LIST: {
      let arr2 = state.adsDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        adsDetail: state.adsDetail.length == 0 ? payload.data.data : arr2,
        adsTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false
      }
    }
    case DASHBOARD_LIST: {
      return {
        ...state,
        dashboardCount: payload
      }
    }
    case REWARD_LIST: {
      let arr2 = state.rewardDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        rewardDetail: state.rewardDetail.length == 0 ? payload.data.data : arr2,
        rewardTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case CAMPAIGN_LIST: {
      return {
        ...state,
        campaignDetail: payload.data,
        updated: false,
      }
    }
    case CAMPAIGN_TYPE_LIST: {
      let arr2 = state.campaignTypeDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        campaignTypeDetail: state.campaignTypeDetail.length == 0 ? payload.data.data : arr2,
        campaignTypeTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case ADS_TYPE_LIST: {
      let arr2 = state.adsTypeDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        adsTypeDetail: state.adsTypeDetail.length == 0 ? payload.data.data : arr2,
        adsTypeTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case AGE_LIST: {
      let arr2 = state.ageDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        ageDetail: state.ageDetail.length == 0 ? payload.data.data : arr2,
        ageTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case GENDER_LIST: {
      let arr2 = state.genderDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        genderDetail: state.genderDetail.length == 0 ? payload.data.data : arr2,
        genderTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case INTEREST_LIST: {
      let arr2 = state.interestDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        interestDetail: state.interestDetail.length == 0 ? payload.data.data : arr2,
        interestTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case LANGUAGE_LIST: {
      let arr2 = state.languageDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        languageDetail: state.languageDetail.length == 0 ? payload.data.data : arr2,
        languageTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case OCCUPATION_LIST: {
      let arr2 = state.occupationDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        occupationDetail: state.occupationDetail.length == 0 ? payload.data.data : arr2,
        occupationTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case WEB_USER_LIST: {
      let arr2 = state.webUserDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        webUserDetail: state.webUserDetail.length == 0 ? payload.data.data : arr2,
        webTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false,
      }
    }
    case MOBILE_USER_LIST: {
      let arr2 = state.mobileUserDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        mobileUserDetail: state.mobileUserDetail.length == 0 ? payload.data.data : arr2,
        mobileTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false,
      }
    }
    case SINGLE_MOBILE_USER_LIST: {
      return {
        ...state,
        singleMobileUser: payload.data
      }
    }
    case SINGLE_WEB_USER_LIST: {
      return {
        ...state,
        singleWebUser: payload.data
      }
    }
    case SINGLE_REWARD_LIST: {
      return {
        ...state,
        singleReward: payload.data
      }
    }
    case SINGLE_VIDEO_LIST: {
      return {
        ...state,
        singleVideoDetail: payload.data
      }
    }
    case SINGLE_BUDGET: {
      return {
        ...state,
        budgetDetail: payload.data
      }
    }
    case SINGLE_AUDIENCE: {
      return {
        ...state,
        audienceDetail: payload.data
      }
    }
    case SINGLE_ADS_LIST: {
      return {
        ...state,
        singleAdsDetail: payload.data
      }
    }
    case STAT_LIST: {
      return {
        ...state,
        statDetail: payload.data
      }
    }
    case ALL_TRANSACTION: {
      return {
        ...state,
        allTrasaction: payload.data
      }
    }
    case ALL_LANDINGS: {
      let arr2 = state.allScore.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        allScore: state.allScore.length == 0 ? payload.data.data : arr2,
        scoreTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false
      }
    }
    case ALL_EXCHANGES: {
      let arr2 = state.allExchanges.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        allExchanges: state.allExchanges.length == 0 ? payload.data.data : arr2,
        exchangeTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false
      }
    }
    case ALL_DAILY_RANKS: {
      let arr2 = state.allDailyRanks.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        allDailyRanks: state.allDailyRanks.length == 0 ? payload.data.data : arr2,
        dailyTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false,
      }
    }
    case ALL_CALLING_RANKS: {
      let arr2 = state.allCallingRanks.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        allCallingRanks: state.allCallingRanks.length == 0 ? payload.data.data : arr2,
        callingTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        updated: false,
      }
    }
    case STATUS_LIST: {
      return {
        ...state,
        statusDetail: payload.data,
        created: false,
        updated: false
      }
    }
    case SCHEDULE_LIST: {
      let arr2 = state.scheduleDetail.concat(payload.data.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        scheduleDetail: state.scheduleDetail.length == 0 ? payload.data.data : arr2,
        scheduleTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false,
      }
    }
    case CALLING_RANK_UPDATE: {
      var rankC = [...state.allCallingRanks]
      return {
        ...state,
        allCallingRanks: (rankC.length > 0) ? rankC.map((item, i) => {
          if (item._id == payload._id) {
            return payload
          }
          return item
        }) : rankC
      }
    }
    case DAILY_RANK_UPDATE: {
      var rankD = [...state.allDailyRanks]
      return {
        ...state,
        allDailyRanks: (rankD.length > 0) ? rankD.map((item, i) => {
          if (item._id == payload._id) {
            return payload
          }
          return item
        }) : rankD
      }
    }
    case VIDEO_UPDATED: {
      var viddata = [...state.videoDetail]
      return {
        ...state,
        videoDetail: (viddata.length > 0) ? viddata.map((vid) => {
          if (vid._id == payload._id) {
            return payload
          }
          return vid
        }) : viddata
      }
    }
    case SOCKET_CONNECT: {
      return {
        ...state,
        socket_info: payload
      }
    }
    case NOTIFICATION_LIST: {
      let arr2 = state.notificationDetail.concat(payload.data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        notificationDetail: state.notificationDetail.length == 0 ? payload.data : arr2,
        notificationTotal: (payload.total == null || payload.total == undefined) ? null : payload.total,
        updated: false,
      }
    }
    case WEB_ONLINE_LIST: {
      let arr2 = (state.onlineDetail.onlineList == null || state.onlineDetail.onlineList == undefined) ? {} : state.onlineDetail.onlineList.concat(payload.data.onlineList).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        onlineDetail: Object.keys(state.onlineDetail).length == 0 ? payload.data : { onlineList: arr2 },
        onlineTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case WEB_WORK_LIST: {
      let arr2 = (state.workDetail.workList == null || state.workDetail.workList == undefined) ? {} : state.workDetail.workList.concat(payload.data.workList).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        workDetail: Object.keys(state.workDetail).length == 0 ? payload.data : { workList: arr2 },
        workTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case NETWORK_LINK_LIST: {
      let arr2 = (state.networkDetail.networkList == null || state.networkDetail.networkList == undefined) ? {} : state.networkDetail.networkList.concat(payload.data.networkList).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        networkDetail: Object.keys(state.networkDetail).length == 0 ? payload.data : { networkList: arr2 },
        networkTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case FOOTER_LIST: {
      let arr2 = (state.footerDetail.footerList == null || state.footerDetail.footerList == undefined) ? {} : state.footerDetail.footerList.concat(payload.data.footerList).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        footerDetail: Object.keys(state.footerDetail).length == 0 ? payload.data : { footerList: arr2 },
        footerTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case ISSUE_TYPE_LIST: {
      let arr2 = (state.issueDetail.issueList == null || state.issueDetail.issueList == undefined) ? {} : state.issueDetail.issueList.concat(payload.data.issueList).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        issueDetail: Object.keys(state.issueDetail).length == 0 ? payload.data : { issueList: arr2 },
        issueTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case USER_CONTACT_LIST: {
      let arr2 = (state.contactDetail.contactList == null || state.contactDetail.contactList == undefined) ? {} : state.contactDetail.contactList.concat(payload.data.contactList).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        contactDetail: Object.keys(state.contactDetail).length == 0 ? payload.data : { contactList: arr2 },
        contactTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case SUCCESS:
      return {
        ...state,
        loader: false,
        updated: false,
        created: false
      }
    case SUCCESS_REDIRECT:
      return {
        ...state,
        loader: false,
        updated: true,
        created: false
      }
    case LOADING:
      return {
        ...state,
        loader: true
      }
    case FAILED: {
      return {
        ...state,
        loader: false,
        updated: false,
        created: false
      }
    }

    case LOGOUT: {
      localStorage.removeItem('auth');
      setAuthorizationToken(false);
      return initialState
    }
    case CREATED: {
      return {
        ...state,
        created: true,
        loader: false,
        updated: false
      }
    }
    case ALL_COMBINATIONS: {
      return {
        ...state,
        combinationDetail: payload.data,
        created: false,
        updated: false
      }
    }
    case CALLING_REQUEST: {
      let arr2 = (state.callingTable.calling_data == null || state.callingTable.calling_data == undefined) ? {} : state.callingTable.calling_data.concat(payload.data.calling_data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        callingTable: Object.keys(state.callingTable).length == 0 ? payload.data : { calling_data: arr2 },
        callingTableTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case DAILY_REQUEST: {
      let arr2 = (state.dailyTable.daily_data == null || state.dailyTable.daily_data == undefined) ? {} : state.dailyTable.daily_data.concat(payload.data.daily_data).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        dailyTable: Object.keys(state.dailyTable).length == 0 ? payload.data : { daily_data: arr2 },
        dailyTableTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        created: false,
        updated: false
      }
    }
    case CAMPAIGN_REQUEST: {
      return {
        ...state,
        campaignTable: payload.data,
        created: false,
        updated: false
      }
    }
    case TARGET_LIST: {
      let arr2 = (state.targetDetail.target == null || state.targetDetail.target == undefined) ? {} : state.targetDetail.target.concat(payload.target).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        targetDetail: Object.keys(state.targetDetail).length == 0 ? payload : { target: arr2 },
        targetTotal: (payload.total == null || payload.total == undefined) ? null : payload.total,
        updated: false
      }
    }
    case WEBUSER_UPDATE_LIST: {
      return {
        ...state,
        webUserUpdate: payload,
        updated: false
      }
    }
    case CAMPAIGN_DETAIL_UPDATE: {
      return {
        ...state,
        campaignDetail: state.campaignDetail.length > 0 ? state.campaignDetail.map((item, i) => {
          if (item._id == payload._id) {
            return payload
          }
          return item
        }) : [],
        updated: false
      }
    }
    case CAMPAIGN_REQUEST_UPDATE: {
      return {
        ...state,
        campaignTable: (state.campaignTable.length == 0) ? [] : {
          ...state.campaignTable, campaign_data: state.campaignTable.campaign_data.map((item, i) => {
            if (item._id == payload._id) {
              return payload
            }
            return item
          })
        },
        updated: false
      }
    }
    case ADMIN_SETTINGS: {
      return {
        ...state,
        adminSettingData: payload,
        created: false,
        updated: false,
        loader: false,
      }
    }
    case USER_STATUS_SOCKET: {
      let arr2 = (state.userSocketStatus.socket_status == null || state.userSocketStatus.socket_status == undefined) ? {} : state.userSocketStatus.socket_status.concat(payload.socket_status).map(item => JSON.stringify(item)).filter((item, index, arr) => { return arr.indexOf(item) == index }).map(item => JSON.parse(item))
      return {
        ...state,
        userSocketStatus: Object.keys(state.userSocketStatus).length == 0 ? payload : { socket_status: arr2 },
        userSocketTotal: (payload.total == null || payload.total == undefined) ? null : payload.total,
        created: false,
        updated: false,
        loader: false,
      }
    }
    case MOBILE_USER_UPDATE: {
      var mobileData = [...state.mobileUserDetail]
      return {
        ...state,
        mobileUserDetail: (mobileData.length > 0) ? mobileData.map((mob) => {
          if (mob._id == payload._id) {
            return payload
          }
          return mob
        }) : mobileData,
        updated: false
      }
    }
    case WEB_USER_UPDATE: {
      var webUserD = [...state.webUserDetail]
      return {
        ...state,
        webUserDetail: (webUserD.length > 0) ? webUserD.map((web) => {
          if (web._id == payload._id) {
            return payload
          }
          return web
        }) : webUserD,
        updated: false
      }
    }
    default:
      return state;
  }
}