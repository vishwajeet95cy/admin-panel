import isEmpty from 'lodash/isEmpty';

const initialState = {
    isAuthenticated: false,
    user: {}
};

const auth_reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_USER':
            return {
                isAuthenticated: !isEmpty(action.user),
                user: action.user
            }
        default:
            return state;
    }
};

export default auth_reducer;