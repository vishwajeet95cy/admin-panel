import { VIDEO_LIST, ADS_LIST, SET_USER, ADS_TYPE_LIST, AGE_LIST, SINGLE_ADS_LIST, SINGLE_AUDIENCE, SINGLE_BUDGET, SINGLE_MOBILE_USER_LIST, SINGLE_REWARD_LIST, SINGLE_VIDEO_LIST, SINGLE_WEB_USER_LIST, CAMPAIGN_LIST, CAMPAIGN_TYPE_LIST, CREATED, DASHBOARD_LIST, GENDER_LIST, INTEREST_LIST, LANGUAGE_LIST, MOBILE_USER_LIST, OCCUPATION_LIST, REWARD_LIST, WEB_USER_LIST, LOGOUT, STAT_LIST, ALL_TRANSACTION, ALL_LANDINGS, ALL_EXCHANGES, ALL_CALLING_RANKS, ALL_DAILY_RANKS, STATUS_LIST, FAILED, CALLING_RANK_UPDATE, DAILY_RANK_UPDATE, VIDEO_UPDATED, SCHEDULE_LIST, SOCKET_CONNECT, NOTIFICATION_LIST, WEB_ONLINE_LIST, WEB_WORK_LIST, NETWORK_LINK_LIST, FOOTER_LIST, ISSUE_TYPE_LIST, USER_CONTACT_LIST, LOADING, SUCCESS, SUCCESS_REDIRECT, ALL_COMBINATIONS, CALLING_REQUEST, DAILY_REQUEST, CAMPAIGN_REQUEST, UPDATED, TARGET_LIST, WEBUSER_UPDATE_LIST, CAMPAIGN_DETAIL_UPDATE, CAMPAIGN_REQUEST_UPDATE, ADMIN_SETTINGS, USER_STATUS_SOCKET, MOBILE_USER_UPDATE, WEB_USER_UPDATE } from './types';
import axios from 'axios';
import { toast } from 'react-toastify';
import BaseUrl from '../../components/utils/BaseUrl';
import setAuthorizationToken from '../../components/utils/setAuthorizationToken';
import jwt from 'jsonwebtoken';

export const Userlogin = (data) => async dispatch => {

  const config = {
    "Accept": "application/json",
    "Content-Type": "application/json",
  }

  dispatch({ type: LOADING })

  try {
    const res = await axios.post(BaseUrl + 'server/admin/login', data, config);

    dispatch({ type: SUCCESS })

    localStorage.setItem("auth", res.data.data.token);
    setAuthorizationToken(res.data.data.token);

    dispatch({
      type: SET_USER,
      payload: jwt.decode(res.data.data.token)
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const WelcomeUser = (data) => async dispatch => {
  dispatch({
    type: SET_USER,
    payload: data
  })
}

export const Logout = () => async dispatch => {
  localStorage.removeItem('auth')
  dispatch({
    type: LOGOUT
  })
}

export const AdminCount = (data) => async dispatch => {

  dispatch({
    type: DASHBOARD_LIST,
    payload: data
  })

}

// export const getCount = () => async dispatch => {

//   try {
//     const res = await axios.get(BaseUrl + 'server/admin/allCount');


//     dispatch({
//       type: DASHBOARD_LIST,
//       payload: res.data
//     });

//   } catch (err) {

//     if (!err.response || err.response === null || err.response === undefined) {
//       toast(err.message, {
//         position: "top-right",
//         autoClose: 5000,
//         type: 'error',
//         closeOnClick: true,
//         pauseOnHover: true,
//       });
//     } else {
//       toast(err.response.data.message, {
//         position: "top-right",
//         autoClose: 5000,
//         type: 'error',
//         closeOnClick: true,
//         pauseOnHover: true,
//       });
//     }

//   }

// };

export const GetAllVideo = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/getallvideos/' + skip + '/' + limit);


    dispatch({
      type: VIDEO_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const getAllAds = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/getallads/' + skip + '/' + limit);


    dispatch({
      type: ADS_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const ChangeAdsVideo = (id, value) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const { data } = await axios.post(BaseUrl + 'server/admin/changeVideo/' + id, value);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast(data.message, {
      position: "top-right",
      autoClose: 5000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const getAllRewards = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/getAllRewards/' + skip + '/' + limit);


    dispatch({
      type: REWARD_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllCampaign = () => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/getallcampaigns');


    dispatch({
      type: CAMPAIGN_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateCampaignCredential = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updatecampaigndata', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const FilterCampaignByType = (type) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const res = await axios.get(BaseUrl + 'server/admin/getCampaignQuery/' + type);

    dispatch({ type: SUCCESS })

    dispatch({
      type: CAMPAIGN_LIST,
      payload: res.data
    });


  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllCampaignTypes = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allCampaignTypes/' + skip + '/' + limit);


    dispatch({
      type: CAMPAIGN_TYPE_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateCampaignTypes = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateCampaignTypes', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllAdsTypes = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allAdsTypes/' + skip + '/' + limit);


    dispatch({
      type: ADS_TYPE_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateAdsTypes = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateAdsTypes', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllAge = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allAges/' + skip + '/' + limit);


    dispatch({
      type: AGE_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateAge = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateAges', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllGender = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allGenders/' + skip + '/' + limit);


    dispatch({
      type: GENDER_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateGender = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateGender', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllInterest = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allInterest/' + skip + '/' + limit);


    dispatch({
      type: INTEREST_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateInterest = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateInterest', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}


export const GetAllLanguage = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allLanguage/' + skip + '/' + limit);


    dispatch({
      type: LANGUAGE_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateLanguage = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateLanguage', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllOccupation = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allOccupation/' + skip + '/' + limit);


    dispatch({
      type: OCCUPATION_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateOccupation = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateOccupation', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllWebUser = (skip, limit) => async dispatch => {

  try {

    const res = await axios.get(BaseUrl + 'server/admin/getwebuser/' + skip + '/' + limit)

    dispatch({
      type: WEB_USER_LIST,
      payload: res.data
    })

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const ActivateWebUser = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/webchangestatus/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllMobileUser = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/getmobileusers/' + skip + '/' + limit);


    dispatch({
      type: MOBILE_USER_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const ActivateMobileUser = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/mobilechangestatus/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const SingleWebUsers = (id) => async dispatch => {

  try {

    const res = await axios.get(BaseUrl + 'server/admin/singleWebUser/' + id);

    dispatch({
      type: SINGLE_WEB_USER_LIST,
      payload: res.data
    })

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }


}

export const UpdateWebUser = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  const config = {
    'Content-Type': 'multipart/form-data'
  }

  try {
    await axios.post(BaseUrl + 'server/admin/updatewebuser/' + id, data, config);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const SingleMobileUser = (id) => async dispatch => {

  try {

    const res = await axios.get(BaseUrl + 'server/admin/singleMobileUser/' + id);

    dispatch({
      type: SINGLE_MOBILE_USER_LIST,
      payload: res.data
    })

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateMobileUser = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updatemobileuser/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const SingleRewards = (id) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/singleRewards/' + id);


    dispatch({
      type: SINGLE_REWARD_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const updateReward = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/changeRewards/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const SingleVideo = (id) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/singleVideo/' + id);


    dispatch({
      type: SINGLE_VIDEO_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }


}

export const UpdateVideo = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  const config = {
    'Content-Type': 'multipart/form-data'
  }

  try {
    await axios.post(BaseUrl + 'server/admin/updateVideo/' + id, data, config);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const SingleAds = (id) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/singleAds/' + id);


    dispatch({
      type: SINGLE_ADS_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateAds = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/changeAds/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const SingleBudget = (id) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/singleBudget/' + id);


    dispatch({
      type: SINGLE_BUDGET,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateBudget = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateBudget/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const SingleAudience = (id) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/singleAudience/' + id);


    dispatch({
      type: SINGLE_AUDIENCE,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateAudience = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateAudience/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllStats = () => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allStats')

    dispatch({
      type: STAT_LIST,
      payload: res.data
    })

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const CreateAdsType = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addAdsTypes', data)

    dispatch({
      type: CREATED
    })

    toast("Ads Type Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const CreateCampaignType = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addCampaignTypes', data)

    dispatch({
      type: CREATED
    })

    toast("Campaign Type Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const CreateAge = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addAges', data)

    dispatch({
      type: CREATED
    })

    toast("Age Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const CreateGender = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addGender', data)

    dispatch({
      type: CREATED
    })

    toast("Gender Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const CreateInterest = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addInterest', data)

    dispatch({
      type: CREATED
    })

    toast("Interest Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const CreateLanguage = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addLanguage', data)

    dispatch({
      type: CREATED
    })

    toast("Language Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const CreateOccupation = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addOccupation', data)

    dispatch({
      type: CREATED
    })

    toast("Occupation Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const CreateDailyReward = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/createrewards', data)

    dispatch({
      type: CREATED
    })

    toast("Daily Reward Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllTransaction = () => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/AllTransaction');


    dispatch({
      type: ALL_TRANSACTION,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}


export const GetAllLandings = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allLandings/' + skip + '/' + limit);


    dispatch({
      type: ALL_LANDINGS,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateLandings = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateLandings', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllExchanges = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allExchanges/' + skip + '/' + limit);


    dispatch({
      type: ALL_EXCHANGES,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateExchanges = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateExchanges', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const AllCallingRanks = (skip, limit) => async dispatch => {

  try {

    const res = await axios.get(BaseUrl + 'server/admin/callingRanks/' + skip + '/' + limit);

    dispatch({
      type: ALL_CALLING_RANKS,
      payload: res.data
    })

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const AllDailyRanks = (skip, limit) => async dispatch => {

  try {

    const res = await axios.get(BaseUrl + 'server/admin/dailyRanks/' + skip + '/' + limit);

    dispatch({
      type: ALL_DAILY_RANKS,
      payload: res.data
    })

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const CreateStatus = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addStatus', data)

    dispatch({
      type: CREATED
    })

    toast("Status Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllStatus = () => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allStatus');


    dispatch({
      type: STATUS_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateStatus = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateStatus', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const StartAllCampaign = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/startAllCamp', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast("Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({
      type: FAILED
    });

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const ActiveAllCampaign = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/activateAllCamp', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast("Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({
      type: FAILED
    });

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const ChangeAuthStatus = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/changeAuthStatus/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast("Updated Successfully", {
      position: "top-center",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateAllAuthBudget = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/budgetAllCamp', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast("Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({
      type: FAILED
    });

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateAllAdsScore = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/scoreAllAds', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast("Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({
      type: FAILED
    });

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}


export const UpdateCallingRank = (data) => async dispatch => {
  dispatch({
    type: CALLING_RANK_UPDATE,
    payload: data
  })
}

export const UpdateDailyRank = (data) => async dispatch => {
  dispatch({
    type: DAILY_RANK_UPDATE,
    payload: data
  })
}

export const simpleCampaign = (data) => async dispatch => {
  dispatch({
    type: CAMPAIGN_DETAIL_UPDATE,
    payload: data
  })
}

export const requestCampaign = (data) => async dispatch => {
  dispatch({
    type: CAMPAIGN_REQUEST_UPDATE,
    payload: data
  })
}

export const ChangeTargetStatus = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/changeTargetStatus/' + id, data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast("Updated Successfully", {
      position: "top-center",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const convertVideo = (id) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const { data } = await axios.get(BaseUrl + 'server/admin/adminConvertVideo/' + id);

    dispatch({ type: SUCCESS })

    toast(data.message, {
      position: "top-center",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const ConvertVideoUpdate = (data) => async dispatch => {

  dispatch({
    type: VIDEO_UPDATED,
    payload: data
  })

}

export const CreateSchedules = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/addSchedule', data)

    dispatch({
      type: CREATED
    })

    toast("Schedule Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllSchedules = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allSchedule/' + skip + '/' + limit);


    dispatch({
      type: SCHEDULE_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const UpdateSchedules = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateSchedule', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Socket_Connection = (data) => async dispatch => {
  dispatch({
    type: SOCKET_CONNECT,
    payload: data
  })
}

export const Delete_Age = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteAge', data);

    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Age Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Gender = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteGender', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Gender Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Language = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteLanguage', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });


    toast.success('Language Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Occupation = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteOccupation', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Occupation Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Interest = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteInterest', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Interest Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Campaign_Type = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteCampaignType', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Campaign Type Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Ads_Type = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteAdsType', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('AdType Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Ads_Schedule = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteAdSchedules', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('AdSchedule Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Status = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteStatus', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Status Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllNotifications = (skip, limit) => async dispatch => {
  try {
    const res = await axios.get(BaseUrl + 'server/admin/allNotification/' + skip + '/' + limit);


    dispatch({
      type: NOTIFICATION_LIST,
      payload: res.data.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Delete_Notification = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteNotification', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Notification Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

// WEB_FRONT APIS
export const GetAllOnlineWork = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allOnlineList/' + skip + '/' + limit);


    dispatch({
      type: WEB_ONLINE_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Delete_Online = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteOnline', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Online Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllWorkList = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allWorkList/' + skip + '/' + limit);


    dispatch({
      type: WEB_WORK_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Delete_Work = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteWork', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Work Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Create_Online = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    // await axios.post(BaseUrl + 'server/admin/createOnline', data, config);
    await axios({
      method: 'POST',
      url: BaseUrl + 'server/admin/createOnline', headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: data
    })

    dispatch({
      type: CREATED
    })

    toast("Online Data Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Create_Work = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    // await axios.post(BaseUrl + 'server/admin/createWork', data, config);
    await axios({
      method: 'POST',
      url: BaseUrl + 'server/admin/createWork', headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: data
    })

    dispatch({
      type: CREATED
    })

    toast("Work Data Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Update_Online = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios({
      method: 'POST',
      url: BaseUrl + 'server/admin/updateOnline/' + id, headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: data
    })

    dispatch({
      type: SUCCESS_REDIRECT
    })

    toast("Online Data Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Update_Work = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios({
      method: 'POST',
      url: BaseUrl + 'server/admin/updateWork/' + id, headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: data
    })

    dispatch({
      type: SUCCESS_REDIRECT
    })

    toast("Work Data Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Start_Work = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/startWork', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Work Start Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Stop_Work = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/startWork', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Work Stopped Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Start_Online = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/startOnline', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Online Start Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Stop_Online = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/startOnline', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Online Stop Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

// NetWork List
export const Create_Network = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/createNetwork', data);

    dispatch({
      type: CREATED
    })

    toast("Network Data Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllNetworkList = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allNetworkList/' + skip + '/' + limit);


    dispatch({
      type: NETWORK_LINK_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Delete_Network = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteNetwork', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Network Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Update_Network = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateNetwork/' + id, data)

    dispatch({
      type: SUCCESS_REDIRECT
    })

    toast("Network Data Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Start_Network = (value) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const { data } = await axios.post(BaseUrl + 'server/admin/startNetwork', value);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success(data.message)

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Stop_Network = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/startNetwork', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Network Stopped Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}


// Banner Start or STop

export const BannerOnlineStart = (value) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const { data } = await axios.post(BaseUrl + 'server/admin/bannerOnline', value);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success(data.message)

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const BannerOnlineStop = (value) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const { data } = await axios.post(BaseUrl + 'server/admin/bannerOnline', value);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success(data.message)

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const BannerWorkStart = (value) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const { data } = await axios.post(BaseUrl + 'server/admin/bannerWork', value);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success(data.message)

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const BannerWorkStop = (value) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    const { data } = await axios.post(BaseUrl + 'server/admin/bannerWork', value);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success(data.message)

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

// Footer List Link
export const Create_Footer = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/createFooter', data);

    dispatch({
      type: CREATED
    })

    toast("Footer Data Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllFooterList = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allFooterList/' + skip + '/' + limit);


    dispatch({
      type: FOOTER_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Delete_Footer = (data) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteFooter', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Footer Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Update_Footer = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateFooter/' + id, data)

    dispatch({
      type: SUCCESS_REDIRECT
    })

    toast("Footer Data Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

// Issue Type  List Link
export const Create_IssueType = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/createIssueType', data);

    dispatch({
      type: CREATED
    })

    toast("Issue Type Data Created", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllIssueTypeList = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allIssueTypeList/' + skip + '/' + limit);


    dispatch({
      type: ISSUE_TYPE_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Delete_IssueType = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteIssueType', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Issue Type Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const Update_IssueType = (id, data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateIssueType/' + id, data)

    dispatch({
      type: SUCCESS_REDIRECT
    })

    toast("Issue Type Updated Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const GetAllUserContactList = (skip, limit) => async dispatch => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/allUserContact/' + skip + '/' + limit);


    dispatch({
      type: USER_CONTACT_LIST,
      payload: res.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }

}

export const Delete_UserContact = (data) => async dispatch => {

  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/deleteContactRequest', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('User Contact Deleted Successfully')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const createCombinations = () => async (dispatch) => {

  dispatch({ type: LOADING })

  try {
    await axios.get(BaseUrl + 'server/admin/createCombination');

    dispatch({
      type: CREATED
    })

    toast("Combinations Created Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const getAllCombinations = () => async (dispatch) => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/getCombination');

    dispatch({
      type: ALL_COMBINATIONS,
      payload: res.data
    })


  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const createFinals = () => async (dispatch) => {

  dispatch({ type: LOADING })

  try {
    await axios.get(BaseUrl + 'server/admin/finalResult');

    dispatch({
      type: CREATED
    })

    toast("Final and Unions Created Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const getCallingRequests = (skip, limit) => async (dispatch) => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/callingRequest/' + skip + '/' + limit);

    dispatch({
      type: CALLING_REQUEST,
      payload: res.data
    })


  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }


}

export const getDailyRequests = (skip, limit) => async (dispatch) => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/dailyRequest/' + skip + '/' + limit);

    dispatch({
      type: DAILY_REQUEST,
      payload: res.data
    })


  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }


}

export const getCampaignRequests = () => async (dispatch) => {

  try {
    const res = await axios.get(BaseUrl + 'server/admin/campaignRequest');

    dispatch({
      type: CAMPAIGN_REQUEST,
      payload: res.data
    })


  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }


}

export const createMobileCombination = (id) => async (dispatch) => {
  dispatch({ type: LOADING })

  try {
    await axios.get(BaseUrl + 'server/admin/createMobileCombination/' + id);

    dispatch({
      type: SUCCESS_REDIRECT
    })

    toast("Mobile Combination Created Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const multipleMobileCombination = (data) => async (dispatch) => {
  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/multipleMobileCombination', data);


    dispatch({
      type: SUCCESS_REDIRECT
    });

    toast.success('Request Submitted')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const allMobileCombination = () => async (dispatch) => {
  dispatch({ type: LOADING })

  try {
    await axios.get(BaseUrl + 'server/admin/testUser');


    dispatch({
      type: SUCCESS
    });

    toast.success('Request Submitted')

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const GetAllTargets = (skip, limit) => async dispatch => {
  try {
    const res = await axios.get(BaseUrl + 'server/admin/allTarget/' + skip + '/' + limit);


    dispatch({
      type: TARGET_LIST,
      payload: res.data.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const getAllUserUpdates = () => async (dispatch) => {
  try {
    const res = await axios.get(BaseUrl + 'server/admin/webuserUpdate');


    dispatch({
      type: WEBUSER_UPDATE_LIST,
      payload: res.data.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const updateExchangeStatus = (id, data) => async (dispatch) => {
  dispatch({ type: LOADING })

  try {
    await axios.post(BaseUrl + 'server/admin/updateExchangeStatus/' + id, data);

    dispatch({
      type: SUCCESS_REDIRECT
    })

    toast("Exhange Status Updates Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const getAdminSetting = () => async (dispatch) => {
  try {
    const res = await axios.get(BaseUrl + 'server/admin/adminSetting');


    dispatch({
      type: ADMIN_SETTINGS,
      payload: res.data.data
    });

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const updateAdminSetting = (id, data) => async (dispatch) => {
  dispatch({ type: LOADING })
  console.log('dd', data)

  try {
    const res = await axios.post(BaseUrl + 'server/admin/updateAdminSetting/' + id, data);

    dispatch({
      type: ADMIN_SETTINGS,
      payload: res.data.data
    });

    toast("Admin Updates Successfully", {
      position: "top-right",
      autoClose: 3000,
      type: 'success',
      closeOnClick: true,
      pauseOnHover: true,
    });

  } catch (err) {

    dispatch({ type: FAILED })

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }

  }
}

export const getAllUserStatusList = (skip, limit) => async (dispatch) => {

  try {

    const res = await axios.get(BaseUrl + 'server/admin/getUserStatus/' + skip + '/' + limit)

    dispatch({
      type: USER_STATUS_SOCKET,
      payload: res.data.data
    })

  } catch (err) {

    if (!err.response || err.response === null || err.response === undefined) {
      toast(err.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    } else {
      toast(err.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        type: 'error',
        closeOnClick: true,
        pauseOnHover: true,
      });
    }
  }

}

export const MobileDataUpdate = (data) => async dispatch => {

  dispatch({
    type: MOBILE_USER_UPDATE,
    payload: data
  })

}

export const webUserDataUpdate = (data) => async dispatch => {

  dispatch({
    type: WEB_USER_UPDATE,
    payload: data
  })

}