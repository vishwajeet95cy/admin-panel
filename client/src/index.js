import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux';
import setAuthorizationToken from './components/utils/setAuthorizationToken';
import jwt from 'jsonwebtoken';
import { WelcomeUser } from './redux/actions'
import LocationContext from './components/utils/LocationContext';
import { store } from './redux';

if (localStorage.auth) {
  setAuthorizationToken(localStorage.auth)
  store.dispatch(WelcomeUser(jwt.decode(localStorage.auth)))
}

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <Router>
        <LocationContext>
          <App />
        </LocationContext>
      </Router>
    </React.StrictMode>
  </Provider>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
