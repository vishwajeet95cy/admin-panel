import React, { useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Collapse, Box, IconButton, Typography, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { Close } from '@material-ui/icons'
import moment from 'moment';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { toast } from 'react-toastify';
import ButtonComp from '../UsableComponent/ButtonComp';
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Button from '../../newviews/CustomButtons/Button';

var ClickView

const CallingRequestBody = (props) => {

  const { datas, set } = props

  const [open, setOpen] = useState(false)
  const [view, setView] = useState(false)

  const handleView = (data) => {
    ClickView = <GridContainer>
      {data.length > 0 ? data.map((cc) => {
        return <GridItem xs={4} md={4} sm={4}>
          <div>Ip: {cc.ip}</div>
          <div>Header: {JSON.stringify(cc.header)}</div>
        </GridItem>
      }) : <GridItem xs={12} md={12} sm={12}>
        No Data Found
      </GridItem>}
    </GridContainer>
    setView(!view)
  }

  const handleClose = () => {
    setView(!view)
  }

  // onClick={() => { (Object.keys(datas.request_data.ipDetail).length > 0) ? set(datas._id, datas.request_data.ipDetail.latitude, datas.request_data.ipDetail.longitude) : toast.error('Not Latitude Found') }}

  return (
    <React.Fragment>
      <TableRow key={datas._id} >
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">{datas._id}</TableCell>
        <TableCell>{datas.op_location}</TableCell>
        <TableCell>{datas.req_ip}</TableCell>
        <TableCell>{datas.campaign_name}</TableCell>
        <TableCell>{datas.campaign_type}</TableCell>
        <TableCell>{datas.stream}</TableCell>
        <TableCell>{datas.click_count}</TableCell>
        <TableCell>
          <ButtonComp value={datas.completed} />
        </TableCell>
        <TableCell>{moment(datas.createdAt).format('LLLL')}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0, backgroundColor: '#eeeeee' }} colSpan={14}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Details
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Budget</TableCell>
                    <TableCell>Budget Type</TableCell>
                    <TableCell>Schedule</TableCell>
                    <TableCell>Target Name</TableCell>
                    <TableCell>Ads Name</TableCell>
                    <TableCell>Ads Type</TableCell>
                    <TableCell>Video Title</TableCell>
                    <TableCell>Click</TableCell>
                    <TableCell>Click Detail</TableCell>
                    <TableCell>View</TableCell>
                    <TableCell>Chunks Count</TableCell>
                    <TableCell>Created At</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {/* {(datas.stream.length == 0) ? (<TableRow >
                    <TableCell>&#8377;{datas.campaign_data.campaign_id.budget_id.budget}</TableCell>
                    <TableCell>{datas.campaign_data.campaign_id.budget_id.budgetType}</TableCell>
                    <TableCell>{datas.campaign_data.campaign_id.budget_id.adSchedule.name}</TableCell>
                    <TableCell>{datas.target_data.name}</TableCell>
                    <TableCell>{(!datas.target_data.ads_id.ads_name || datas.target_data.ads_id.ads_name == null || datas.target_data.ads_id.ads_name == undefined) ? datas.target_data.ads_id.video_id.title : datas.target_data.ads_id.ads_name}</TableCell>
                    <TableCell>{datas.target_data.ads_id.ads_type_id.ads_name}</TableCell>
                    <TableCell>{datas.target_data.ads_id.video_id.title}</TableCell>
                    <TableCell>{datas.click_count.length}</TableCell>
                    <TableCell>0</TableCell>
                    <TableCell>{datas.stream_count.length}</TableCell>
                    <TableCell>{moment(datas.date).format('LLLL')}</TableCell>
                  </TableRow>) : datas.stream.map((cc, i) => {
                    return (<TableRow key={i}>
                      <TableCell>&#8377;{datas.campaign_data.campaign_id.budget_id.budget}</TableCell>
                      <TableCell>{datas.campaign_data.campaign_id.budget_id.budgetType}</TableCell>
                      <TableCell>{datas.campaign_data.campaign_id.budget_id.adSchedule.name}</TableCell>
                      <TableCell>{datas.target_data.name}</TableCell>
                      <TableCell>{(!datas.target_data.ads_id.ads_name || datas.target_data.ads_id.ads_name == null || datas.target_data.ads_id.ads_name == undefined) ? datas.target_data.ads_id.video_id.title : datas.target_data.ads_id.ads_name}</TableCell>
                      <TableCell>{datas.target_data.ads_id.ads_type_id.ads_name}</TableCell>
                      <TableCell>{datas.target_data.ads_id.video_id.title}</TableCell>
                      <TableCell>{datas.click_count.length}</TableCell>
                      <TableCell>{(datas.stream_count.filter((item) => cc.iv == item.key).length >= 10) ? 1 : 0}</TableCell>
                      <TableCell>{datas.stream_count.length}</TableCell>
                      <TableCell>{moment(datas.date).format('LLLL')}</TableCell>
                    </TableRow>)
                  })} */}
                  <TableRow >
                    <TableCell>&#8377;{datas.campaign_data.campaign_id.budget_id.budget}</TableCell>
                    <TableCell>{datas.campaign_data.campaign_id.budget_id.budgetType}</TableCell>
                    <TableCell>{datas.campaign_data.campaign_id.budget_id.adSchedule.name}</TableCell>
                    <TableCell>{datas.target_data.name}</TableCell>
                    <TableCell>{(!datas.target_data.ads_id.ads_name || datas.target_data.ads_id.ads_name == null || datas.target_data.ads_id.ads_name == undefined) ? datas.target_data.ads_id.video_id.title : datas.target_data.ads_id.ads_name}</TableCell>
                    <TableCell>{datas.target_data.ads_id.ads_type_id.ads_name}</TableCell>
                    <TableCell>{datas.target_data.ads_id.video_id.title}</TableCell>
                    <TableCell>{datas.click_count}</TableCell>
                    <TableCell>
                      <Button color="primary" round onClick={() => { handleView(datas.click_data) }}>Click</Button>
                    </TableCell>
                    <TableCell>{(datas.completed == true) ? 1 : 0}</TableCell>
                    <TableCell>{datas.total_count}</TableCell>
                    <TableCell>{moment(datas.createdAt).format('LLLL')}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
      <Dialog
        open={view}
        onClose={handleClose}
        maxWidth="md"
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Click Data</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box>
        </DialogTitle>
        <DialogContent>
          {ClickView}
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}

export default React.memo(CallingRequestBody)
