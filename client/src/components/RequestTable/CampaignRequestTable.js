import React, { useEffect, useState, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import {
  Grid,
  Switch,
  Typography,
  DialogActions,
  Dialog,
  Box,
  DialogTitle,
  IconButton,
  TextField,
  DialogContent
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { ChangeAuthStatus, getCampaignRequests, UpdateCampaignCredential } from "../../redux/actions";
import NoData from "../UsableComponent/NoData";
import Loader from "../UsableComponent/Loader";
import NewDelete from "../UsableComponent/NewDelete";
import CampaignRequestBody from "./CampaignRequestBody";
import { Button } from "@material-ui/core";
import LeafletMap from "../UsableComponent/LeafletMap";
import { LayersControl, Marker, Popup, Circle, FeatureGroup } from "react-leaflet";
import { dateDifference } from "../UsableComponent/UniqueFunctions";
import { StatusInput } from "../utils/TargetStatus";
import { toast } from "react-toastify";
import { Close } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import ButtonMode from '../../newviews/CustomButtons/Button'

/// New Des
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from "../../newviews/Card/Card";
import CardHeader from "../../newviews/Card/CardHeader.js";
import CardBody from "../../newviews/Card/CardBody.js";
import useTable from "../UsableComponent/TableSorting/useTable";
import TblPagination from "../UsableComponent/TableSorting/TblPagination";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

const HeaderArr = [
  { name: 'form', lcheck: 1 },
  { name: 'editBudget', lcheck: 1 },
];

const headCells = [
  {
    id: "",
    numeric: false,
    disablePadding: false,
    label: "",
    sort: true,
  },
  {
    id: "yty",
    numeric: false,
    disablePadding: false,
    label: "",
    sort: true,
  },
  {
    id: "campaign_name",
    numeric: false,
    disablePadding: false,
    label: "Campaign Name",
    sort: true,
  },
  {
    id: "campaign_type",
    numeric: false,
    disablePadding: false,
    label: "Campaign Type",
    sort: true,
  },
  {
    id: "campaign_budget",
    numeric: false,
    disablePadding: false,
    label: "Campaign Admin Budget",
    sort: true,
  },
  {
    id: "budget",
    numeric: false,
    disablePadding: false,
    label: "Campaign Budget",
    sort: true,
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "Campaign Status",
    sort: true,
  },
  {
    id: "budget_type",
    numeric: false,
    disablePadding: false,
    label: "Budget Type",
    sort: true,
  },
  {
    id: "schedule",
    numeric: false,
    disablePadding: false,
    label: "AdSchedule",
    sort: true,
  },
  {
    id: "start_date",
    numeric: false,
    disablePadding: false,
    label: "Start Date",
    sort: true,
  },
  {
    id: "end_date",
    numeric: false,
    disablePadding: false,
    label: "End Date",
    sort: true,
  },
  {
    id: "days",
    numeric: false,
    disablePadding: false,
    label: "Days",
    sort: true,
  },
  {
    id: "payment_count",
    numeric: false,
    disablePadding: false,
    label: "Payment Count",
    sort: true,
  },
  {
    id: "estimated_view",
    numeric: false,
    disablePadding: false,
    label: "Estimated Views / Clicks",
    sort: true,
  },
  {
    id: "daily_view",
    numeric: false,
    disablePadding: false,
    label: "Daily Views",
    sort: true,
  },
  {
    id: "total_view",
    numeric: false,
    disablePadding: false,
    label: "Total View",
    sort: true,
  },
  {
    id: "total_count",
    numeric: false,
    disablePadding: false,
    label: "Total Count (Request)",
    sort: true,
  },
];

var NewArray = [];
var ss = "ss";
var show = 'dd'
var AuthId = [];
var BudgetId = [];

const CampaignRequestTable = () => {

  const dispatch = useDispatch();
  const history = useHistory()
  const classes = useStyles();
  const [element, setElement] = useState([]);

  const campaignRequest = useSelector((state) => state.auth.campaignTable);
  const Updated = useSelector(state => state.auth.updated)
  const [flag, setFlag] = useState(false);
  const [map, setMap] = useState(false);

  const [open, setOpen] = useState(false);
  const [data, setData] = useState({
    start: false,
    active: false,
    admin_budget: '',
    id: '',
    campaign_type: '',
    campaign_name: '',
    campaign_type_id: '',
    ads_type_id: ''
  })

  const handleClose = () => {
    setOpen(false);
  };
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  };

  const handleSwitch = (e) => {
    setData({ ...data, [e.target.name]: e.target.checked })
  }

  const handleOpen = () => {
    setOpen(true)
  }


  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    // if (newPage >= 1) {
    //   dispatch(getAllAds(ads.length, 10))
    // }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  const handleArrayData = useCallback((array) => {
    NewArray = [];
    array.map((datas) => {
      NewArray.push({
        _id: datas._id,
        campaign_name: datas.campaign_name,
        campaign_type: datas.campaign_type_id.campaign_name,
        budget: parseInt(datas.budget_id.budget),
        budget_type: datas.budget_id.budgetType,
        schedule: datas.budget_id.adSchedule.name,
        start_date: datas.budget_id.startDate,
        end_date: datas.budget_id.endDate,
        payment_count: datas.payment.length,
        days: (datas.budget_id.budgetType == 'Daily') ? 1 : dateDifference(datas.budget_id.endDate, datas.budget_id.startDate),
        estimated_view: datas.payment.reduce(function (sum, item) {
          sum = sum + parseInt(item.budgetDetail.filter((cc) => (cc._id == datas.budget_id._id))[0].budget)
          return sum
        }, 0) / datas.campaign_type_id.item_price,
        daily_view: (datas.budget_id.budgetType == 'Daily') ? datas.payment.reduce(function (sum, item) {
          sum = sum + parseInt(item.budgetDetail.filter((cc) => (cc._id == datas.budget_id._id))[0].budget)
          return sum
        }, 0) / datas.campaign_type_id.item_price : datas.payment.reduce(function (sum, item) {
          sum = sum + parseInt(item.budgetDetail.filter((cc) => (cc._id == datas.budget_id._id))[0].budget)
          return sum
        }, 0) / datas.campaign_type_id.item_price / dateDifference(datas.budget_id.endDate, datas.budget_id.startDate),
        total_view: datas.target_data
          .map((item) =>
            item.ads_id.ads_type_id.ads_type == "Daily Rewards Ads"
              ? item.streamDaily.length > 0
                ? item.streamDaily
                  .map((cc) => {
                    return item.stream_count_daily.filter(
                      (tt) => cc.iv == tt.key
                    );
                  })
                  .filter((cc) => {
                    return cc.length >= 10;
                  }).length
                : 0
              : item.streamCalling.length > 0
                ? item.streamCalling
                  .map((cc) => {
                    return item.stream_count_calling.filter(
                      (tt) => cc.iv == tt.key
                    );
                  })
                  .filter((cc) => {
                    return cc.length >= 10;
                  }).length
                : 0
          )
          .reduce(function (sum, item) {
            sum = sum + item;
            return sum;
          }, 0),
        total_count: datas.target_data.reduce(function (sum, item) {
          if (item.ads_id.ads_type_id.ads_type == "Daily Rewards Ads") {
            sum = sum + item.dailymatched.length;
          } else {
            sum = sum + item.callingmatched.length;
          }
          return sum;
        }, 0),
        target_data: datas.target_data,
        payment_data: datas.payment,
        budget_id: datas.budget_id._id,
        auth_id: datas.auth_id,
        status: datas.auth_id.astatus.name,
        campaign_budget: datas.auth_id.admin_budget,
      });
    });
    return NewArray;
  }, []);

  useEffect(() => {
    if (campaignRequest.length === 0) {
      dispatch(getCampaignRequests());
    } else {
      setElement(campaignRequest.campaign_data);
      setFlag(true);
    }

    if (Updated) {
      setOpen(false)
      AuthId = [];
      BudgetId = [];
      setCheckedCampaignNum(0)
    }

    return () => { AuthId = []; BudgetId = []; }
  }, [campaignRequest, Updated]);

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(handleArrayData(element), headCells, filterFn)

  const ChangeAuth = (e, data) => {
    var value
    var statusData

    StatusInput.map((cc) => {
      if (cc.name == e.target.value && cc.name == "Approve") {
        value = true
        statusData = {
          name: cc.name,
          type: cc.type
        }
      } else if (cc.name == e.target.value && cc.name != "Approve") {
        value = false
        statusData = {
          name: cc.name,
          type: cc.type
        }
      }
    })

    if (value) {
      if (data.admin_budget == 0) {
        return toast.error('First set Campaign Budget then it will change')
      }
    }

    const file = {
      status: statusData
    }
    console.log('Status Selected', file)
    dispatch(ChangeAuthStatus(data._id, file))
  }


  const [checkedCampaign, setCheckedCampaign] = useState([]);
  const [checkedCampaignNum, setCheckedCampaignNum] = useState(0);

  const setCheck = (e, cc) => {
    if (e.target.checked === true) {
      AuthId.push(e.target.value);
      BudgetId.push(cc.budget_id);
      setData({ start: cc.auth_id.start, active: cc.auth_id.active, admin_budget: cc.auth_id.admin_budget, id: cc._id })
    } else {
      AuthId = AuthId.filter(function (item) {
        return item !== e.target.value
      });
      BudgetId = BudgetId.filter(function (item) {
        return item !== cc.budget_id
      });
    }
    setCheckedCampaign(AuthId);
    setCheckedCampaignNum(AuthId.length);
  }

  const handlBudgetEdit = () => {
    history.push('/dashboard/editbudget', {
      from: history.location.pathname,
      id: BudgetId[0]
    })
  }

  const handleSubmit = () => {
    dispatch(UpdateCampaignCredential(data));
  }

  const handleRefresh = () => {
    dispatch(getCampaignRequests())
  }

  return (
    <GridContainer>
      {map ? element.length > 0 ? (
        <Card style={{ width: "100%", height: "80%", marginBottom: "5px" }}>
          <LeafletMap>
            {element.map((item) => {
              var arr = [];
              item.target_data.map((items) => {
                if (!items.audience_id.allLocation || items.audience_id.allLocation == null || items.audience_id.allLocation == undefined) {

                } else {
                  items.audience_id.allLocation.coordinates.map((cc) => {
                    arr.push({ name: item.campaign_name, langLat: cc });
                  })
                }
              }
              );
              return arr.map((ite) => (
                <LayersControl.Overlay checked name={ite.name}>
                  <FeatureGroup>
                    <Popup>
                      {ite.name} <br />  {ite.total_count}
                    </Popup>
                    <Circle
                      center={[ite.langLat[1], ite.langLat[0]]}
                      pathOptions={{ fillColor: getRandomColor() }}
                      radius={10000}
                    />
                  </FeatureGroup>
                </LayersControl.Overlay>
              ));
            })}
          </LeafletMap>
        </Card>
      ) : null : null}
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary" >
            <h4 className={classes.cardTitleWhite}>
              <NewDelete
                name="All Campaign Request"
                length={checkedCampaignNum} data={HeaderArr} form={handleOpen} editBudget={handlBudgetEdit}
              />
            </h4>
            <ButtonMode color="primary" round onClick={() => { handleRefresh() }} >Refresh</ButtonMode>
            {element.length > 0 ? <ButtonMode color="primary" round onClick={() => { setMap(!map) }} >{map ? 'Hide Map' : 'Show Map'}</ButtonMode> : null}
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              {flag ? (
                element.length > 0 ? (
                  <TableBody>
                    {recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage)
                      .map((row, index) => {
                        return <CampaignRequestBody key={row._id} datas={row} ChangeAuth={ChangeAuth} setCheck={setCheck} AuthId={AuthId} />;
                      })}
                  </TableBody>
                ) : (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
              ) : (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={element.length}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog open={open} fullWidth onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Edit Campaign Form</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="admin_budget"
            label="Budget"
            name="admin_budget"
            type="text"
            value={data.admin_budget}
            onChange={handleChange}
            fullWidth
          />
          <Typography component="div">
            <Grid component="label" container alignItems="center" spacing={1}>
              <Grid item>Deactive</Grid>
              <Grid item>
                <Switch checked={data.active} onChange={handleSwitch} name="active" />
              </Grid>
              <Grid item>Active</Grid>
            </Grid>
          </Typography>
          <Typography component="div">
            <Grid component="label" container alignItems="center" spacing={1}>
              <Grid item>Stop</Grid>
              <Grid item>
                <Switch checked={data.start} onChange={handleSwitch} name="start" />
              </Grid>
              <Grid item>Start</Grid>
            </Grid>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  );
};

export default CampaignRequestTable;

// <TableContainer component={Paper}>
//           <Table className={classes.table} size="small" aria-label="simple table">
//             <TableHead>
//               <TableRow>
//                 <TableCell></TableCell>
//                 <TableCell>Campaign Name</TableCell>
//                 <TableCell>Campaign Type</TableCell>
//                 <TableCell>Budget</TableCell>
//                 <TableCell>Budget Type</TableCell>
//                 <TableCell>Schedule</TableCell>
//                 <TableCell>Start Date</TableCell>
//                 <TableCell>End Date</TableCell>
//                 <TableCell>Total View</TableCell>
//                 <TableCell>Total Count</TableCell>
//               </TableRow>
//             </TableHead>
//             <TableBody>
//               {flag ? (
//                 (element.length === 0) ? <TableRow>
//                   <TableCell align="center" colSpan={15}>
//                     <NoData />
//                   </TableCell>
//                 </TableRow> : element.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((datas) => {
//                   return (
//                     <CampaignRequestBody key={datas._id} datas={datas} />
//                   )
//                 })
//               ) : <TableRow>
//                 <TableCell align="center" colSpan={15}>
//                   <Loader />
//                 </TableCell>
//               </TableRow>}
//             </TableBody>
//           </Table>
//         </TableContainer>
//         {element.length > 0 ? <TablePagination
//           rowsPerPageOptions={[5, 15, 25, 35]}
//           component="div"
//           count={element.length}
//           rowsPerPage={rowsPerPage}
//           page={page}
//           onPageChange={handleChangePage}
//           onRowsPerPageChange={handleChangeRowsPerPage}
//         /> : null}
