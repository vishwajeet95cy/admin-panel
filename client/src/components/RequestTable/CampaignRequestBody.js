import React, { useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Collapse, Box, IconButton, Typography } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import moment from 'moment';
import { StatusInput } from '../utils/TargetStatus';


const CampaignRequestBody = (props) => {

  const { datas, ChangeAuth, setCheck, AuthId } = props

  const [open, setOpen] = useState(false)

  return (
    <React.Fragment>
      <TableRow key={datas._id}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>
          <input
            type="checkbox"
            className="chekboxCampaignList"
            onClick={(e) => setCheck(e, datas)}
            id={datas._id}
            name={datas._id}
            value={datas._id}
            checked={AuthId.includes(datas._id) ? true : false}
          />
        </TableCell>
        <TableCell component="th" scope="row">{datas.campaign_name}</TableCell>
        <TableCell>{datas.campaign_type}</TableCell>
        <TableCell>&#8377;{datas.campaign_budget}</TableCell>
        <TableCell>&#8377;{datas.budget}</TableCell>
        <TableCell>
          <select style={{ margin: '6px', padding: '6px', borderRadius: '11px' }} value={datas.auth_id ? datas.status : null} onChange={(e) => { ChangeAuth(e, datas.auth_id) }}>
            {StatusInput.map((item) => {
              return (
                <option key={item.id} value={item.name}>{item.name}</option>
              )
            })}
          </select>
        </TableCell>
        <TableCell>{datas.budget_type}</TableCell>
        <TableCell>{datas.schedule}</TableCell>
        <TableCell>{datas.start_date}</TableCell>
        <TableCell>{datas.end_date}</TableCell>
        <TableCell>{datas.days}</TableCell>
        <TableCell>{datas.payment_count}</TableCell>
        <TableCell>{datas.estimated_view}</TableCell>
        <TableCell>{datas.daily_view}</TableCell>
        <TableCell>{datas.total_view}</TableCell>
        <TableCell>{datas.total_count}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0, backgroundColor: '#eeeeee' }} colSpan={14}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Details
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Target Name</TableCell>
                    <TableCell>Ads Name</TableCell>
                    <TableCell>Ads Type</TableCell>
                    <TableCell>Video Title</TableCell>
                    <TableCell>Rank</TableCell>
                    <TableCell>Request</TableCell>
                    <TableCell>View Request</TableCell>
                    <TableCell>View</TableCell>
                    <TableCell>Chunks Count</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {datas.target_data.map((cc, i) => {
                    return (<TableRow key={i}>
                      <TableCell>{cc.name}</TableCell>
                      <TableCell>{(!cc.ads_id.ads_name || cc.ads_id.ads_name == null || cc.ads_id.ads_name == undefined) ? cc.ads_id.video_id.title : cc.ads_id.ads_name}</TableCell>
                      <TableCell>{cc.ads_id.ads_type_id.ads_name}</TableCell>
                      <TableCell>{cc.ads_id.video_id.title}</TableCell>
                      <TableCell>{(cc.ads_id.ads_type_id.ads_type == 'Daily Rewards Ads') ? cc.dailyRank[0]?.rank : cc.callingRank[0]?.rank}</TableCell>
                      <TableCell>{(cc.ads_id.ads_type_id.ads_type == 'Daily Rewards Ads') ? cc.dailymatched.length : cc.callingmatched.length}</TableCell>
                      <TableCell>{(cc.ads_id.ads_type_id.ads_type == 'Daily Rewards Ads') ? cc.streamDaily.length : cc.streamCalling.length}</TableCell>
                      <TableCell>{(cc.ads_id.ads_type_id.ads_type == 'Daily Rewards Ads') ? (cc.streamDaily.length > 0) ? cc.streamDaily.map((val) => {
                        return cc.stream_count_daily.filter((tt) => val.iv == tt.key)
                      }).filter((tt) => { return tt.length >= 10 }).length : 0 : (cc.streamCalling.length > 0) ? cc.streamCalling.map((value) => {
                        return cc.stream_count_calling.filter((tt) => value.iv == tt.key)
                      }).filter((ss) => { return ss.length >= 10 }).length : 0}</TableCell>
                      <TableCell>{(cc.ads_id.ads_type_id.ads_type == 'Daily Rewards Ads') ? cc.stream_count_daily.length : cc.stream_count_calling.length}</TableCell>
                    </TableRow>)
                  })}
                </TableBody>
              </Table>
            </Box>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Payments
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Payment Id</TableCell>
                    <TableCell>Budget</TableCell>
                    <TableCell>Budget Type</TableCell>
                    <TableCell>Start Date</TableCell>
                    <TableCell>End Date</TableCell>
                    <TableCell>Payment Method</TableCell>
                    <TableCell>Total Amount</TableCell>
                    <TableCell>Date</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {datas.payment_data.map((item) => {
                    return (<TableRow>
                      <TableCell>{item._id}</TableCell>
                      {item.budgetDetail.filter((cc) => (cc._id == datas.budget_id)).map((val) => {
                        return (
                          <React.Fragment>
                            <TableCell>
                              {val.budget}
                            </TableCell>
                            <TableCell>{val.budgetType}</TableCell>
                            <TableCell>{val.startDate}</TableCell>
                            <TableCell>{val.endDate}</TableCell>
                          </React.Fragment>
                        )
                      })}
                      <TableCell>{item.payment_method}</TableCell>
                      <TableCell>{item.Amount}</TableCell>
                      <TableCell>{moment(item.createdAt).format('LLLL')}</TableCell>
                    </TableRow>)
                  })}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

export default React.memo(CampaignRequestBody)
