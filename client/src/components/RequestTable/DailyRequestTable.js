import React, { useEffect, useState, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { getDailyRequests } from '../../redux/actions';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
import NewDelete from '../UsableComponent/NewDelete';
import DailyRequestBody from './DailyRequestBody';
import useData from '../UsableComponent/sortTable/customFunction';
import TableHeadComp from '../UsableComponent/sortTable/TableHeadComp';
import LeafletMap from '../UsableComponent/LeafletMap';
import { LayersControl, Marker, Popup } from "react-leaflet";
import ButtonMode from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';
import Card from '../../newviews/Card/Card';
import useTable from '../UsableComponent/TableSorting/useTable';
import TblPagination from '../UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const headCells = [
  { id: '', label: '' },
  { id: 'request_id', label: 'Request Id' },
  { id: 'op_location', label: 'Operator Location' },
  { id: 'req_ip', label: 'Request Ip' },
  { id: 'campaign_name', label: 'Campaign Name' },
  { id: 'campaign_type', label: 'Campaign Type' },
  { id: 'total_count', label: 'Total Count' },
  { id: 'click_count', label: 'Total Click' },
  { id: 'completed', label: 'Completed' },
  { id: 'createdAt', label: 'Created At' },
];

const HeaderArr = []

const DailyRequestTable = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [element, setElement] = useState([])

  const dailyRequest = useSelector(state => state.auth.dailyTable)
  const totalCount = useSelector(state => state.auth.dailyTableTotal)
  const [flag, setFlag] = useState(false)
  const [map, setMap] = useState(false);
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== element.length) {
      dispatch(getDailyRequests(element.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {

    if (Object.keys(dailyRequest).length === 0) {
      dispatch(getDailyRequests(0, 10));
    } else {
      setElement(dailyRequest.daily_data);
      setFlag(true)
    }
  }, [dailyRequest])

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(element, headCells, filterFn)

  const handleRefresh = () => {
    dispatch(getDailyRequests(0, 10))
  }

  return (
    <GridContainer>
      {map ? (element.length > 0) ? <LeafletMap>
        {element.filter((cc) => (Object.keys(cc.request_data.ipDetail).length > 0)).map((val) => {
          return <LayersControl.Overlay checked name={val.key_data}>
            <Marker position={[val.request_data.ipDetail.latitude, val.request_data.ipDetail.longitude]}>
              <Popup>
                {val.response_data.name} <br /> {val.click_count.length}
              </Popup>
            </Marker>
          </LayersControl.Overlay>
        })}
      </LeafletMap> : null : null}
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}><NewDelete name="All Daily Rewards Request" length={HeaderArr} data={HeaderArr} /></h4>
            <ButtonMode color="primary" round onClick={() => { handleRefresh() }} >Refresh</ButtonMode>
            {element.length > 0 ? <ButtonMode color="primary" round onClick={() => { setMap(!map) }} >{map ? 'Hide Map' : 'Show Map'}</ButtonMode> : null}
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              {flag ? (element.length > 0 ? (<TableBody>
                {recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage)
                  .map((row, index) => {
                    return (
                      <DailyRequestBody key={row._id} datas={row} />
                    );
                  })}
              </TableBody>) : (<TableBody>
                <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <NoData />
                  </TableCell>
                </TableRow>
              </TableBody>)) : (<TableBody>
                <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>
              </TableBody>)}
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default DailyRequestTable


{/* <TableContainer component={Paper}>
          <Table className={classes.table} size="small" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell>Request Id</TableCell>
                <TableCell>Operator Location</TableCell>
                <TableCell>Request Ip</TableCell>
                <TableCell>Campaign Name</TableCell>
                <TableCell>Campaign Type</TableCell>
                <TableCell>Total Count</TableCell>
                <TableCell>Date Time</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {flag ? (
                (element.length === 0) ? <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <NoData />
                  </TableCell>
                </TableRow> : element.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((datas) => {
                  return (
                    <DailyRequestBody key={datas._id} datas={datas} />
                  )
                })
              ) : <TableRow>
                <TableCell align="center" colSpan={15}>
                  <Loader />
                </TableCell>
              </TableRow>}
            </TableBody>
          </Table>
        </TableContainer>
        {element.length > 0 ? <TablePagination
          rowsPerPageOptions={[5, 15, 25, 35]}
          component="div"
          count={element.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        /> : null} */}


        // <TableHeadComp alldata={NewArray} order={order} orderBy={orderBy} handleSelectAllClick={handleSelectAllClick} handleRequestSort={handleRequestSort} handleChangePage={handleChangePage} handleChangeRowsPerPage={handleChangeRowsPerPage} rowsPerPage={rowsPerPage} page={page} head={headCells} ss={ss}>
        //   {flag ? (element.length > 0 ? (<TableBody>
        //     {value
        //       .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        //       .map((row, index) => {
        //         return (
        //           <DailyRequestBody key={row._id} datas={row} />
        //         );
        //       })}
        //     {emptyRows > 0 && (
        //       <TableRow style={{ height: 53 * emptyRows }}>
        //         <TableCell colSpan={6} />
        //       </TableRow>
        //     )}
        //   </TableBody>) : (<TableBody>
        //     <TableRow>
        //       <TableCell align="center" colSpan={15}>
        //         <NoData />
        //       </TableCell>
        //     </TableRow>
        //   </TableBody>)) : (<TableBody>
        //     <TableRow>
        //       <TableCell align="center" colSpan={15}>
        //         <Loader />
        //       </TableCell>
        //     </TableRow>
        //   </TableBody>)}
        // </TableHeadComp>