import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TableBody, TableCell, TableRow } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllCampaign, ChangeTargetStatus } from '../redux/actions';
import { toast } from 'react-toastify';
import { StatusInput } from './utils/TargetStatus';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
import ChipComp from './UsableComponent/ChipComp';
import ButtonComp from './UsableComponent/ButtonComp';
// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import useTable from './UsableComponent/TableSorting/useTable';
import Button from '../newviews/CustomButtons/Button'
import TblPagination from './UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const headCells = [
  {
    id: "",
    numeric: false,
    disablePadding: false,
    label: "",
    disableSorting: true
  },
  {
    id: "name",
    numeric: false,
    disablePadding: false,
    label: "Target Name",
  },
  {
    id: "tstatus.name",
    numeric: false,
    disablePadding: false,
    label: "Status",
  },
  {
    id: "auth_id.start",
    numeric: false,
    disablePadding: false,
    label: "Campaign Status",
  },
  {
    id: "auth_id.active",
    numeric: false,
    disablePadding: false,
    label: "Campaign Active",
  },
  {
    id: "campaign_name",
    numeric: false,
    disablePadding: false,
    label: "Campaign Name",
  },
  {
    id: "campaign_type.campaign_name",
    numeric: false,
    disablePadding: false,
    label: "Campaign Type",
  },
  {
    id: "ads_id.ads_name",
    numeric: false,
    disablePadding: false,
    label: "Ads Name",
  },
  {
    id: "ads_id.ads_type_id.ads_name",
    numeric: false,
    disablePadding: false,
    label: "Ads Type",
  },
  {
    id: "audience_id.age",
    numeric: false,
    disablePadding: false,
    label: "Age",
  },
  {
    id: "audience_id.gender",
    numeric: false,
    disablePadding: false,
    label: "Gender",
  },
  {
    id: "audience_id.lsData",
    numeric: false,
    disablePadding: false,
    label: "Location",
  },
  {
    id: "audience_id.languages",
    numeric: false,
    disablePadding: false,
    label: "Language",
  },
  {
    id: "audience_id.inventory",
    numeric: false,
    disablePadding: false,
    label: "Inventory",
  },
  {
    id: "ads_id.trackingTemplate",
    numeric: false,
    disablePadding: false,
    label: "Tracking Template",
  },
  {
    id: "ads_id.video_id._id",
    numeric: false,
    disablePadding: false,
    label: "Video",
  },
]

const Target = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const history = useHistory()
  const [campaign, setCamapign] = useState([]);

  const AllCampaign = useSelector(state => state.auth.campaignDetail);
  const Updated = useSelector(state => state.auth.updated)
  const [load, setLoad] = useState(false)
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    // if (newPage >= 1) {
    //   dispatch(getAllAds(ads.length, 10))
    // }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }


  useEffect(() => {
    if (AllCampaign.length === 0) {
      dispatch(GetAllCampaign());
    } else {
      setCamapign(AllCampaign);
      setLoad(true)
    }

    if (Updated === true) {
      dispatch(GetAllCampaign());
    }

    return () => { setCamapign([]); }
  }, [AllCampaign, Updated])

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(campaign, headCells, filterFn)

  const ChangeStatusTarget = (e, data) => {

    var value
    var statusData

    StatusInput.map((cc) => {
      if (cc.name == e.target.value && cc.name == "Approve") {
        value = true
        statusData = {
          name: cc.name,
          type: cc.type
        }
      } else if (cc.name == e.target.value && cc.name != "Approve") {
        value = false
        statusData = {
          name: cc.name,
          type: cc.type
        }
      }
    })
    if (value) {
      if (data.ads_id.score_id.score == 0) {
        return toast.error('First Set Ads Score then it will change')
      }
    }
    const ssData = {
      status: statusData
    }
    console.log('Status Data', ssData)
    dispatch(ChangeTargetStatus(data._id, ssData))
  }

  const handleRefresh = () => {
    dispatch(GetAllCampaign());
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Active Target Details</h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              <TableBody>
                {load ? (
                  (campaign.length === 0) ? <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <NoData />
                    </TableCell>
                  </TableRow> : recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((datas) => {
                    return <React.Fragment key={datas._id}>
                      {datas.target_id.map((target) => {
                        return <TableRow key={target._id}>
                          <TableCell></TableCell>
                          <TableCell>{target.name}</TableCell>
                          <TableCell><select style={{ margin: '6px', padding: '6px', borderRadius: '11px' }} value={target._id ? target.tstatus.name : null} onChange={(e) => { ChangeStatusTarget(e, target) }}>
                            {StatusInput.map((item) => {
                              return (
                                <option key={item.id} value={item.name}>{item.name}</option>
                              )
                            })}
                          </select>
                          </TableCell>
                          <TableCell>
                            <ButtonComp value={datas.auth_id.start} />
                          </TableCell>
                          <TableCell>
                            <ButtonComp value={datas.auth_id.active} />
                          </TableCell>
                          <TableCell>{datas.campaign_name}</TableCell>
                          <TableCell>{datas.campaign_type_id.campaign_name}</TableCell>
                          <TableCell>{(!target.ads_id.ads_name || target.ads_id.ads_name == null || target.ads_id.ads_name == undefined) ? target.ads_id.video_id.title : target.ads_id.ads_name}</TableCell>
                          <TableCell>{target.ads_id.ads_type_id.ads_name}</TableCell>
                          <TableCell>{target.audience_id.age.map((data) => {
                            return <ChipComp val={data.name} />
                          })}</TableCell>
                          <TableCell>{target.audience_id.gender.map((vv) => {
                            return <ChipComp val={vv.name} />
                          })}</TableCell>
                          <TableCell>{target.audience_id.lsData.map((cc) => {
                            return <ChipComp val={cc.name} />
                          })}</TableCell>
                          <TableCell>{target.audience_id.languages.map((cc) => {
                            return <ChipComp val={cc.id.map((vv) => { return vv.name + ' ' })} />
                          })} </TableCell>
                          <TableCell>{target.audience_id.inventory.map((cc) => {
                            return <ChipComp val={cc.id.map((vv) => { return vv.name + ' ' })} />
                          })}</TableCell>
                          <TableCell>{target.ads_id.trackingTemplate}</TableCell>
                          <TableCell style={{ cursor: 'pointer' }} onClick={() => { history.push('/dashboard/videos') }}>{target.ads_id.video_id._id}</TableCell>
                        </TableRow>
                      })}
                    </React.Fragment>
                  })
                ) : <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>}
              </TableBody>
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={campaign.length}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default Target
