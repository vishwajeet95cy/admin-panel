import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import NewDelete from '../UsableComponent/NewDelete';
import { getAllUserUpdates } from '../../redux/actions';
import Loader from '../UsableComponent/Loader';
import NoData from '../UsableComponent/NoData';
import moment from 'moment';
import ButtonComp from '../UsableComponent/ButtonComp';

// New View
import Button from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from '../../newviews/Card/Card';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const HeaderArr = []

const UserUpdate = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const classes = useStyles();
  const [element, setElement] = useState([])

  const AllUserUpdate = useSelector(state => state.auth.webUserUpdate)
  const [flag, setFlag] = useState(false)



  const [numSelected, setnumSelected] = useState(0)

  useEffect(() => {

    if (AllUserUpdate.length === 0) {
      dispatch(getAllUserUpdates());
    } else {
      setElement(AllUserUpdate.update);
      setFlag(true)
    }

  }, [AllUserUpdate])

  const handleRefresh = () => {
    dispatch(getAllUserUpdates())
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite} ><NewDelete name="All Web User Update Details" length={numSelected} data={HeaderArr} /></h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer component={Paper}>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell >ID</TableCell>
                    <TableCell>User Id</TableCell>
                    <TableCell>User Name</TableCell>
                    <TableCell>Update Ids</TableCell>
                    <TableCell>Message</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>Created</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (element.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : element.map((datas) => {
                      return (
                        <TableRow key={datas._id}>
                          <TableCell component="th" scope="row">{datas._id}</TableCell>
                          <TableCell>{datas.user_id._id}</TableCell>
                          <TableCell>{datas.user_id?.profile.name}</TableCell>
                          <TableCell>{datas.actual_data?._id}</TableCell>
                          <TableCell>{datas.message}</TableCell>
                          <TableCell>
                            <ButtonComp value={datas.status} />
                          </TableCell>
                          <TableCell>{moment(datas.createdAt).format('LLLL')}</TableCell>
                        </TableRow>
                      )
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default UserUpdate
