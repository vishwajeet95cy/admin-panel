import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button, Card, Container, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { CreateCampaignType } from '../../redux/actions';

const useStyles = makeStyles({
  ButtonStyles: {
    marginBottom: "5px",
    marginLeft: "7px",
  },
  TypographyStyles: {
    textAlign: "center",
    padding: "5px",
  },
  textField: {
    padding: "5px",
    display: 'flex',
    margin: "24px"
  },
})

const AddCampaignType = () => {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState({
    'name': '',
    'type': '',
    'item': 0,
    'item_price': 0,
    'minimum_items': 0
  })

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const SubmitData = () => {
    console.log(data)

    dispatch(CreateCampaignType(data))
  }

  const Create = useSelector(state => state.auth.created)
  const LoadingTrue = useSelector(state => state.auth.loader);

  useEffect(() => {
    if (Create) {
      history.push('/dashboard/campaigntype')
    }
  }, [Create])

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xl" >
        <div className={classes.ContainerStyles} >
          <Card >
            <Typography className={classes.TypographyStyles} component="h1" variant="h5">
              Create Campaign Type
            </Typography>
            <form id="form-new">
              <TextField
                variant="outlined"
                margin="normal"
                required
                className={classes.textField}
                id="name"
                label="Enter Campaign Type Name"
                name="name"
                autoComplete="name"
                autoFocus
                onChange={handleChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                className={classes.textField}
                name="type"
                label="Enter Campaign Type"
                type="text"
                id="type"
                autoComplete="type"
                onChange={handleChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                className={classes.textField}
                name="item"
                label="Enter Item"
                type="number"
                id="item"
                autoComplete="item"
                onChange={handleChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                className={classes.textField}
                name="item_price"
                label="Enter Item Price"
                type="number"
                id="item_price"
                autoComplete="item_price"
                onChange={handleChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                className={classes.textField}
                name="minimum_items"
                label="Enter Minimum Items"
                type="number"
                id="minimum_items"
                autoComplete="minimum_items"
                onChange={handleChange}
              />
              <Button
                type="button"
                variant="contained"
                color="primary"
                className={classes.ButtonStyles}
                onClick={() => { history.push('/dashboard/campaigntype') }}
              >
                Back
              </Button>
              <Button
                type="button"
                variant="contained"
                color="primary"
                disabled={LoadingTrue ? true : false}
                className={classes.ButtonStyles}
                onClick={SubmitData}
              >
                Submit
              </Button>
            </form>
          </Card>
        </div>
      </Container>
    </React.Fragment>
  )
}

export default AddCampaignType
