import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, IconButton, Tooltip, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Box, Chip } from '@material-ui/core';
import { Close, Edit } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllLandings, UpdateLandings } from '../../redux/actions';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
// new views
import GridItem from '../../newviews/Grid/GridItem'
import GridContainer from "../../newviews/Grid/GridContainer.js";
import Card from "../../newviews/Card/Card";
import CardHeader from "../../newviews/Card/CardHeader.js";
import CardBody from "../../newviews/Card/CardBody.js";
import Button from '../../newviews/CustomButtons/Button'
import TblPagination from '../UsableComponent/TableSorting/TblPagination';
import TooltipComp from '../UsableComponent/TooltipComp';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const Landing = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [data, setData] = useState([])
  const [element, setElement] = useState({
    "score": 0,
    "id": ''
  })

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const AllLandings = useSelector(state => state.auth.allScore)
  const totalCount = useSelector(state => state.auth.scoreTotal)
  const Updated = useSelector(state => state.auth.updated);
  const LoadingTrue = useSelector(state => state.auth.loader);
  const [flag, setFlag] = useState(false)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== data.length) {
      dispatch(GetAllLandings(data.length, 10));
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (AllLandings.length === 0) {
      dispatch(GetAllLandings(0, 10));
    } else {

      setData(AllLandings);
      setFlag(true)
    }
    if (Updated) {
      dispatch(GetAllLandings());
      setOpen(false)
    }
    return () => { setData([]); }
  }, [AllLandings, Updated])

  const handleEdit = (e, data) => {
    e.preventDefault()
    setOpen(true);
    setElement({ score: data.score, id: data._id })
  }

  const handleChange = (e) => {
    setElement({ ...element, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    dispatch(UpdateLandings(element));
  }

  const handleRefresh = () => {
    dispatch(GetAllLandings(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              All Score Details
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Ads Name</TableCell>
                    <TableCell>Ads Type</TableCell>
                    <TableCell>Video Title</TableCell>
                    <TableCell>Score</TableCell>
                    <TableCell>Heading</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>Call To Action</TableCell>
                    <TableCell>Tracking Template</TableCell>
                    <TableCell>Visibility</TableCell>
                    <TableCell>Operation</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((item) => {
                      return <TableRow key={item._id}>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ? item.ads_id.video_id.title : item.ads_id.ads_name}</TableCell>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ? item.ads_id.ads_type_id.ads_name : null}</TableCell>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ? item.ads_id.video_id.title : null}</TableCell>
                        <TableCell>{item.score}</TableCell>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ?
                          <TooltipComp value={item.ads_id.heading} >
                            <Chip label="Heading" />
                          </TooltipComp> : null}</TableCell>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ? <TooltipComp value={item.ads_id.description} >
                          <Chip label="description" />
                        </TooltipComp> : null}</TableCell>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ? item.ads_id.callToAction : null}</TableCell>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ? <Chip component="a" href={item.ads_id.trackingTemplate} target="_blank" label="Click Me" /> : null}</TableCell>
                        <TableCell>{(!item.ads_id || item.ad_id == null || item.ads_id == undefined) ? item.ads_id.video_id.video_status : null}</TableCell>
                        <TableCell><Tooltip title="Edit">
                          <IconButton onClick={(e) => { handleEdit(e, item) }}>
                            <Edit color="primary"></Edit>
                          </IconButton>
                        </Tooltip></TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Update Score</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="score"
            name="score"
            value={element.score}
            label="Enter Score"
            type="number"
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Back
          </Button>
          <Button onClick={handleSubmit} disabled={LoadingTrue ? true : false} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  )
}

export default Landing
