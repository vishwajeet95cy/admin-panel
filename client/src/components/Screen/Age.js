import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import EditButton from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { Close } from '@material-ui/icons';
import { Box, Grid } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Delete_Age, GetAllAge, UpdateAge } from '../../redux/actions';
import NewDelete from '../UsableComponent/NewDelete';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';

// New View
import Button from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from '../../newviews/Card/Card';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';
import TblPagination from '../UsableComponent/TableSorting/TblPagination';


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const Age = () => {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([])
  const [element, setElement] = useState({
    "name": '',
    "type": '',
    "id": ''
  })
  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleEdit = (e, data) => {
    e.preventDefault()
    setOpen(true);
    setElement({ name: data.name, type: data.type, id: data._id })
  }

  const handleChange = (e) => {
    setElement({ ...element, [e.target.name]: e.target.value })
  }

  const AllAge = useSelector(state => state.auth.ageDetail)
  const totalCount = useSelector(state => state.auth.ageTotal)
  const Updates = useSelector(state => state.auth.updated)
  const Create = useSelector(state => state.auth.created)
  const LoadingTrue = useSelector(state => state.auth.loader);
  const [flag, setFlag] = useState(false)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== data.length) {
      dispatch(GetAllAge(data.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (AllAge.length === 0) {
      dispatch(GetAllAge(0, 10));
    } else {

      setData(AllAge)
      setFlag(true)
    }
    if (Updates) {
      dispatch(GetAllAge(0, 10));
      setnumSelected(0)
      setId([])
      setOpen(false)
    }
    if (Create) {
      dispatch(GetAllAge(0, 10));
      setOpen(false)
    }
    return () => { setData([]); setId([]); }
  }, [AllAge, Updates, Create])

  const handleSubmit = () => {
    dispatch(UpdateAge(element));
  }

  const addAgeId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const handleDeleteButton = () => {
    dispatch(Delete_Age({ id: id }))
  }

  const handleRefresh = () => {
    dispatch(GetAllAge(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}><NewDelete name="All Age" length={numSelected} click={LoadingTrue ? null : handleDeleteButton} /></h4>
            <Button color="primary" variant="contained" round onClick={() => { handleRefresh() }}>Refresh</Button>
            <Button color="primary" variant="contained" round onClick={() => { history.push('/dashboard/addages') }}>Create</Button>
          </CardHeader>
          <CardBody>
            <TableContainer component={Paper} className={classes.container}>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Type</TableCell>
                    <TableCell>Edit</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>

                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((item) => {
                      return <TableRow key={item._id}>
                        <TableCell>
                          <input
                            type="checkbox"
                            className="ageList"
                            onClick={(e) => addAgeId(e)}
                            id={item._id}
                            name={item._id}
                            value={item._id}
                          />
                        </TableCell>
                        <TableCell>{item.name}</TableCell>
                        <TableCell>{item.type}</TableCell>
                        <TableCell><Tooltip title="Edit">
                          <IconButton onClick={(e) => { handleEdit(e, item) }}>
                            <EditButton color="primary"></EditButton>
                          </IconButton>
                        </Tooltip></TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Update Age</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            name="name"
            value={element.name}
            label="Enter Age Name"
            type="text"
            fullWidth
            onChange={handleChange}
          />
          <TextField
            autoFocus
            margin="dense"
            id="type"
            value={element.type}
            name="type"
            label="Enter Age Type"
            type="text"
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Back
          </Button>
          <Button onClick={handleSubmit} disabled={LoadingTrue ? true : false} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  )
}

export default Age
