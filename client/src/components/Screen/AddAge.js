import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button, Card, Container, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { CreateAge } from '../../redux/actions';

const useStyles = makeStyles({
  ButtonStyles: {
    marginBottom: "5px",
    marginLeft: "7px",
  },
  TypographyStyles: {
    textAlign: "center",
    padding: "5px",
  },
  textField: {
    padding: "5px",
    display: 'flex',
    margin: "24px"
  },
})

const AddAge = () => {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState({
    'name': '',
    'type': ''
  })

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const SubmitData = () => {
    console.log(data)
    dispatch(CreateAge(data))
  }

  const Create = useSelector(state => state.auth.created)
  const LoadingTrue = useSelector(state => state.auth.loader);

  useEffect(() => {
    if (Create) {
      history.push('/dashboard/ages')
    }
  }, [Create])

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xl" >
        <div className={classes.ContainerStyles} >
          <Card >
            <Typography className={classes.TypographyStyles} component="h1" variant="h5">
              Create Age
            </Typography>
            <form id="form-new">
              <TextField
                variant="outlined"
                margin="normal"
                required
                className={classes.textField}
                id="name"
                label="Enter Age Name"
                name="name"
                autoComplete="name"
                autoFocus
                onChange={handleChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                className={classes.textField}
                name="type"
                label="Enter Age Type"
                type="text"
                id="type"
                autoComplete="type"
                onChange={handleChange}
              />
              <Button
                type="button"
                variant="contained"
                color="primary"
                className={classes.ButtonStyles}
                onClick={() => { history.push('/dashboard/ages') }}
              >
                Back
              </Button>
              <Button
                type="button"
                variant="contained"
                color="primary"
                disabled={LoadingTrue ? true : false}
                className={classes.ButtonStyles}
                onClick={SubmitData}
              >
                Submit
              </Button>
            </form>
          </Card>
        </div>
      </Container>
    </React.Fragment>
  )
}

export default AddAge
