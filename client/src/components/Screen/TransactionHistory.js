import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllTransaction } from '../../redux/actions';
import moment from 'moment'
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
// new views
import GridItem from '../../newviews/Grid/GridItem'
import GridContainer from "../../newviews/Grid/GridContainer.js";
import Card from "../../newviews/Card/Card";
import CardHeader from "../../newviews/Card/CardHeader.js";
import CardBody from "../../newviews/Card/CardBody.js";
import Button from '../../newviews/CustomButtons/Button'

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const TransactionHistory = () => {

  const classes = useStyles();
  const [transaction, setTransaction] = useState([])
  const dispatch = useDispatch()
  const TransactionData = useSelector(state => state.auth.allTrasaction)
  const [flag, setFlag] = useState(false)

  useEffect(() => {
    if (TransactionData.length === 0) {
      dispatch(GetAllTransaction());
    } else {
      setTransaction(TransactionData)
      setFlag(true)
    }
    return () => { setTransaction([]) }
  }, [TransactionData])

  const handleRefresh = () => {
    dispatch(GetAllTransaction())
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Transaction History</h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Campaign Name</TableCell>
                    <TableCell>Campaign Type</TableCell>
                    <TableCell>Ads Type</TableCell>
                    <TableCell>Transaction Id</TableCell>
                    <TableCell>Amount</TableCell>
                    <TableCell>Payment Invoice</TableCell>
                    <TableCell>Payment Method</TableCell>
                    <TableCell>Payment Status</TableCell>
                    <TableCell>Payment Date</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (transaction.length == 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : transaction.map((cc) => {
                      return <React.Fragment key={cc._id}>
                        {cc.campaign_id.map((dd) => {
                          return <TableRow key={dd._id}>
                            <TableCell>{dd.campaign_name}</TableCell>
                            <TableCell>{dd.campaign_type_id.campaign_name}</TableCell>
                            <TableCell>{dd.target_id.map((target) => {
                              return <p>{target.ads_id.ads_type_id.ads_name}</p>
                            })}</TableCell>
                            <TableCell>{cc.payment_id}</TableCell>
                            <TableCell>&#x20B9;{cc.Amount}</TableCell>
                            <TableCell><a href={cc.payment_url} target="_blanck" style={{ textDecoration: 'none' }}>Click</a></TableCell>
                            <TableCell>{cc.payment_method}</TableCell>
                            <TableCell>{cc.payment_status}</TableCell>
                            <TableCell>{moment(cc.createdAt).format('LLLL')}</TableCell>
                          </TableRow>
                        })}
                      </React.Fragment>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )

}

export default TransactionHistory
