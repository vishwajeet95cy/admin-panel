import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import EditButton from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { Close } from '@material-ui/icons';
import { Box } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Delete_Status, GetAllStatus, UpdateStatus } from '../../redux/actions';
import moment from 'moment';
import Loader from '../UsableComponent/Loader';
import NoData from '../UsableComponent/NoData';
import NewDelete from '../UsableComponent/NewDelete';

const useStyles = makeStyles({
  table: {
    minWidth: 400,
  },
  container: {
    margin: 5
  }
});

const Status = () => {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([])
  const [element, setElement] = useState({
    "name": '',
    "id": ''
  })

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const AllStatus = useSelector(state => state.auth.statusDetail);
  const Updated = useSelector(state => state.auth.updated);
  const Create = useSelector(state => state.auth.created)
  const [flag, setFlag] = useState(false)

  useEffect(() => {
    if (AllStatus.length === 0) {
      dispatch(GetAllStatus());
    } else {
      setData(AllStatus.statusData);
      setFlag(true)
    }
    if (Updated) {
      dispatch(GetAllStatus());
      setnumSelected(0)
      setId([])
      setOpen(false)
    }
    if (Create) {
      dispatch(GetAllStatus());
    }
    return () => { setData([]); }
  }, [AllStatus, Updated, Create])

  const handleEdit = (e, data) => {
    e.preventDefault()
    setOpen(true);
    setElement({ name: data.name, id: data._id })
  }

  const handleChange = (e) => {
    setElement({ ...element, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    dispatch(UpdateStatus(element));
  }

  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)

  const addStatusId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const handleDeleteButton = () => {
    dispatch(Delete_Status({ id: id }))
  }

  return (
    <React.Fragment>
      <div>
        <Button color="primary" variant="contained" onClick={() => { history.push('/dashboard/addstatus') }}>Create</Button>
      </div>

      <div>
        <NewDelete name="All Status" length={numSelected} click={handleDeleteButton} />
        <TableContainer component={Paper} className={classes.container}>
          <Table className={classes.table} size="small" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Created</TableCell>
                <TableCell>Operation</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {flag ? (
                (data.length === 0) ? <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <NoData />
                  </TableCell>
                </TableRow> : data.map((item) => {
                  return <TableRow key={item._id}>
                    <TableCell>
                      <input
                        type="checkbox"
                        className="ageList"
                        onClick={(e) => addStatusId(e)}
                        id={item._id}
                        name={item._id}
                        value={item._id}
                      />
                    </TableCell>
                    <TableCell>{item.name}</TableCell>
                    <TableCell>{moment(item.createdAt).format('lll')}</TableCell>
                    <TableCell><Tooltip title="Edit">
                      <IconButton onClick={(e) => { handleEdit(e, item) }}>
                        <EditButton color="primary"></EditButton>
                      </IconButton>
                    </Tooltip></TableCell>
                  </TableRow>
                })
              ) : <TableRow>
                <TableCell align="center" colSpan={15}>
                  <Loader />
                </TableCell>
              </TableRow>}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Update Status</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            name="name"
            value={element.name}
            label="Enter Status Name"
            type="text"
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Back
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}

export default Status
