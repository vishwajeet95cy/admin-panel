import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button, Container, Typography, CssBaseline } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { CreateLanguage } from '../../redux/actions';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
})
)
const AddLanguage = () => {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState({
    'name': '',
    'type': ''
  })

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const SubmitData = () => {
    console.log(data)
    dispatch(CreateLanguage(data))
  }

  const Create = useSelector(state => state.auth.created)
  const LoadingTrue = useSelector(state => state.auth.loader);

  useEffect(() => {
    if (Create) {
      history.push('/dashboard/language')
    }
  }, [Create])

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Create Language
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              id="name"
              label="Enter Language Name"
              name="name"
              autoComplete="name"
              autoFocus
              fullWidth
              onChange={handleChange}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              required
              name="type"
              label="Enter Language Type"
              type="text"
              id="type"
              autoComplete="type"
              onChange={handleChange}
            />
            <Button
              type="button"
              variant="contained" fullWidth color="secondary"
              onClick={() => { history.push('/dashboard/language') }}
            >
              Back
            </Button>
            <Button
              type="button"
              disabled={LoadingTrue ? true : false}
              variant="contained" fullWidth color="primary" className={classes.submit}
              onClick={SubmitData}
            >
              Submit
            </Button>
          </form>
        </div>
      </Container>
    </React.Fragment>
  )
}

export default AddLanguage
