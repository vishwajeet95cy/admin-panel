import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { getAllAds } from '../redux/actions';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import Button from '../newviews/CustomButtons/Button'
import TblPagination from './UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const LandingPage = () => {

  const classes = useStyles();
  const [landing, setLanding] = useState([])
  const dispatch = useDispatch()
  const LandingData = useSelector(state => state.auth.adsDetail)
  const [flag, setFlag] = useState(false)
  const totalCount = useSelector(state => state.auth.adsTotal)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount != landing.length) {
      dispatch(getAllAds(landing.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (LandingData.length === 0) {
      dispatch(getAllAds(0, 10));
    } else {
      setLanding(LandingData)
      setFlag(true)
    }
    return () => { setLanding([]) }
  }, [LandingData])

  const handleRefresh = () => {
    dispatch(getAllAds(0, 10));
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Landing Details</h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Tracking Url</TableCell>
                    <TableCell>CallToAction</TableCell>
                    <TableCell>Final Url Suffix</TableCell>
                    <TableCell>Impr.</TableCell>
                    <TableCell>Views</TableCell>
                    <TableCell>View rate</TableCell>
                    <TableCell>Avg. CPV</TableCell>
                    <TableCell>Cost</TableCell>
                    <TableCell>Conversions</TableCell>
                    <TableCell>Cost / Conv.</TableCell>
                    <TableCell>Conv. rate</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (landing.length == 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : landing.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((data) => {
                      return (
                        <TableRow key={data._id}>
                          <TableCell ><a href={data.trackingTemplate} target="_blanck">Click to Visit</a></TableCell>
                          <TableCell>
                            {data.callToAction}
                          </TableCell>
                          <TableCell>{data.finalURLSuffix}</TableCell>
                          <TableCell>0</TableCell>
                          <TableCell>0</TableCell>
                          <TableCell>--</TableCell>
                          <TableCell>--</TableCell>
                          <TableCell>&#8377;0.00</TableCell>
                          <TableCell>0.00</TableCell>
                          <TableCell>&#8377;0.00</TableCell>
                          <TableCell>0.00%</TableCell>
                        </TableRow>
                      )
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )

}

export default LandingPage
