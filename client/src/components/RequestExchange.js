import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { Close } from '@material-ui/icons';
import { Box } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllExchanges, UpdateExchanges, updateExchangeStatus } from '../redux/actions';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import { ExchangeStatus } from './utils/TargetStatus';
import Button from '../newviews/CustomButtons/Button'
import TblPagination from './UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const RequestExchange = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [element, setElement] = useState([])
  const ExchangeDetails = useSelector(state => state.auth.allExchanges)
  const totalCount = useSelector(state => state.auth.exchangeTotal)
  const Updated = useSelector(state => state.auth.updated)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== element.length) {
      dispatch(GetAllExchanges(element.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  const [data, setData] = useState({
    id: '',
    upi_id: '',
    amount: 0
  })

  const [flag, setFlag] = useState(false)

  useEffect(() => {

    if (ExchangeDetails.length === 0) {
      dispatch(GetAllExchanges(0, 10));
    } else {
      setElement(ExchangeDetails)
      setFlag(true)
    }

    if (Updated) {
      dispatch(GetAllExchanges(0, 10));
      setOpen(false)
    }
    return () => { setElement([]); }
  }, [ExchangeDetails, Updated])

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleEdit = (e, data) => {
    e.preventDefault()
    setOpen(true);
    setData({ upi_id: data.upi_id, id: data._id })
  }

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {

  }

  const handleStatus = (e, data) => {
    console.log('values', e.target.value, 'mai', ExchangeStatus[e.target.value], 'id', data)
    var status = false
    if (e.target.value == 'Pending' || e.target.value == 'Failed') {
      status = false
    } else {
      status = true
    }

    console.log('data for update', {
      tstatus: ExchangeStatus[e.target.value],
      status: status
    })

    dispatch(updateExchangeStatus(data, {
      tstatus: ExchangeStatus[e.target.value],
      status: status
    }))
  }

  const handleRefresh = () => {
    dispatch(GetAllExchanges(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              All Request Exchnages
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    {/* <TableCell>Actions</TableCell> */}
                    <TableCell>User Id</TableCell>
                    <TableCell>Upi Id</TableCell>
                    <TableCell>Coin Unit</TableCell>
                    <TableCell>Coin Value</TableCell>
                    <TableCell>User Status</TableCell>
                    <TableCell>User Login Status</TableCell>
                    <TableCell>Contact Permission</TableCell>
                    <TableCell>Call Permission</TableCell>
                    <TableCell>Location Permission</TableCell>
                    <TableCell>Socket Status</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>Working Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (element.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : element.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((ex) => {
                      return <TableRow key={ex._id}>
                        {/* <TableCell><Button variant="contained" color="primary" onClick={(e) => { handleEdit(e, ex) }}>Add</Button></TableCell> */}
                        <TableCell>{ex.user_id._id}</TableCell>
                        <TableCell>{ex.upi}</TableCell>
                        <TableCell>{ex.coins_request?.unit}</TableCell>
                        <TableCell>{ex.coins_request?.value}</TableCell>
                        <TableCell>{(ex?.user_status?.user_status == true) ? 'true' : 'false'}</TableCell>
                        <TableCell>{(ex?.user_status?.user_login_status == true) ? 'true' : 'false'}</TableCell>
                        <TableCell >{(!ex.user_status || ex.user_status.permissions == null || ex.user_status.permissions == undefined) ? null : (ex.user_status.permissions[0].contact_permission === true) ? "true" : "false"}</TableCell>
                        <TableCell >{(!ex.user_status || ex.user_status.permissions == null || ex.user_status.permissions == undefined) ? null : (ex.user_status.permissions[0].call_permission === true) ? "true" : "false"}</TableCell>
                        <TableCell >{(!ex.user_status || ex.user_status.permissions == null || ex.user_status.permissions == undefined) ? null : (ex.user_status.permissions[0].location_permission === true) ? "true" : "false"}</TableCell>
                        <TableCell>{(ex?.user_status?.socketStatus == true) ? 'true' : 'false'}</TableCell>
                        <TableCell>{(ex?.status == true) ? 'true' : 'false'}</TableCell>
                        <TableCell>
                          <select value={ex?.tstatus} onChange={(e) => { handleStatus(e, ex._id) }}>
                            {Object.keys(ExchangeStatus).map((cc, i) => {
                              return <option key={i} value={cc} >{cc}</option>
                            })}
                          </select>
                        </TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Add Amount Exchange</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="upi_id"
            name="upi_id"
            value={data.upi_id}
            label="Upi Id"
            type="text"
            fullWidth
            onChange={handleChange}
          />
          <TextField
            autoFocus
            margin="dense"
            id="amount"
            value={data.amount}
            name="amount"
            label="Enter Amount"
            type="number"
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Back
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  )
}

export default RequestExchange
