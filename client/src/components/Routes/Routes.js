import Ads from '../Ads';
import Users from '../Users';
import Videos from '../Videos';
import Campaign from '../Campaign';
import Home from "../Home/Home";
import MobileUser from '../Users/MobileUser';
import WebUser from '../Users/WebUser';
import AddDailyRewards from '../AddDailyRewards';
import Gender from '../Screen/Gender';
import AddGender from '../Screen/AddGender';
import AddInterest from '../Screen/AddInterest';
import Interest from '../Screen/Interest';
import Occupation from '../Screen/Occupation';
import AddOccupation from '../Screen/AddOccupation';
import CampaignType from '../Screen/CampaignType';
import AdsType from '../Screen/AdsType';
import AddAdsType from '../Screen/AddAdsType';
import AddCampaignType from '../Screen/AddCampaignType';
import TransactionHistory from '../Screen/TransactionHistory';
import GenerateInvoice from '../Screen/GenerateInvoice';
import Rewards from '../Rewards';
import EditVideo from '../EditVideo';
import EditBudget from '../EditBudget';
import EditAudience from '../EditAudience';
import EditDailyRewards from '../EditDailyRewards';
import Age from '../Screen/Age';
import AddAge from '../Screen/AddAge';
import EditWebUser from '../Users/EditWebUser';
import Language from '../Screen/Language';
import AddLanguage from '../Screen/AddLanguage';
import LandingPage from '../LandingPage';
import EditAds from '../EditAds';
import EditMobilerUser from '../Users/EditMobilerUser';
import Stats from '../Stats';
import Score from '../Screen/Score';
import Target from '../Target';
import RequestExchange from '../RequestExchange';
import CallingRewardsRank from '../CallingRewardsRank';
import DailyRewardsRank from '../DailyRewardsRank';
import Status from '../Screen/Status';
import AddStatus from '../Screen/AddStatus';
import MultipleSelect from '../Testing';
import AdSchedule from '../Screen/AdSchedule';
import AddSchedule from '../Screen/AddSchedules';
import NotificationPanel from '../NotificationPanel';
import OnlineMarketing from '../WebFront/OnlineMarketing';
import AdsWork from '../WebFront/AdsWork';
import FormComponent from '../Form/FormComponent';
import OnlineForm from '../WebFront/OnlineForm';
import WorkForm from '../WebFront/WorkForm';
import EditOnlineForm from '../WebFront/EditOnlineForm';
import EditWorkForm from '../WebFront/EditWorkForm';
import NetworkLink from '../WebFront/NetworkLink';
import AddNetwork from '../WebFront/AddNetwork';
import EditNetwork from '../WebFront/EditNetwork';
import FooterList from '../WebFront/FooterList';
import AddFooter from '../WebFront/AddFooter';
import EditFooter from '../WebFront/EditFooter';
import IssueTypeList from '../WebFront/IssueTypeList';
import AddIssueType from '../WebFront/AddIssueType';
import EditIssueType from '../WebFront/EditIssueType';
import UserContact from '../WebFront/UserContact';
import PageCombination from '../Combinations Page/PageCombination';
import CallingRequestTable from '../RequestTable/CallingRequestTable';
import DailyRequestTable from '../RequestTable/DailyRequestTable';
import CampaignRequestTable from '../RequestTable/CampaignRequestTable';
import AllTarget from '../AllTarget';
import UserUpdate from '../Screen/UserUpdate';
import Queue from '../Queue';
import AdminSetting from '../AdminSetting/AdminSetting';
import UserSocketStatus from '../UserStatus/UserSocketStatus';

const routes = [
  {
    key: "Home",
    path: "/dashboard/",
    component: Home
  },
  {
    key: "Ads",
    path: '/dashboard/ads',
    component: Ads
  },
  {
    key: "EditAds",
    path: '/dashboard/editads',
    component: EditAds
  },
  {
    key: "Users",
    path: '/dashboard/users',
    component: Users
  },
  {
    key: "Videos",
    path: '/dashboard/videos',
    component: Videos
  },
  {
    key: "EditVideos",
    path: '/dashboard/editVideo',
    component: EditVideo
  },
  {
    key: "Campaign",
    path: '/dashboard/campaign',
    component: Campaign
  },
  {
    key: "MobileUser",
    path: '/dashboard/users/mobileuser',
    component: MobileUser
  },
  {
    key: "EditMobileUser",
    path: '/dashboard/users/mobileuser/:id',
    component: EditMobilerUser
  },
  {
    key: "WebUser",
    path: '/dashboard/users/webuser',
    component: WebUser
  },
  {
    key: "EditWebUser",
    path: '/dashboard/users/webuser/:id',
    component: EditWebUser
  },
  {
    key: "DailyRewards",
    path: '/dashboard/dailyrewards',
    component: Rewards
  },
  {
    key: "LandingPage",
    path: '/dashboard/landing',
    component: LandingPage
  },
  {
    key: "AddDailyRewards",
    path: '/dashboard/adddailyrewards',
    component: AddDailyRewards
  },

  {
    key: "EditDailyRewards",
    path: '/dashboard/editrewards',
    component: EditDailyRewards
  },
  {
    key: "Gender",
    path: '/dashboard/gender',
    component: Gender
  },
  {
    key: "Create Gender",
    path: '/dashboard/addgender',
    component: AddGender
  },
  {
    key: "Interest",
    path: '/dashboard/interest',
    component: Interest
  },
  {
    key: "Create Interest",
    path: '/dashboard/addinterest',
    component: AddInterest
  },
  {
    key: "Occupation",
    path: '/dashboard/occupation',
    component: Occupation
  },
  {
    key: "Create Occupation",
    path: '/dashboard/addoccupation',
    component: AddOccupation
  },
  {
    key: "CampaignTypes",
    path: '/dashboard/campaigntype',
    component: CampaignType
  },
  {
    key: "Create Campaign Types",
    path: '/dashboard/addcampaigntypes',
    component: AddCampaignType
  },
  {
    key: "AdsTypes",
    path: '/dashboard/adstype',
    component: AdsType
  },
  {
    key: "Create Ads Types",
    path: '/dashboard/addadstypes',
    component: AddAdsType
  },
  {
    key: "Ages",
    path: '/dashboard/ages',
    component: Age
  },
  {
    key: "Create Ages",
    path: '/dashboard/addages',
    component: AddAge
  },
  {
    key: "Language",
    path: '/dashboard/language',
    component: Language
  },
  {
    key: "Create Language",
    path: '/dashboard/addlanguage',
    component: AddLanguage
  },
  {
    key: "TransactionHistory",
    path: '/dashboard/history',
    component: TransactionHistory
  },
  {
    key: "Generate Invoice",
    path: '/dashboard/invoice',
    component: GenerateInvoice
  },
  {
    key: "Edit Campaign Budget",
    path: '/dashboard/editbudget',
    component: EditBudget
  },
  {
    key: "Edit Campaign Audience",
    path: '/dashboard/editaudience/:id',
    component: EditAudience
  },
  {
    key: "Stats",
    path: '/dashboard/stats',
    component: Stats
  },
  {
    key: "Score",
    path: '/dashboard/score',
    component: Score
  },
  {
    key: "Target",
    path: '/dashboard/target',
    component: Target
  },
  {
    key: 'Request Exchange',
    path: '/dashboard/exchange',
    component: RequestExchange
  },
  {
    key: 'Calling Rewards Rank',
    path: '/dashboard/callingrank',
    component: CallingRewardsRank
  },
  {
    key: 'Daily Rewards Rank',
    path: '/dashboard/dailyrank',
    component: DailyRewardsRank
  },
  {
    key: "Status",
    path: '/dashboard/status',
    component: Status
  },
  {
    key: "Create Status",
    path: '/dashboard/addstatus',
    component: AddStatus
  },
  {
    key: "Schedules",
    path: '/dashboard/schedules',
    component: AdSchedule
  },
  {
    key: "Create Schedules",
    path: '/dashboard/addSchedule',
    component: AddSchedule
  },
  {
    key: "Notifications",
    path: '/dashboard/notify',
    component: NotificationPanel
  },
  {
    key: 'Testing',
    path: "/dashboard/test",
    component: MultipleSelect
  },
  //Web Front ROutes
  {
    key: 'Online marketing',
    path: "/dashboard/online",
    component: OnlineMarketing
  },
  {
    key: 'Ads Work',
    path: "/dashboard/new",
    component: AdsWork
  },
  {
    key: 'Work Form',
    path: "/dashboard/workform",
    component: WorkForm
  },
  {
    key: 'Online Form',
    path: "/dashboard/onlineform",
    component: OnlineForm
  },
  {
    key: 'Edit Online From',
    path: "/dashboard/editOnline",
    component: EditOnlineForm
  },
  {
    key: 'Edit Work Form',
    path: "/dashboard/editWork",
    component: EditWorkForm
  },
  {
    key: 'Network List',
    path: "/dashboard/network",
    component: NetworkLink
  },
  {
    key: 'Network Form',
    path: "/dashboard/networkform",
    component: AddNetwork
  },
  {
    key: 'Edit Network From',
    path: "/dashboard/editNetwork",
    component: EditNetwork
  },
  {
    key: 'Footer List',
    path: "/dashboard/footer",
    component: FooterList
  },
  {
    key: 'Footer Form',
    path: "/dashboard/footerform",
    component: AddFooter
  },
  {
    key: 'Edit Footer From',
    path: "/dashboard/editFooter",
    component: EditFooter
  },
  {
    key: 'Issue Type List',
    path: "/dashboard/issue",
    component: IssueTypeList
  },
  {
    key: 'Issue Type Form',
    path: "/dashboard/issueform",
    component: AddIssueType
  },
  {
    key: 'Edit Issue Type From',
    path: "/dashboard/editIssue",
    component: EditIssueType
  },
  {
    key: 'User Contact request',
    path: "/dashboard/userContact",
    component: UserContact
  },
  {
    key: 'Combinations',
    path: "/dashboard/pages",
    component: PageCombination
  },
  {
    key: 'Calling Rewards Request',
    path: "/dashboard/callingTable",
    component: CallingRequestTable
  },
  {
    key: 'Daily Rewards Request',
    path: "/dashboard/dailyTable",
    component: DailyRequestTable
  },
  {
    key: 'Campaign Request',
    path: "/dashboard/campaignTable",
    component: CampaignRequestTable
  },
  {
    key: 'All Target',
    path: "/dashboard/allTarget",
    component: AllTarget
  },
  {
    key: 'Web User Updates',
    path: "/dashboard/webUpdate",
    component: UserUpdate
  },
  {
    key: 'Queues',
    path: "/dashboard/queue",
    component: Queue
  },
  {
    key: 'Admin Settings',
    path: '/dashboard/adminSetting',
    component: AdminSetting
  },
  {
    key: 'User Status',
    path: '/dashboard/users/userStatus',
    component: UserSocketStatus
  },
]


export default routes;