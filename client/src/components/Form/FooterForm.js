import { Grid, TextField, FormControl, FormLabel, Chip, makeStyles, Button } from '@material-ui/core';
import React, { useState } from 'react'
import { toast } from 'react-toastify';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

const FooterForm = ({ value, Static, addItem, removeItem, nopen, editLink }) => {

  const [data, setData] = useState(value)
  const classes = useStyles()
  const [id, setId] = useState(nopen ? nopen : '')

  const handleChange = (event) => {

    Static(event)
    if (id !== '') {
      let lsde = [...data.subitems];
      lsde[id] = { ...lsde[id], [event.target.name]: event.target.value }
      setData({ ...data, subitems: lsde });
      setData({ ...data, [event.target.name]: event.target.value });
    } else {
      setData({ ...data, [event.target.name]: event.target.value });
    }
  };

  const onSelect = (e) => {
    e.preventDefault()
    if (data.name == "") {
      return toast.error('Please Enter Name')
    } else if (data.link == "") {
      return toast.error('Please Enter Link')
    } else {
      let lsde = [...value.subitems];
      var cc = { name: data.name, link: data.link }
      if (!lsde.some((item) => item.name == cc.name)) {
        lsde.push(cc);
        setData({ ...data, subitems: lsde });
        addItem()
      } else {
        toast.error('Subitem Already Exist')
      }
      console.log(data.subitems, lsde);
    }
  };

  const onRemoveItem = (e, ind) => {
    setId('')
    setData({
      ...data,
      subitems: data.subitems.filter((inv, index) => index !== ind)
    });
    removeItem(e, ind)
  };

  const EditItem = (e, ind) => {
    const nvalue = value.subitems.filter((inv, index) => index == ind)
    setData({ ...data, name: nvalue[0].name, link: nvalue[0].link })
    setId(ind)
    editLink(e, ind)
  }

  const NewForm = () => {
    setData({ ...data, name: '', link: '' })
    setId('')
  }


  return (
    <form>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Main Title</FormLabel>
            <TextField
              required
              variant="outlined"
              id="maintitle"
              name="maintitle"
              type="text"
              placeholder="Enter Main Title"
              value={data.maintitle}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12} className={classes.root}>
          {(id !== '') ? <Chip onClick={NewForm} label="Add New" /> : null}
          {value.subitems.length == 0 ? (
            <span>Add your sub items..</span>
          ) : (
            value.subitems.map((item, index) => (
              <Chip
                key={index}
                label={item.name}
                onClick={e => EditItem(e, index)}
                onDelete={e => onRemoveItem(e, index)}
              />
            ))
          )}
        </Grid>
        <Grid item xs={4}>
          <FormControl fullWidth>
            <FormLabel>Sub Item Name</FormLabel>
            <TextField
              required
              variant="outlined"
              id="name"
              name="name"
              type="text"
              placeholder="Enter Sub Item Name"
              value={data.name}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={4}>
          <FormControl fullWidth>
            <FormLabel>Sub Item Link</FormLabel>
            <TextField
              required
              variant="outlined"
              id="link"
              name="link"
              type="text"
              placeholder="Enter Sub Item Link"
              value={data.link}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={4} style={{ position: 'relative', top: '24px' }}>
          <Button type="button" color="primary" disabled={(id !== '') ? true : false} fullWidth variant='contained' onClick={(e) => { onSelect(e) }}>Add</Button>
        </Grid>
      </Grid>
    </form>
  )
}

export default FooterForm
