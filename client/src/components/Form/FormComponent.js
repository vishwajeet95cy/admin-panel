import { Grid, TextField, FormControl, FormLabel, Typography, Button, Container, Card, CardContent, Dialog, DialogContent } from '@material-ui/core';
import React from 'react'
import { useState } from 'react'
import { TextareaAutosize } from '@material-ui/core';
import { toast } from 'react-toastify';
import NewCropper from './ImageCropper';

const FormComponent = ({ value, Static, ImageChange, ImageChangeN, imgVal, imgVal2, ImageCrop, ImageCropN }) => {

  const [img, setImg] = useState(imgVal ? imgVal : '')
  const [image, setImage] = useState(imgVal2 ? imgVal2 : '')
  const [imageFile, setImagefile] = useState()
  const [imageFiles, setImagefiles] = useState()
  const [data, setData] = useState(value)

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
    Static(event)
  };

  const [open, setOpen] = useState(false)
  const [load, setLoad] = useState(false)

  const onThumb = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;

    console.log('files', e.target.files[0])
    if (e.target.files[0].size > 30720000) {
      setImg('')
      setImagefile()
      return toast.error("File size too big! Only files less than 30 MB are allowed");
    } else if (!e.target.files[0].name.match(allowedExtensions)) {
      setImg('')
      setImagefile()
      return toast.error("Only jpeg and png files are allowed!");
    } else {
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImg(fileURL)
      setImagefile(e.target.files[0])
      ImageChange(e)
    }
  };

  const onThumbN = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;

    console.log('files', e.target.files[0])
    if (e.target.files[0].size > 30720000) {
      setImage('')
      setImagefiles()
      return toast.error("File size too big! Only files less than 30 MB are allowed");
    } else if (!e.target.files[0].name.match(allowedExtensions)) {
      setImage('')
      setImagefiles()
      return toast.error("Only jpeg and png files are allowed!");
    } else {
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImage(fileURL)
      setImagefiles(e.target.files[0])
      ImageChangeN(e)
    }
  };

  const CrooppedImage = (imgss) => {
    console.log('Cropped Image Called', imgss)
    setOpen(!open)
    const values = blobToFile(imgss, 'newImage')
    console.log('Cropped Image Called', values)
    setImg(URL.createObjectURL(values))
    ImageCrop(values)
  }

  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    setLoad(!load)
    const values = blobToFile(imgss, 'newImage')
    console.log('Cropped Image Called', values)
    setImage(URL.createObjectURL(values))
    ImageCropN(values)
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  return (
    <form>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <FormLabel>Image</FormLabel>
            <TextField
              required
              variant="outlined"
              id="image"
              name="photo"
              type="file"
              onChange={onThumb}
            />
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <div style={{ display: 'flex', margin: '5px', justifyContent: 'space-between' }}>
            {(img === undefined || img === null || img === "") ? '' : <div>
              <img src={img} width="80px" height="80px" alt="Unknown" />
              <Button type="button" color="primary" variant="contained" onClick={() => { setOpen(true) }}>Crop Image</Button>
            </div>}
          </div>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Title 1</FormLabel>
            <TextField
              required
              variant="outlined"
              id="title1"
              name="title1"
              type="text"
              value={data.title1}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Description 1</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="description1"
              name="description1"
              type="text"
              value={data.description1}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Right Side Title</FormLabel>
            <TextField
              required
              variant="outlined"
              id="rtitle"
              name="rtitle"
              type="text"
              value={data.rtitle}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Right Side Description</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="rdescription"
              name="rdescription"
              type="text"
              value={data.rdescription}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Title Head</FormLabel>
            <TextField
              required
              variant="outlined"
              id="titlehead"
              name="titlehead"
              type="text"
              value={data.titlehead}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <FormLabel>Image 2</FormLabel>
            <TextField
              required
              variant="outlined"
              id="image"
              name="photo"
              type="file"
              onChange={onThumbN}
            />
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <div style={{ display: 'flex', margin: '5px', justifyContent: 'space-between' }}>
            {(image === undefined || image === null || image === "") ? '' : <div>
              <img src={image} width="80px" height="80px" alt="Unknown" />
              <Button type="button" color="primary" variant="contained" onClick={() => { setLoad(true) }}>Crop Image</Button>
            </div>}
          </div>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Title 2</FormLabel>
            <TextField
              required
              variant="outlined"
              id="title2"
              name="title2"
              type="text"
              value={data.title2}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Description 2</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="description2"
              name="description2"
              type="text"
              value={data.description2}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Title 3</FormLabel>
            <TextField
              required
              variant="outlined"
              id="title3"
              name="title3"
              type="text"
              value={data.title3}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Description 3</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="description3"
              name="description3"
              type="text"
              value={data.description3}
              onChange={handleChange}
            />
          </FormControl>
        </Grid><Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Title 4</FormLabel>
            <TextField
              required
              variant="outlined"
              id="title4"
              name="title4"
              type="text"
              value={data.title4}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Description 4</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="description4"
              name="description4"
              type="text"
              value={data.description4}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Title 5</FormLabel>
            <TextField
              required
              variant="outlined"
              id="title5"
              name="title5"
              type="text"
              value={data.title5}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Description 5</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="description5"
              name="description5"
              type="text"
              value={data.description5}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Summary Title</FormLabel>
            <TextField
              required
              variant="outlined"
              id="stitle"
              name="stitle"
              type="text"
              value={data.stitle}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Summary Description</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="sdescription"
              name="sdescription"
              type="text"
              value={data.sdescription}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
      </Grid>
      <Dialog open={open}
        onClose={() => { setOpen(!open) }}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" fullWidth>
        <DialogContent>
          <NewCropper img={img} click={CrooppedImage} />
        </DialogContent>
      </Dialog>
      <Dialog open={load}
        onClose={() => { setLoad(!load) }}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" fullWidth>
        <DialogContent>
          <NewCropper img={image} click={CrooppedImageN} />
        </DialogContent>
      </Dialog>
    </form>
  )
}

export default FormComponent
