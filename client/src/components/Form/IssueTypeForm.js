import { Grid, TextField, FormControl, FormLabel } from '@material-ui/core';
import React from 'react'
import { useState } from 'react'

const IssueTypeForm = ({ value, Static }) => {

  const [data, setData] = useState(value)

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
    Static(event)
  };

  return (
    <form>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <FormLabel>Name</FormLabel>
            <TextField
              required
              variant="outlined"
              id="name"
              name="name"
              type="text"
              value={data.name}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <FormLabel>Type</FormLabel>
            <TextField
              required
              variant="outlined"
              id="type"
              name="type"
              type="text"
              value={data.type}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
      </Grid>
    </form>
  )
}

export default IssueTypeForm
