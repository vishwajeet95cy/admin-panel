import { Grid, TextField, FormControl, FormLabel } from '@material-ui/core';
import React from 'react'
import { useState } from 'react'
import { TextareaAutosize } from '@material-ui/core';

const LinkForm = ({ value, Static }) => {

  const [data, setData] = useState(value)

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
    Static(event)
  };

  return (
    <form>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Title</FormLabel>
            <TextField
              required
              variant="outlined"
              id="title"
              name="title"
              type="text"
              value={data.title}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Description</FormLabel>
            <TextareaAutosize
              required
              style={{ height: '55px' }}
              variant="outlined"
              id="description"
              name="description"
              type="text"
              value={data.description}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Email Link</FormLabel>
            <TextField
              required
              variant="outlined"
              id="email"
              name="email"
              type="text"
              value={data.email}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Facebook Link</FormLabel>
            <TextField
              required
              variant="outlined"
              id="facebook"
              name="facebook"
              type="text"
              value={data.facebook}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Twitter Link</FormLabel>
            <TextField
              required
              variant="outlined"
              id="twitter"
              name="twitter"
              type="text"
              value={data.twitter}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <FormLabel>Linkedin Link</FormLabel>
            <TextField
              required
              variant="outlined"
              id="linkedin"
              name="linkedin"
              type="text"
              value={data.linkedin}
              onChange={handleChange}
            />
          </FormControl>
        </Grid>
      </Grid>
    </form>
  )
}

export default LinkForm
