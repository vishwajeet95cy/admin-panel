import { Container, Card, Button } from '@material-ui/core'
import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { createCombinations, createFinals, getAllCombinations } from '../../redux/actions'

const PageCombination = () => {

  const dispatch = useDispatch()
  const allCombination = useSelector(state => state.auth.combinationDetail)
  const Create = useSelector(state => state.auth.created)
  const loader = useSelector(state => state.auth.loader)
  const [lang, setLang] = useState([])
  const [int, setInt] = useState([])
  const [age, setAge] = useState([])
  const [gen, setGen] = useState([])

  const [flag, setFlag] = useState(false)

  useEffect(() => {

    if (Object.keys(allCombination).length == 0) {
      dispatch(getAllCombinations())
    } else {
      setLang(allCombination.language)
      setInt(allCombination.interest)
      setAge(allCombination.age)
      setGen(allCombination.gender)
      setFlag(true)
    }

    if (Create) {
      dispatch(getAllCombinations());
    }

  }, [allCombination, Create])

  const handleCombinationButton = () => {
    dispatch(createCombinations())
  }

  const handleFinalButton = () => {
    dispatch(createFinals())
  }

  return (
    <Container>
      <Card>
        <Button color="primary" variant="contained" disabled={loader ? true : false} onClick={() => { handleCombinationButton() }}>Create Combinations</Button>
        <Button color="primary" variant="contained" disabled={loader ? true : false} onClick={() => { handleFinalButton() }}>Create Final</Button>
      </Card>
    </Container>
  )
}

export default PageCombination
