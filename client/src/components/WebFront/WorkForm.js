import { Container, Card, CardContent, Typography, Grid, Button } from '@material-ui/core'
import React, { useState } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, withRouter } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Create_Work } from '../../redux/actions'
import FormComponent from '../Form/FormComponent'

const WorkForm = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const [img, setImg] = useState('')
  const [imageFile, setImagefile] = useState()
  const [image, setImage] = useState('')
  const [imageFiles, setImagefiles] = useState()

  const LoadingTrue = useSelector(state => state.auth.loader);

  const [data, setData] = useState({
    title1: '',
    description1: '',
    title2: '',
    description2: '',
    title3: '',
    titlehead: '',
    description3: '',
    title4: '',
    description4: '',
    title5: '',
    description5: '',
    stitle: '',
    sdescription: '',
    rtitle: '',
    rdescription: ''
  })

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
  };

  const CallBlobSelect = (ss) => {
    setImg(URL.createObjectURL(ss))
    setImagefile(ss)
  }

  const CallBlobSelectN = (ss) => {
    setImage(URL.createObjectURL(ss))
    setImagefiles(ss)
  }

  const onThumb = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;
    if (e.target.files[0].size > 30720000) {
      setImg('')
      setImagefile()
      return
    } else if (!e.target.files[0].name.match(allowedExtensions)) {
      setImg('')
      setImagefile()
      return
    } else {
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImg(fileURL)
      setImagefile(e.target.files[0])
    }
  };

  const onThumbN = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;
    if (e.target.files[0].size > 30720000) {
      setImage('')
      setImagefiles()
      return
    } else if (!e.target.files[0].name.match(allowedExtensions)) {
      setImage('')
      setImagefiles()
      return
    } else {
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImage(fileURL)
      setImagefiles(e.target.files[0])
    }
  };

  const handleSubmit = () => {


    if (img == "") {

      return toast.error('Please upload Image')
    } else if (data.title1 == "") {

      return toast.error('Please Write Title 1')
    } else if (data.title2 == "") {

      return toast.error('Please Write Title 2')
    } else if (data.title3 == "") {

      return toast.error('Please Write Title 3')
    } else if (data.title4 == "") {

      return toast.error('Please Write Title 4')
    } else if (data.title5 == "") {

      return toast.error('Please Write Title 5')
    } else if (data.rtitle == "") {

      return toast.error('Please Write right tile')
    } else if (data.description1 == "") {

      return toast.error('Please Write Description 1')
    } else if (data.description2 == "") {

      return toast.error('Please Write Description 2')
    } else if (data.description3 == "") {

      return toast.error('Please Write Description 3')
    } else if (data.description4 == "") {

      return toast.error('Please Write Description 4')
    } else if (data.description5 == "") {

      return toast.error('Please Write Description 5')
    } else if (data.sdescription == "") {

      return toast.error('Please Write summary Description')
    } else if (data.rdescription == "") {

      return toast.error('Please Write right Description')
    } else if (data.stitle == "") {

      return toast.error('Please Write Summary')
    } else if (data.titlehead == "") {

      return toast.error('Please Write titlehead')
    }

    var workFormData = new FormData();
    workFormData.append('title1', data.title1)
    workFormData.append('title2', data.title2)
    workFormData.append('title3', data.title3)
    workFormData.append('title4', data.title4)
    workFormData.append('title5', data.title5)
    workFormData.append('rtitle', data.rtitle)
    workFormData.append('stitle', data.stitle)
    workFormData.append('titlehead', data.titlehead)
    workFormData.append('description1', data.description1)
    workFormData.append('description2', data.description2)
    workFormData.append('description3', data.description3)
    workFormData.append('description4', data.description4)
    workFormData.append('description5', data.description5)
    workFormData.append('rdescription', data.rdescription)
    workFormData.append('sdescription', data.sdescription)
    workFormData.append('first', imageFile)
    workFormData.append('second', imageFiles)
    dispatch(Create_Work(workFormData))
  }

  const Create = useSelector(state => state.auth.created)

  useEffect(() => {
    if (Create) {
      if (!history.location.state || history.location.state == null || history.location.state == undefined) {
        history.push('/dashboard/new')
      } else {
        history.push(history.location.state.from)
      }
    }
  }, [Create])


  return (
    <Container component="main" maxWidth="xl">
      <Card style={{ padding: '15px 15px', width: '100%' }}>
        <CardContent>
          <Typography style={{
            textAlign: "center",
          }} component="h1" variant="h5">
            Work Form
          </Typography>
        </CardContent>
        <FormComponent value={data} Static={handleChange} ImageChange={onThumb} ImageChangeN={onThumbN} imgVal={img} imgVal2={image} ImageCrop={CallBlobSelect} ImageCropN={CallBlobSelectN} />
        <Grid container spacing={3} justify="center" style={{ marginBottom: '5px' }}>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary" onClick={() => {
                if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                  history.push('/dashboard/new')
                } else {
                  history.push(history.location.state.from)
                }
              }}>Back</Button>
          </Grid>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary"
              disabled={LoadingTrue ? true : false}
              onClick={() => { handleSubmit() }}
            >Submit</Button>
          </Grid>
        </Grid>
      </Card>
    </Container>
  )
}

export default withRouter(WorkForm)
