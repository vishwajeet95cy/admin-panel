import { Container, Card, CardContent, Typography, Grid, Button } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Update_Footer } from '../../redux/actions'
import FooterForm from '../Form/FooterForm'
import Loader from '../UsableComponent/Loader';

var FormId
const EditFooter = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const [btnD, setBtnD] = useState(true)
  const LoadingTrue = useSelector(state => state.auth.loader);

  const [data, setData] = useState({
    maintitle: '',
    subitems: [],
    name: '',
    link: ''
  })

  const AllFooterData = useSelector(state => state.auth.footerDetail)

  const [flag, setFlag] = useState(false)
  const [id, setId] = useState('')

  const Updated = useSelector(state => state.auth.updated);

  useEffect(() => {

    console.log('history.location.state', history.location.state)
    if (!history.location.state || history.location.state == null || history.location.state == undefined) {
      history.push('/dashboard/footer')
    } else {
      FormId = history.location.state.id
      AllFooterData.footerList.map((cc) => {
        if (cc._id == FormId) {
          setData({ ...cc, name: '', link: '' })
          setFlag(true)
        }
      })
    }

    if (Updated) {
      history.push(history.location.state.from)
    }
  }, [Updated])

  const handleChange = useCallback((event) => {

    if (id !== '') {
      let lsde = [...data.subitems];
      lsde[id] = { ...lsde[id], [event.target.name]: event.target.value }
      setData({ ...data, subitems: lsde });
      console.log('id', lsde)
      setBtnD(false)
    } else {
      setData({ ...data, [event.target.name]: event.target.value });
      console.log('id', [event.target.name], event.target.value)
      setBtnD(false)
    }
  }, [id])

  const onSelect = e => {
    let lsde = [...data.subitems];
    var cc = { name: data.name, link: data.link }
    if (!lsde.some((item) => item.name == cc.name)) {
      lsde.push(cc);
      setData({ ...data, subitems: lsde });
      setBtnD(false)
    }
  };

  const onRemoveItem = (e, ind) => {
    setId('')
    setData({
      ...data,
      subitems: data.subitems.filter((inv, index) => index !== ind)
    });
    setBtnD(false)
  };

  const handleSubmit = () => {
    if (data.maintitle == "") {
      return toast.error('Please Write Main Title')
    } else if (data.subitems.length == 0) {
      return toast.error('Please add Sub Items')
    }

    const cc = {
      maintitle: data.maintitle,
      subitems: data.subitems,
    }

    console.log('Submitted Data', cc)

    dispatch(Update_Footer(FormId, {
      maintitle: data.maintitle,
      subitems: data.subitems,
    }))

  }

  const EditItem = (e, ind) => {
    const nvalue = data.subitems.filter((inv, index) => index == ind)
    setData({ ...data, name: nvalue[0].name, link: nvalue[0].link })
    setId(ind)
  }


  return (
    <Container component="main" maxWidth="xl">
      <Card style={{ padding: '15px 15px', width: '100%' }}>
        <CardContent>
          <Typography style={{
            textAlign: "center",
          }} component="h1" variant="h5">
            Edit  Footer Form
          </Typography>
        </CardContent>
        {flag ? <React.Fragment>
          <FooterForm value={data} Static={handleChange} addItem={onSelect} removeItem={onRemoveItem} nopen={id} editLink={EditItem} />
          <Grid container spacing={3} justify="center" style={{ marginBottom: '5px' }}>
            <Grid item>
              <Button type="button"
                variant="contained"
                color="primary" onClick={() => {
                  if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                    history.push('/dashboard/footer')
                  } else {
                    history.push(history.location.state.from)
                  }
                }}>Back</Button>
            </Grid>
            <Grid item>
              <Button type="button"
                variant="contained"
                color="primary"
                disabled={(btnD || LoadingTrue) ? true : false}
                onClick={() => { handleSubmit() }}
              >Update</Button>
            </Grid>
          </Grid>
        </React.Fragment> : <Loader />}
      </Card>
    </Container>
  )
}

export default EditFooter
