import { Container, Card, CardContent, Typography, Grid, Button } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Create_IssueType } from '../../redux/actions'
import IssueTypeForm from '../Form/IssueTypeForm'

const AddIssueType = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const LoadingTrue = useSelector(state => state.auth.loader);

  const [data, setData] = useState({
    name: '',
    type: '',
  })

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
  };

  const handleSubmit = () => {
    if (data.name == "") {
      return toast.error('Please Write Name')
    } else if (data.type == "") {
      return toast.error('Please Write Type')
    }

    dispatch(Create_IssueType({
      name: data.name,
      type: data.type,
    }))

  }


  const Create = useSelector(state => state.auth.created)

  useEffect(() => {
    if (Create) {
      if (!history.location.state || history.location.state == null || history.location.state == undefined) {
        history.push('/dashboard/issue')
      } else {
        history.push(history.location.state.from)
      }
    }
  }, [Create])


  return (
    <Container component="main" maxWidth="xl">
      <Card style={{ padding: '15px 15px', width: '100%' }}>
        <CardContent>
          <Typography style={{
            textAlign: "center",
          }} component="h1" variant="h5">
            Issue Type Form
          </Typography>
        </CardContent>
        <IssueTypeForm value={data} Static={handleChange} />
        <Grid container spacing={3} justify="center" style={{ marginBottom: '5px' }}>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary" onClick={() => {
                if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                  history.push('/dashboard/issue')
                } else {
                  history.push(history.location.state.from)
                }
              }}>Back</Button>
          </Grid>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary"
              disabled={LoadingTrue ? true : false}
              onClick={() => { handleSubmit() }}
            >Submit</Button>
          </Grid>
        </Grid>
      </Card>
    </Container>
  )
}

export default AddIssueType
