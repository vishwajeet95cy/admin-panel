import { Container, Card, CardContent, Typography, Grid, Button } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Update_Network } from '../../redux/actions'
import LinkForm from '../Form/LinkForm'
import Loader from '../UsableComponent/Loader';

var FormId
const EditNetwork = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const [btnD, setBtnD] = useState(true)
  const LoadingTrue = useSelector(state => state.auth.loader);

  const [data, setData] = useState({
    title: '',
    description: '',
    email: '',
    linkedin: '',
    facebook: '',
    twitter: '',
  })


  const AllNetworkData = useSelector(state => state.auth.networkDetail)

  const [flag, setFlag] = useState(false)

  const Updated = useSelector(state => state.auth.updated);

  useEffect(() => {

    console.log('history.location.state', history.location.state)
    if (!history.location.state || history.location.state == null || history.location.state == undefined) {
      history.push('/dashboard/network')
    } else {
      FormId = history.location.state.id
      AllNetworkData.networkList.map((cc) => {
        if (cc._id == FormId) {
          setData(cc)
          setFlag(true)
        }
      })
    }

    if (Updated) {
      history.push(history.location.state.from)
    }
  }, [Updated])

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
    setBtnD(false)
  };

  const handleSubmit = () => {
    if (data.title == "") {
      return toast.error('Please Write Title')
    } else if (data.description == "") {
      return toast.error('Please Write Description')
    } else if (data.email == "") {
      return toast.error('Please Write email link')
    } else if (data.linkedin == "") {
      return toast.error('Please Write linkedin link')
    } else if (data.facebook == "") {
      return toast.error('Please Write facebook link')
    } else if (data.twitter == "") {
      return toast.error('Please Write twitter link')
    }

    dispatch(Update_Network(FormId, {
      title: data.title,
      description: data.description,
      email: data.email,
      linkedin: data.linkedin,
      facebook: data.facebook,
      twitter: data.twitter
    }))
  }


  return (
    <Container component="main" maxWidth="xl">
      <Card style={{ padding: '15px 15px', width: '100%' }}>
        <CardContent>
          <Typography style={{
            textAlign: "center",
          }} component="h1" variant="h5">
            Edit  NetWork Form
          </Typography>
        </CardContent>
        {flag ? <React.Fragment>
          <LinkForm value={data} Static={handleChange} />
          <Grid container spacing={3} justify="center" style={{ marginBottom: '5px' }}>
            <Grid item>
              <Button type="button"
                variant="contained"
                color="primary" onClick={() => {
                  if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                    history.push('/dashboard/network')
                  } else {
                    history.push(history.location.state.from)
                  }
                }}>Back</Button>
            </Grid>
            <Grid item>
              <Button type="button"
                variant="contained"
                color="primary"
                disabled={(btnD || LoadingTrue) ? true : false}
                onClick={() => { handleSubmit() }}
              >Update</Button>
            </Grid>
          </Grid>
        </React.Fragment> : <Loader />}
      </Card>
    </Container>
  )
}

export default EditNetwork
