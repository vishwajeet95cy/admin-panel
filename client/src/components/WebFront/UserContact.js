import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Dialog, DialogTitle, Grid, IconButton, Box, DialogContent, DialogActions } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllUserContactList, Delete_UserContact } from '../../redux/actions';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
import NewDelete from '../UsableComponent/NewDelete';
import DefaultImage from "../../assets/image.jpg";
import TooltipComp from '../UsableComponent/TooltipComp';
import { Close } from '@material-ui/icons';

// New View
import Button from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from '../../newviews/Card/Card';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';
import { Chip } from '@material-ui/core';
import TblPagination from '../UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

var ResulData

const UserContact = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [data, setData] = useState([])
  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)

  const AllContactData = useSelector(state => state.auth.contactDetail)
  const totalCount = useSelector(state => state.auth.contactTotal)
  const Updates = useSelector(state => state.auth.updated)
  const [flag, setFlag] = useState(false)
  const LoadingTrue = useSelector(state => state.auth.loader);

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== data.length) {
      dispatch(GetAllUserContactList(data.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (Object.keys(AllContactData).length === 0) {
      dispatch(GetAllUserContactList(0, 10));
    } else {

      setData(AllContactData.contactList)
      setFlag(true)
    }
    if (Updates) {
      dispatch(GetAllUserContactList(0, 10));
      setnumSelected(0)
      setId([])

    }

    return () => { setData([]); }
  }, [AllContactData, Updates])


  const addUserContactId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const handleDeleteButton = () => {

    dispatch(Delete_UserContact({ id: id }))
  }

  const [open, setOpen] = useState(false)

  const handleDialog = (item) => {
    data.map((cc) => {
      if (cc._id == item) {
        setOpen(true)
        return ResulData = <Grid container spacing={3} justify="center">
          <Grid item xs={6}>CustomeId : </Grid>
          <Grid item xs={6}>{cc.customer_id}</Grid>
          <Grid item xs={6}>Name</Grid>
          <Grid item xs={6}>{cc.name}</Grid>
          <Grid item xs={6}>Email Id</Grid>
          <Grid item xs={6}>{cc.email}</Grid>
          <Grid item xs={6}>Issue Type</Grid>
          <Grid item xs={6}>{cc.issueType.name}</Grid>
          <Grid item xs={6}>Campaign Name</Grid>
          <Grid item xs={6}>{cc.campaign_name}</Grid>
          <Grid item xs={6}>Summary</Grid>
          <Grid item xs={6}>{cc.summary}</Grid>
          <Grid item xs={6}>Help</Grid>
          <Grid item xs={6}>{cc.help}</Grid>
        </Grid>
      }
    })
  }

  const handleClose = () => {
    setOpen(!open)
  }

  const handleRefresh = () => {
    dispatch(GetAllUserContactList(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite} ><NewDelete name="All User Contact List" length={numSelected} click={LoadingTrue ? null : handleDeleteButton} /></h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer component={Paper} className={classes.container}>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>ID</TableCell>
                    <TableCell>Customer ID</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Issue Type</TableCell>
                    <TableCell>Email CC</TableCell>
                    <TableCell>Campaign Name</TableCell>
                    <TableCell>Image</TableCell>
                    <TableCell>Help</TableCell>
                    <TableCell>Summary</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>

                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((item) => {
                      return <TableRow key={item._id}>
                        <TableCell>
                          <input
                            type="checkbox"
                            className="contactlist"
                            onClick={(e) => addUserContactId(e)}
                            id={item._id}
                            name={item._id}
                            value={item._id}
                          />
                        </TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>{item._id}</TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>{item.customer_id}</TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>{item.name}</TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>{item.email}</TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>{item.issueType.name}</TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>{item.emailcc}</TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>{item.campaign_name}</TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}><img
                          width="90px"
                          height="47px"
                          src={item.image}
                          alt="Unkown"
                          style={{ cursor: "pointer" }}
                          onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = DefaultImage;
                          }}
                        />
                        </TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>
                          <TooltipComp value={item.help}>
                            <Chip label="Help" />
                          </TooltipComp>
                        </TableCell>
                        <TableCell onClick={() => { handleDialog(item._id) }}>
                          <TooltipComp value={item.summary}>
                            <Chip label="Summary" />
                          </TooltipComp>
                        </TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog
        fullWidth
        maxWidth="md"
        open={open}
        onClose={handleClose}
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >User Contact Data</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          {ResulData}
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  )
}

export default UserContact
