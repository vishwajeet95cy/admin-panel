import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllNetworkList, Delete_Network, Start_Network, Stop_Network } from '../../redux/actions';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
import NewDelete from '../UsableComponent/NewDelete';
import TooltipComp from '../UsableComponent/TooltipComp';
import ButtonComp from '../UsableComponent/ButtonComp';

// New View
import Button from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from '../../newviews/Card/Card';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';
import { Chip } from '@material-ui/core';
import { cardHeader } from '../UsableComponent/TableSorting/main';
import TblPagination from '../UsableComponent/TableSorting/TblPagination';


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const HeaderArr = [
  { name: 'start' },
  { name: 'stop' },
]

const NetworkLink = () => {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([])
  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)

  const AllNetworkData = useSelector(state => state.auth.networkDetail)
  const totalCount = useSelector(state => state.auth.networkTotal)
  const Updates = useSelector(state => state.auth.updated)
  const Create = useSelector(state => state.auth.created)
  const LoadingTrue = useSelector(state => state.auth.loader);
  const [flag, setFlag] = useState(false)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== data.length) {
      dispatch(GetAllNetworkList(data.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (Object.keys(AllNetworkData).length === 0) {
      dispatch(GetAllNetworkList(0, 10));
    } else {

      setData(AllNetworkData.networkList)
      setFlag(true)
    }
    if (Updates) {
      dispatch(GetAllNetworkList(0, 10));
      setnumSelected(0)
      setId([])
    }
    if (Create) {
      dispatch(GetAllNetworkList(0, 10));
    }
    return () => { setData([]); setId([]); }
  }, [AllNetworkData, Updates, Create])


  const addWorkId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const handleDeleteButton = () => {

    dispatch(Delete_Network({ id: id }))
  }

  const handleStartButton = () => {

    dispatch(Start_Network({ id: id, show: true }))
  }

  const handleStopButton = () => {

    dispatch(Stop_Network({ id: id, show: false }))
  }

  const handleRefresh = () => {
    dispatch(GetAllNetworkList(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}><NewDelete name="All Network List" length={numSelected} data={HeaderArr} click={LoadingTrue ? null : handleDeleteButton} start={LoadingTrue ? null : handleStartButton} stop={LoadingTrue ? null : handleStopButton} /></h4>
            <Button round color="primary" variant="contained" onClick={() => {
              handleRefresh()
            }}>Create</Button>
            <Button round color="primary" variant="contained" onClick={() => {
              history.push('/dashboard/networkform', {
                from: history.location.pathname
              })
            }}>Create</Button>
          </CardHeader>
          <CardBody>
            <TableContainer component={Paper} className={classes.container}>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>ID</TableCell>
                    <TableCell>Start</TableCell>
                    <TableCell>Title</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Facebook</TableCell>
                    <TableCell>Linkedin</TableCell>
                    <TableCell>Twitter</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>

                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((item) => {
                      return <TableRow key={item._id}>
                        <TableCell>
                          <input
                            type="checkbox"
                            className="ageList"
                            onClick={(e) => addWorkId(e)}
                            id={item._id}
                            name={item._id}
                            value={item._id}
                          />
                        </TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>{item._id}</TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>
                          <ButtonComp value={item.show} />
                        </TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>{item.title}</TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>
                          <TooltipComp value={item.description}>
                            <Chip label="description1" />
                          </TooltipComp>
                        </TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>{item.email}</TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>{item.facebook}</TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>{item.linkedin}</TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editNetwork', { from: history.location.pathname, id: item._id }) }}>{item.twitter}</TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default NetworkLink
