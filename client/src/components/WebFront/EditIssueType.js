import { Container, Card, CardContent, Typography, Grid, Button } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Update_IssueType } from '../../redux/actions'
import IssueTypeForm from '../Form/IssueTypeForm'
import Loader from '../UsableComponent/Loader';

var FormId
const EditIssueType = () => {

  const dispatch = useDispatch()
  const history = useHistory()

  const [data, setData] = useState({
    name: '',
    type: '',
  })


  const AllIssueTypeData = useSelector(state => state.auth.issueDetail)
  const LoadingTrue = useSelector(state => state.auth.loader);

  const [flag, setFlag] = useState(false)
  const [btnD, setBtnD] = useState(true)

  const Updated = useSelector(state => state.auth.updated);

  useEffect(() => {

    console.log('history.location.state', history.location.state)
    if (!history.location.state || history.location.state == null || history.location.state == undefined) {
      history.push('/dashboard/issue')
    } else {
      FormId = history.location.state.id
      AllIssueTypeData.issueList.map((cc) => {
        if (cc._id == FormId) {
          setData(cc)
          setFlag(true)
        }
      })
    }

    if (Updated) {
      history.push(history.location.state.from)
    }
  }, [Updated])

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
    setBtnD(false)
  };

  const handleSubmit = () => {
    if (data.name == "") {
      return toast.error('Please Write name')
    } else if (data.type == "") {
      return toast.error('Please Write Type')
    }

    dispatch(Update_IssueType(FormId, {
      name: data.name,
      type: data.type,
    }))
  }


  return (
    <Container component="main" maxWidth="xl">
      <Card style={{ padding: '15px 15px', width: '100%' }}>
        <CardContent>
          <Typography style={{
            textAlign: "center",
          }} component="h1" variant="h5">
            Edit  Issue type Form
          </Typography>
        </CardContent>
        {flag ? <React.Fragment>
          <IssueTypeForm value={data} Static={handleChange} />
          <Grid container spacing={3} justify="center" style={{ marginBottom: '5px' }}>
            <Grid item>
              <Button type="button"
                variant="contained"
                color="primary" onClick={() => {
                  if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                    history.push('/dashboard/issue')
                  } else {
                    history.push(history.location.state.from)
                  }
                }}>Back</Button>
            </Grid>
            <Grid item>
              <Button type="button"
                variant="contained"
                color="primary"
                disabled={(btnD || LoadingTrue) ? true : false}
                onClick={() => { handleSubmit() }}
              >Update</Button>
            </Grid>
          </Grid>
        </React.Fragment> : <Loader />}
      </Card>
    </Container>
  )
}

export default EditIssueType
