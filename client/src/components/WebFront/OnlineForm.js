import { Container, Card, CardContent, Typography, Grid, Button } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Create_Online } from '../../redux/actions'
import FormComponent from '../Form/FormComponent'

const OnlineForm = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const [img, setImg] = useState('')
  const [imageFile, setImagefile] = useState()
  const [image, setImage] = useState('')
  const [imageFiles, setImagefiles] = useState()
  const LoadingTrue = useSelector(state => state.auth.loader);

  const [data, setData] = useState({
    title1: '',
    description1: '',
    title2: '',
    description2: '',
    title3: '',
    titlehead: '',
    description3: '',
    title4: '',
    description4: '',
    title5: '',
    description5: '',
    stitle: '',
    sdescription: '',
    rtitle: '',
    rdescription: ''
  })

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
  };
  const CallBlobSelect = (ss) => {
    setImg(URL.createObjectURL(ss))
    setImagefile(ss)
  }

  const CallBlobSelectN = (ss) => {
    setImage(URL.createObjectURL(ss))
    setImagefiles(ss)
  }

  const onThumb = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;
    if (e.target.files[0].size > 30720000) {
      setImg('')
      setImagefile()
      return
    } else if (!e.target.files[0].name.match(allowedExtensions)) {
      setImg('')
      setImagefile()
      return
    } else {
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImg(fileURL)
      setImagefile(e.target.files[0])
    }
  };

  const onThumbN = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;
    if (e.target.files[0].size > 30720000) {
      setImage('')
      setImagefiles()
      return
    } else if (!e.target.files[0].name.match(allowedExtensions)) {
      setImage('')
      setImagefiles()
      return
    } else {
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImage(fileURL)
      setImagefiles(e.target.files[0])
    }
  };

  const handleSubmit = () => {

    if (img == "") {

      return toast.error('Please upload Image')
    } else if (data.title1 == "") {

      return toast.error('Please Write Title 1')
    } else if (data.title2 == "") {

      return toast.error('Please Write Title 2')
    } else if (data.title3 == "") {

      return toast.error('Please Write Title 3')
    } else if (data.title4 == "") {

      return toast.error('Please Write Title 4')
    } else if (data.title5 == "") {

      return toast.error('Please Write Title 5')
    } else if (data.rtitle == "") {

      return toast.error('Please Write right tile')
    } else if (data.description1 == "") {

      return toast.error('Please Write Description 1')
    } else if (data.description2 == "") {

      return toast.error('Please Write Description 2')
    } else if (data.description3 == "") {

      return toast.error('Please Write Description 3')
    } else if (data.description4 == "") {

      return toast.error('Please Write Description 4')
    } else if (data.description5 == "") {

      return toast.error('Please Write Description 5')
    } else if (data.sdescription == "") {

      return toast.error('Please Write summary Description')
    } else if (data.rdescription == "") {

      return toast.error('Please Write right Description')
    } else if (data.stitle == "") {

      return toast.error('Please Write Summary')
    } else if (data.titlehead == "") {

      return toast.error('Please Write titlehead')
    } else if (image == "") {

      return toast.error('Please upload Image2')
    }

    var onlineFormData = new FormData();
    onlineFormData.append('title1', data.title1)
    onlineFormData.append('title2', data.title2)
    onlineFormData.append('title3', data.title3)
    onlineFormData.append('title4', data.title4)
    onlineFormData.append('title5', data.title5)
    onlineFormData.append('rtitle', data.rtitle)
    onlineFormData.append('stitle', data.stitle)
    onlineFormData.append('titlehead', data.titlehead)
    onlineFormData.append('description1', data.description1)
    onlineFormData.append('description2', data.description2)
    onlineFormData.append('description3', data.description3)
    onlineFormData.append('description4', data.description4)
    onlineFormData.append('description5', data.description5)
    onlineFormData.append('rdescription', data.rdescription)
    onlineFormData.append('sdescription', data.sdescription)
    onlineFormData.append('first', imageFile)
    onlineFormData.append('second', imageFiles)

    dispatch(Create_Online(onlineFormData))
  }


  const Create = useSelector(state => state.auth.created)

  useEffect(() => {
    if (Create) {
      if (!history.location.state || history.location.state == null || history.location.state == undefined) {
        history.push('/dashboard/online')
      } else {
        history.push(history.location.state.from)
      }
    }
  }, [Create])


  return (
    <Container component="main" maxWidth="xl">
      <Card style={{ padding: '15px 15px', width: '100%' }}>
        <CardContent>
          <Typography style={{
            textAlign: "center",
          }} component="h1" variant="h5">
            Online Form
          </Typography>
        </CardContent>
        <FormComponent value={data} Static={handleChange} ImageChange={onThumb} ImageChangeN={onThumbN} imgVal={img} imgVal2={image} ImageCrop={CallBlobSelect} ImageCropN={CallBlobSelectN} />
        <Grid container spacing={3} justify="center" style={{ marginBottom: '5px' }}>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary" onClick={() => {
                if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                  history.push('/dashboard/online')
                } else {
                  history.push(history.location.state.from)
                }
              }}>Back</Button>
          </Grid>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary"
              disabled={LoadingTrue ? true : false}
              onClick={() => { handleSubmit() }}
            >Submit</Button>
          </Grid>
        </Grid>
      </Card>
    </Container>
  )
}

export default OnlineForm
