import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllFooterList, Delete_Footer } from '../../redux/actions';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
import NewDelete from '../UsableComponent/NewDelete';

// New View
import Button from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from '../../newviews/Card/Card';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';
import { Chip } from '@material-ui/core';
import TblPagination from '../UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const FooterList = () => {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([])
  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)

  const AllFooterData = useSelector(state => state.auth.footerDetail)
  const totalCount = useSelector(state => state.auth.footerTotal)
  const Updates = useSelector(state => state.auth.updated)
  const Create = useSelector(state => state.auth.created)
  const [flag, setFlag] = useState(false)
  const LoadingTrue = useSelector(state => state.auth.loader);

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== data.length) {
      dispatch(GetAllFooterList(data.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (Object.keys(AllFooterData).length === 0) {
      dispatch(GetAllFooterList(0, 10));
    } else {

      setData(AllFooterData.footerList)
      setFlag(true)
    }
    if (Updates) {
      dispatch(GetAllFooterList(0, 10));
      setnumSelected(0)
      setId([])
    }
    if (Create) {
      dispatch(GetAllFooterList(0, 10));
    }
    return () => { setData([]); setId([]); }
  }, [AllFooterData, Updates, Create])


  const addWorkId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const handleDeleteButton = () => {

    dispatch(Delete_Footer({ id: id }))
  }

  const handleRefresh = () => {
    dispatch(GetAllFooterList(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}><NewDelete name="All Footer List" length={numSelected} click={LoadingTrue ? null : handleDeleteButton} /></h4>
            <Button round color="primary" variant="contained" onClick={() => {
              handleRefresh()
            }}>Refresh</Button>
            <Button round color="primary" variant="contained" onClick={() => {
              history.push('/dashboard/footerform', {
                from: history.location.pathname
              })
            }}>Create</Button>
          </CardHeader>
          <CardBody>
            <TableContainer component={Paper} className={classes.container}>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>ID</TableCell>
                    <TableCell>Main Title</TableCell>
                    <TableCell>SubItems</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>

                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((item) => {
                      return <TableRow key={item._id}>
                        <TableCell>
                          <input
                            type="checkbox"
                            className="ageList"
                            onClick={(e) => addWorkId(e)}
                            id={item._id}
                            name={item._id}
                            value={item._id}
                          />
                        </TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editFooter', { from: history.location.pathname, id: item._id }) }}>{item._id}</TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editFooter', { from: history.location.pathname, id: item._id }) }}>
                          {item.maintitle}
                        </TableCell>
                        <TableCell onClick={() => { history.push('/dashboard/editFooter', { from: history.location.pathname, id: item._id }) }}>{item.subitems.map((cc, index) => {
                          return <Chip
                            key={index}
                            label={(!cc.name || cc.name == null || cc.name == undefined) ? 'wait' : cc.name}
                            style={{ margin: '2px' }}
                          />
                        })}</TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default FooterList
