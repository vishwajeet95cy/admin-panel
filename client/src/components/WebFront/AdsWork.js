import React, { useEffect, useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useHistory, withRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
import { BannerWorkStart, BannerWorkStop, Delete_Work, GetAllWorkList, Start_Work, Stop_Work } from '../../redux/actions';
import NewDelete from '../UsableComponent/NewDelete';
import DefaultImage from "../../assets/image.jpg";
import TooltipComp from '../UsableComponent/TooltipComp';
import ButtonComp from '../UsableComponent/ButtonComp';
import { BackContext } from '../utils/LocationContext';

// New View
import Button from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from '../../newviews/Card/Card';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';
import { Chip } from '@material-ui/core';
import TblPagination from '../UsableComponent/TableSorting/TblPagination';


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const HeaderArr = [
  { name: 'start' },
  { name: 'stop' },
  { name: 'BannerStart', lcheck: 1 },
  { name: 'BannerStop', lcheck: 1 },
]

const AdsWork = () => {
  const context = useContext(BackContext)
  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([])
  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)

  const AllWorkData = useSelector(state => state.auth.workDetail)
  const totalCount = useSelector(state => state.auth.workTotal)
  const Updates = useSelector(state => state.auth.updated)
  const Create = useSelector(state => state.auth.created)
  const [flag, setFlag] = useState(false)
  const LoadingTrue = useSelector(state => state.auth.loader);

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== data.length) {
      dispatch(GetAllWorkList(data.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    console.log('Adswork  history Value Before Pop', context);
    // context.handleValue([history.location.pathname])
  }
    , [])

  useEffect(() => {
    if (Object.keys(AllWorkData).length === 0) {
      dispatch(GetAllWorkList(0, 10));
    } else {

      setData(AllWorkData.workList)
      setFlag(true)
    }
    if (Updates) {
      dispatch(GetAllWorkList(0, 10));
      setnumSelected(0)
      setId([])
    }
    if (Create) {
      dispatch(GetAllWorkList(0, 10));
    }
    return () => { setData([]); setId([]); }
  }, [AllWorkData, Updates, Create])


  const addWorkId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const handleDeleteButton = () => {

    dispatch(Delete_Work({ id: id }))
  }

  const handleStartButton = () => {

    dispatch(Start_Work({ id: id, start: true }))
  }

  const handleStopButton = () => {

    dispatch(Stop_Work({ id: id, start: false }))
  }

  const handleBannerStartButton = () => {

    dispatch(BannerWorkStart({ id: id, banner: true }))
  }

  const handleBannerStopButton = () => {

    dispatch(BannerWorkStop({ id: id, banner: false }))
  }

  const handleRefresh = () => {
    dispatch(GetAllWorkList(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}><NewDelete name="All Work List" length={numSelected} data={HeaderArr} click={LoadingTrue ? null : handleDeleteButton} start={LoadingTrue ? null : handleStartButton} stop={LoadingTrue ? null : handleStopButton} BannerStart={LoadingTrue ? null : handleBannerStartButton} BannerStop={LoadingTrue ? null : handleBannerStopButton} /></h4>
            <Button round color="primary" variant="contained" onClick={() => {
              handleRefresh()
            }}>Create</Button>
            <Button round color="primary" variant="contained" onClick={() => {
              history.push('/dashboard/workform', {
                from: history.location.pathname
              })
            }}>Create</Button>
          </CardHeader>
          <CardBody>
            <TableContainer component={Paper} className={classes.container}>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>ID</TableCell>
                    <TableCell>Banner</TableCell>
                    <TableCell>Start</TableCell>
                    <TableCell>Title Head</TableCell>
                    <TableCell>Image</TableCell>
                    <TableCell>Image 2</TableCell>
                    <TableCell>Title 1</TableCell>
                    <TableCell>Description 1</TableCell>
                    <TableCell>Title 2</TableCell>
                    <TableCell>Description 2</TableCell>
                    <TableCell>Title 3</TableCell>
                    <TableCell>Description 3</TableCell>
                    <TableCell>Title 4</TableCell>
                    <TableCell>Description 4</TableCell>
                    <TableCell>Title 5</TableCell>
                    <TableCell>Description 5</TableCell>
                    <TableCell>Right Title</TableCell>
                    <TableCell>Right Description</TableCell>
                    <TableCell>Summary Title</TableCell>
                    <TableCell>Summary Description</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>

                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((item) => {
                      return <TableRow key={item._id} >
                        <TableCell>
                          <input
                            type="checkbox"
                            className="ageList"
                            onClick={(e) => addWorkId(e)}
                            id={item._id}
                            name={item._id}
                            value={item._id}
                          />
                        </TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item._id}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>
                          <ButtonComp value={item.banner} />
                        </TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>
                          <ButtonComp value={item.start} />
                        </TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>
                          <TooltipComp value={item.titlehead}>
                            <Chip label="head" />
                          </TooltipComp>
                        </TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>
                          <img
                            width="90px"
                            height="47px"
                            src={item.image}
                            alt="Unkown"
                            style={{ cursor: "pointer" }}
                            onError={(e) => {
                              e.target.onerror = null;
                              e.target.src = DefaultImage;
                            }}
                          />
                        </TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>
                          <img
                            width="90px"
                            height="47px"
                            src={item.image2}
                            alt="Unkown"
                            style={{ cursor: "pointer" }}
                            onError={(e) => {
                              e.target.onerror = null;
                              e.target.src = DefaultImage;
                            }}
                          />
                        </TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item.title1}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>
                          <TooltipComp value={item.description1}>
                            <Chip label="description1" />
                          </TooltipComp>
                        </TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item.title2}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}><TooltipComp value={item.description2}>
                          <Chip label="description2" />
                        </TooltipComp></TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item.title3}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}><TooltipComp value={item.description3}>
                          <Chip label="description3" />
                        </TooltipComp></TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item.title4}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}><TooltipComp value={item.description4}>
                          <Chip label="description4" />
                        </TooltipComp></TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item.title5}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}><TooltipComp value={item.description5}>
                          <Chip label="description5" />
                        </TooltipComp></TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item.rtitle}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}><TooltipComp value={item.rdescription}>
                          <Chip label="description" />
                        </TooltipComp></TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}>{item.stitle}</TableCell>
                        <TableCell onClick={() => { history.push(`/dashboard/editWork`, { from: history.location.pathname, id: item._id }) }}><TooltipComp value={item.sdescription}>
                          <Chip label="description" />
                        </TooltipComp></TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default withRouter(AdsWork)
