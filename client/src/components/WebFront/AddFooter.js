import { Container, Card, CardContent, Typography, Grid, Button } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import { Create_Footer } from '../../redux/actions'
import FooterForm from '../Form/FooterForm'

const AddFooter = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const LoadingTrue = useSelector(state => state.auth.loader);
  const [id, setId] = useState('')

  const [data, setData] = useState({
    maintitle: '',
    subitems: [],
    name: '',
    link: ''
  })

  const handleChange = (event) => {
    if (id !== '') {
      let lsde = [...data.subitems];
      lsde[id] = { ...lsde[id], [event.target.name]: event.target.value }
    } else {
      setData({ ...data, [event.target.name]: event.target.value });
    }
  };

  const onSelect = () => {
    let lsde = [...data.subitems];
    var cc = { name: data.name, link: data.link }
    if (!lsde.some((item) => item.name == cc.name)) {
      lsde.push(cc);
      setData({ ...data, subitems: lsde });
    }
  };

  const onRemoveItem = (e, ind) => {
    setId('')
    setData({
      ...data,
      subitems: data.subitems.filter((inv, index) => index !== ind)
    });
  };

  const handleSubmit = () => {
    if (data.maintitle == "") {
      return toast.error('Please Write Main Title')
    } else if (data.subitems.length == 0) {
      return toast.error('Please add Sub Items')
    }

    const cc = {
      maintitle: data.maintitle,
      subitems: data.subitems,
    }

    console.log('Submitted Data', cc)

    dispatch(Create_Footer({
      maintitle: data.maintitle,
      subitems: data.subitems,
    }))

  }

  const EditItem = (e, ind) => {
    const nvalue = data.subitems.filter((inv, index) => index == ind)
    console.log('Edit Value', nvalue)
    setData({ ...data, name: nvalue[0].name, link: nvalue[0].link })
    setId(ind)
  }


  const Create = useSelector(state => state.auth.created)

  useEffect(() => {
    if (Create) {
      if (!history.location.state || history.location.state == null || history.location.state == undefined) {
        history.push('/dashboard/footer')
      } else {
        history.push(history.location.state.from)
      }
    }
  }, [Create])


  return (
    <Container component="main" maxWidth="xl">
      <Card style={{ padding: '15px 15px', width: '100%' }}>
        <CardContent>
          <Typography style={{
            textAlign: "center",
          }} component="h1" variant="h5">
            Footer Form
          </Typography>
        </CardContent>
        <FooterForm value={data} Static={handleChange} addItem={onSelect} removeItem={onRemoveItem} nopen={id} editLink={EditItem} />
        <Grid container spacing={3} justify="center" style={{ marginBottom: '5px' }}>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary" onClick={() => {
                if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                  history.push('/dashboard/footer')
                } else {
                  history.push(history.location.state.from)
                }
              }}>Back</Button>
          </Grid>
          <Grid item>
            <Button type="button"
              variant="contained"
              color="primary"
              disabled={LoadingTrue ? true : false}
              onClick={() => { handleSubmit() }}
            >Submit</Button>
          </Grid>
        </Grid>
      </Card>
    </Container>
  )
}

export default AddFooter
