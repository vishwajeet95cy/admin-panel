import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getAdminSetting, updateAdminSetting } from '../../redux/actions'
import { Switch, FormControlLabel, makeStyles, FormLabel } from '@material-ui/core'
import GridContainer from '../../newviews/Grid/GridContainer'
import GridItem from '../../newviews/Grid/GridItem'
import Card from "../../newviews/Card/Card";
import CardHeader from "../../newviews/Card/CardHeader.js";
import CardBody from "../../newviews/Card/CardBody.js";
import Loader from '../../components/UsableComponent/Loader'

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const AdminSetting = () => {

  const classes = useStyles()
  const [data, setData] = useState({})
  const [flag, setFlag] = useState(false)
  const dispatch = useDispatch()
  const settingData = useSelector((state) => state.auth.adminSettingData)


  useEffect(() => {

    if (Object.keys(settingData).length == 0) {
      dispatch(getAdminSetting())
    } else {
      setData(settingData)
      setFlag(true)
    }

  }, [settingData])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.checked })
    console.log(e.target.checked, e.target.name)
    dispatch(updateAdminSetting(settingData._id, { [e.target.name]: e.target.checked }))
  }


  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Admin Settings</h4>
          </CardHeader>
          <CardBody>
            {flag ? <form>
              <GridContainer>
                <GridItem xs={6} sm={6} md={6}>
                  <FormLabel component="legend">Mobile User Sigup Login Status</FormLabel>
                  <FormControlLabel
                    control={<Switch checked={data?.mobile_user_status} onChange={handleChange} name="mobile_user_status" />}
                    label="True"
                  />
                </GridItem>
                <GridItem xs={6} sm={6} md={6}>
                  <FormLabel component="legend">Web User Signup Login Status</FormLabel>
                  <FormControlLabel
                    control={<Switch checked={data?.web_user_status} onChange={handleChange} name="web_user_status" />}
                    label="True"
                  />
                </GridItem>
              </GridContainer>
            </form> : <Loader />}
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default AdminSetting
