import React from 'react'
import { Drawer as MUIDrawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import UserIcon from '@material-ui/icons/MoveToInbox';
import AdIcon from '@material-ui/icons/Notifications';
import HomeIcon from '@material-ui/icons/Home';

import { withRouter, Link } from 'react-router-dom';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },

  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
}));
const Drawer = props => {
  const { history } = props
  const classes = useStyles();
  const itemsList = [{
    text: "Home",
    icon: <HomeIcon />,
    path: "/dashboard/",
    onClick: () => history.push('/dashboard/')
  }, {
    text: "Users",
    icon: <UserIcon />,
    path: "/dashboard/user",
    onClick: () => history.push('/dashboard/user')
  },
  {
    text: "Ads",
    icon: <AdIcon />,
    path: "/dashboard/ads",
    onClick: () => history.push('/dashboard/ads')
  }];
  return (
    <MUIDrawer variant="permanent" className={classes.drawer}>
      <List>
        {itemsList.map((item, index) => {
          const { text, icon, onClick, path } = item
          return (

            <Link button key={text} to={path} >
              <ListItem>
                {icon && <ListItemIcon>{icon}</ListItemIcon>}
                <ListItemText primary={text} />
              </ListItem>

            </Link>

          )
        })}
      </List>
    </MUIDrawer>
  )
}

export default withRouter(Drawer);