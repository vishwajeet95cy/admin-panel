import React, { useEffect, useDebugValue, useState, useCallback, useMemo } from "react";


function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  //console.log('Stat', stabilizedThis)
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    //console.log(order)
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

var AllData = [];

const useData = (data, order, orderBy) => {

  //console.log('Custom Hook Value', data, order, orderBy)
  const [ord, setOrd] = useState(order)
  const [value, setValue] = useState([])
  const NewValue = useMemo(() => data, [data])


  useEffect(() => {

    if (value.length == 0 && data.length > 0) {
      console.log('Use Effect Custom Hook')
      handleFunction(data, order, orderBy)
    }

  }, [value, data])

  const handleFunction = (data, order, orderBy) => {
    setValue(stableSort(data, getComparator(order, orderBy)))
  }

  const fun = useCallback((ord, ordBy) => {
    setValue(stableSort(data, getComparator(ord, ordBy)))
  }, [value])

  const newFunction = useCallback((data, order, orderBy) => {
    handleFunction(data, order, orderBy)
  }, [value])

  //console.log('Custom Hook Value', value)

  return [value, fun, newFunction]
}

export default useData