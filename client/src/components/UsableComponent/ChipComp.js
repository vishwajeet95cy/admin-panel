import React from 'react'
import { Chip } from '@material-ui/core'

const ChipComp = (data) => {
  return <Chip style={{ marginBottom: '2px' }} label={data.val} ></Chip>
}

export default ChipComp
