import { CircularProgress, makeStyles } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles((theme) => ({
  ss: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
    position: "absolute",
    left: 0,
    right: 0,
    zIndex: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
}))

const Loader = () => {

  const classes = useStyles()

  return (
    <div className={classes.ss}>
      <CircularProgress />
    </div>
  )
}

export default Loader
