import React, { useState } from 'react'
import ReactMapboxGl, { Layer, Feature, Marker, Popup, ZoomControl } from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1IjoibWFkaHVyNDk5NCIsImEiOiJja3JienlqbXE0eHJ0MnJubGRtdm4xeW1tIn0.ihs3BrYTltvhmjWd4VwR8Q'
});

const MapLocation = (datas) => {

  const [zoomLevel, setZoomLevel] = useState(2);
  // const [selectedHotspot, setselectedHotspot] = useState(null);

  // const closePopup = () => {
  //   setselectedHotspot(null)
  // };

  return (
    <Map
      style="mapbox://styles/mapbox/streets-v11"
      center={[78.9629, 20.5937]}
      zoom={[zoomLevel]}
      onStyleLoad={() => setZoomLevel(4.001)}
      containerStyle={{
        height: '85vh',
        width: '57vw'
      }}
    >
      <Layer type={(datas.type != null || datas.type != undefined) ? datas.type : 'circle'}
        id="marker"
        paint={(datas.paint != null || datas.paint != undefined) ? datas.paint : {
          "circle-color": "#ff5200",
          "circle-stroke-width": 1,
          "circle-stroke-color": "#fff",
          "circle-stroke-opacity": 1
        }} >
        {datas.children}
      </Layer>
      {datas.popup}
      {/* {selectedHotspot !== null ? (
        <Popup
          coordinates={[selectedHotspot.lng, selectedHotspot.lat]}
          onClick={closePopup}
          offset={{
            'bottom-left': [12, -38], 'bottom': [0, -38], 'bottom-right': [-12, -38]
          }}
        >
          <div>
            <p>Hello</p>
          </div>
        </Popup>
      ) : null} */}
    </Map>
  )
}

// const placeholder = document.createElement('div');
// ReactDOM.render(el, placeholder);

// new mapboxgl.Popup({
//   closeButton: true,
//   closeOnClick: true,
//   anchor: 'bottom-left',
// })
//   .setLngLat(coordinates)
//   .setDOMContent(placeholder)
//   .addTo(this.map);

export default MapLocation
