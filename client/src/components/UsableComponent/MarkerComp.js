import React, { useRef } from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { Marker, Popup } from 'react-leaflet'


const MarkerComp = (datas) => {

  const [open, setOpen] = useState(datas.id)
  const markerRef = useRef()
  const popupRef = useRef()

  useEffect(() => {
    console.log('Marker Camp UserEffect')
    if (open) {
      markerRef.leafletElement.openPopup()
      console.log('open streted')
    }
  }, [open])

  return (
    <Marker ref={markerRef} position={datas.latLang}>
      <Popup open ref={popupRef}>
        {datas.name} <br /> {datas.count}
      </Popup>
    </Marker>
  )
}

export default MarkerComp
