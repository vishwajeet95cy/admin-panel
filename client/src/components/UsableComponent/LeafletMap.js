import React from 'react'
import { MapContainer, TileLayer, LayersControl } from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import Retina from '../../assets/marker-icon-2x.png';
import icon from '../../assets/marker-icon.png';
import shadow from '../../assets/marker-shadow.png';

const center = [20.5937, 78.9629]

const LeafletMap = (datas) => {

  React.useEffect(() => {
    const L = require("leaflet");

    delete L.Icon.Default.prototype._getIconUrl;


    L.Icon.Default.mergeOptions({
      iconRetinaUrl: Retina,
      iconUrl: icon,
      shadowUrl: shadow
    });
  }, []);

  return (
    <MapContainer center={center} zoom={5} scrollWheelZoom={true} style={{
      width: '100%',
      height: '100vh'
    }}>
      <LayersControl position="topright">
        <LayersControl.BaseLayer checked name="OpenStreetMap.Mapnik">
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
        </LayersControl.BaseLayer>
        <LayersControl.BaseLayer name="OpenStreetMap.BlackAndWhite">
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
          />
        </LayersControl.BaseLayer>
        {datas.children}
        {datas.popup}
      </LayersControl>
    </MapContainer>
  )
}

export default React.memo(LeafletMap)
