import React from 'react'
import clsx from 'clsx';
import { lighten, Toolbar, Typography, makeStyles } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { Edit, Delete } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  title: {
    flex: '1 1 100%',
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
}));

const NewDelete = (data) => {

  const classes = useStyles()

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: data.length > 0,
      })}
    >
      {data.length > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {data.length} selected
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          {data.name}
        </Typography>
      )}

      {data.length > 0 ? <React.Fragment>
        {(!data.data || data.data == null || data.data == undefined || data.data.length == 0) ? null : data.data.map((cc, i) => {
          if (cc.lcheck) {
            if (data.length === cc.lcheck) {
              return <Tooltip key={i} title={cc.name}>
                <IconButton onClick={data[cc.name]} style={{ fontSize: '21px' }}>
                  {cc.name}
                  <Edit style={{ width: '18px' }}></Edit>
                </IconButton>
              </Tooltip>
            }
          } else {
            return <Tooltip key={i} title={cc.name}>
              <IconButton onClick={data[cc.name]} style={{ fontSize: '21px' }}>
                {cc.name}
                <Edit style={{ width: '18px' }}></Edit>
              </IconButton>
            </Tooltip>
          }
        })}
        <Tooltip title="delete">
          <IconButton onClick={data.click} style={{ fontSize: '21px' }}>
            delete
            <Delete style={{ width: '18px' }}></Delete>
          </IconButton>
        </Tooltip>
      </React.Fragment> : null}
    </Toolbar>
  )
}

export default NewDelete
