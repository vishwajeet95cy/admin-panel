import React from 'react'
import {
  makeStyles,
  LinearProgress
} from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  ss: {
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
    position: "absolute",
    left: 0,
    right: 0,
    zIndex: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    position: 'fixed',
    top: '64px'
  },
}))

const ApiLoader = () => {

  console.log('Loader  render')
  const classes = useStyles()
  return (
    <div className={classes.ss}>
      <LinearProgress />
    </div>
  )
}

export default React.memo(ApiLoader)
