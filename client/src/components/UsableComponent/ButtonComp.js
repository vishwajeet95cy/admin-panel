import { Chip } from '@material-ui/core'
import React from 'react'

const ButtonComp = (data) => {
  return (
    <Chip style={(data.value === true) ? { color: 'white', backgroundColor: 'green' } : { color: 'white', backgroundColor: 'red' }} label={(data.value === true) ? 'true' : 'false'} ></Chip>
  )
}

export default ButtonComp
