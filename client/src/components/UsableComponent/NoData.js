import React, { useState } from "react";
import nodataicon from "../../assets/Nodata1.svg";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '400px',
    height: '400px'
  }
}));


const NoData = () => {
  const classes = useStyles();
  return (
    <Container component="main" maxWidth="xs">

      <div className={classes.paper}>
        <img src={nodataicon} alt="No data found...." />
      </div>
    </Container>
  );
};
export default NoData;
