import React from 'react'
import ReactHlsPlayer from 'react-hls-player';

const ReactPlayer = ({ src }) => {
  return (
    <ReactHlsPlayer
      src={src}
      autoPlay={false}
      controls={true}
      width="100%"
      height="175px"
    />
  )
}

export default ReactPlayer
