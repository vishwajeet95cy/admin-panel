const getRandomColor = () => {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

const dateDifference = (date1, date2) => {
  var Difference_In_Time = Math.abs(new Date(date2).getTime() - new Date(date1).getTime());
  return Math.ceil(Difference_In_Time / (1000 * 3600 * 24));
}

export { getRandomColor, dateDifference }