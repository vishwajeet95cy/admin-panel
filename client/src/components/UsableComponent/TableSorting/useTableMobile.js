import React, { useState } from 'react'
import { Table, TableHead, TableRow, TableCell, makeStyles, TableSortLabel } from '@material-ui/core'
import styles from "./tableStyle";

const useStyles = makeStyles(styles)

export default function useTableMobile(records, headCells, filterFn) {

  const classes = useStyles();

  const [order, setOrder] = useState()
  const [orderBy, setOrderBy] = useState()

  const TblContainer = props => (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {props.children}
      </Table>
    </div>
  )

  const TblHead = props => {

    const handleSortRequest = cellId => {
      const isAsc = orderBy === cellId && order === "asc";
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(cellId)
    }

    return (<TableHead className={classes[props.tableHeaderColor + "TableHeader"]}>
      <TableRow className={classes.tableHeadRow}>
        {
          headCells.map(headCell => (
            <TableCell key={headCell.label}
              sortDirection={orderBy === headCell.id ? order : false} className={classes.tableCell + " " + classes.tableHeadCell}>
              {headCell.disableSorting ? headCell.label :
                <TableSortLabel
                  active={orderBy === headCell.id}
                  direction={orderBy === headCell.id ? order : 'asc'}
                  onClick={() => { handleSortRequest(headCell.id) }}>
                  {headCell.label}
                </TableSortLabel>
              }
            </TableCell>))
        }
      </TableRow>
    </TableHead>)
  }

  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  function getComparator(order, orderBy) {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const recordsAfterPagingAndSorting = () => {
    return stableSort(filterFn.fn(records), getComparator(order, orderBy))
  }

  return {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  }
}