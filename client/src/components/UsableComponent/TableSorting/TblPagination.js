import { TablePagination } from '@material-ui/core'
import React from 'react'

const TblPagination = ({ page, pages, rowsPerPage, count, handleChangePage, handleChangeRowsPerPage }) => {
  return (
    <TablePagination
      component="div"
      page={page}
      rowsPerPageOptions={pages}
      rowsPerPage={rowsPerPage}
      count={count}
      onPageChange={handleChangePage}
      onRowsPerPageChange={handleChangeRowsPerPage}
    />
  )
}

export default TblPagination
