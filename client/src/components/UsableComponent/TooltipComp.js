import { Button } from '@material-ui/core'
import { Tooltip } from '@material-ui/core'
import React from 'react'

const TooltipComp = (data) => {
  return (
    <Tooltip title={data.value} interactive>
      {data.children}
    </Tooltip>
  )
}

export default TooltipComp
