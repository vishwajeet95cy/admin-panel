import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Dialog, DialogActions, DialogContent, DialogTitle, TableBody, TableCell, TableRow, Box, IconButton } from '@material-ui/core';
import { Close, Edit, Delete } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import { GetAllVideo, convertVideo } from '../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import DefaultImage from '../assets/image.jpg'
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
import NewDelete from './UsableComponent/NewDelete';

//newviews
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import useTable from './UsableComponent/TableSorting/useTable';
import Button from '../newviews/CustomButtons/Button'
import ReactPlayer from './UsableComponent/ReactPlayer';
import TblPagination from './UsableComponent/TableSorting/TblPagination';
import NewPlayer from './UsableComponent/NewPlayer';
import { Player } from 'video-react';


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

var VideoMeta

const HeaderArr = [
  { name: 'edit', lcheck: 1 }
]

const headCells = [
  {
    id: "",
    numeric: false,
    disablePadding: false,
    label: "",
    disableSorting: true
  },
  {
    id: "_id",
    numeric: false,
    disablePadding: false,
    label: "Id",
  },
  {
    id: "title",
    numeric: false,
    disablePadding: false,
    label: "Title",
  },
  {
    id: "thumbnail",
    numeric: false,
    disablePadding: false,
    label: "Thumbnail",
  },
  {
    id: "video",
    numeric: false,
    disablePadding: false,
    label: "video",
  },
  {
    id: "video_status",
    numeric: false,
    disablePadding: false,
    label: "Video Status",
  },
  {
    id: "ready_for_stream",
    numeric: false,
    disablePadding: false,
    label: "Conversion Status",
  },
  {
    id: "c",
    numeric: false,
    disablePadding: false,
    label: "Convert Video",
    disableSorting: true
  },
  {
    id: "stream_count",
    numeric: false,
    disablePadding: false,
    label: "Stream Count",
  },
  {
    id: "check",
    numeric: false,
    disablePadding: false,
    label: "Video Stream",
    disableSorting: true
  },
  {
    id: "videoMeta",
    numeric: false,
    disablePadding: false,
    label: "Video MetaData",
    disableSorting: true
  },
]

const Videos = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [data, setData] = useState([])
  const history = useHistory()
  const [open, setOpen] = useState(false)
  const [value, setValue] = useState('')
  const [image, setImage] = useState(false)
  const [img, setImg] = useState('')
  const [vid, setVid] = useState(false)
  const [stream, setStream] = useState('')
  const [meta, setMeta] = useState(false)

  const AllData = useSelector(state => state.auth.videoDetail)
  const totalCount = useSelector(state => state.auth.videoTotal)
  const [flag, setFlag] = useState(false)


  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== AllData.length) {
      dispatch(GetAllVideo(data.length, 10));
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (AllData.length === 0) {
      dispatch(GetAllVideo(0, 10));
    } else {
      setData(AllData)
      setFlag(true)
    }
    return () => { setData([]); setId([]); }
  }, [AllData])

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(data, headCells, filterFn)

  const handleClose = () => {
    setValue('')
    setOpen(false)
  }

  const handleOpen = (e, data) => {
    setValue(data.video)
    setOpen(true)
  }

  const handleImage = () => {
    setImage(false)
  }

  const handleOpenImage = (e, data) => {
    setImage(true);
    setImg(data.thumbnail)
  }

  const handleConvertVide = (e, data) => {
    console.log('Video Id', data)
    dispatch(convertVideo(data))
  }

  const handleStream = () => {
    setVid(false)
  }

  const handleOpenStream = (e, data) => {
    setVid(true);
    setStream(data.stream_path)
  }


  const addWorkId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item != e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const EditButton = () => {
    history.push('/dashboard/editVideo', { from: history.location.pathname, id: id[0] })
  }

  const deleteButton = () => {
    console.log('Button Clicked')
  }

  const handleRefresh = () => {
    dispatch(GetAllVideo(0, 10));
  }

  const handleMetaInfo = (data) => {

    VideoMeta = <GridContainer spacing={3}>
      {(data != null || data != undefined || Object.keys(data).length > 0) ? <React.Fragment>
        <GridItem item xs={12} md={12} sm={12}>
          <h4>Video Detail</h4>
          <div>{JSON.stringify(data.video_detail)}</div>
        </GridItem>
        <GridItem item xs={12} md={12} sm={12}>
          <h4>Audio Detail</h4>
          <div>
            {JSON.stringify(data.audio_detail)}
          </div>
        </GridItem>
      </React.Fragment> : <GridItem item xs={12} md={12} sm={12}>
        <div>No Meta Data Found</div>
      </GridItem>}
    </GridContainer>
    setMeta(true)

  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              <NewDelete name="All Video Details" length={numSelected} data={HeaderArr} edit={EditButton} click={deleteButton} />
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }}>Refresh</Button>
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              <TableBody>
                {flag ? (
                  (data.length === 0) ? <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <NoData />
                    </TableCell>
                  </TableRow> : recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((items) => {
                    return (
                      <TableRow key={items._id}>
                        <TableCell>
                          <input
                            type="checkbox"
                            className="ageList"
                            checked={id.includes(items._id)}
                            onChange={(e) => addWorkId(e)}
                            id={items._id}
                            name={items._id}
                            value={items._id}
                          />
                        </TableCell>
                        <TableCell>{items._id}</TableCell>
                        <TableCell>{items.title}</TableCell>
                        <TableCell onClick={(e) => { handleOpenImage(e, items) }}><img width="40px" height="40px" src={items.thumbnail} alt="Unkown" style={{ cursor: "pointer" }} onError={(e) => { e.target.onerror = null; e.target.src = DefaultImage }} /></TableCell>
                        <TableCell onClick={(e) => { handleOpen(e, items) }}><img width="40px" height="40px" src={items.thumbnail} alt="Unkown" style={{ cursor: "pointer" }} onError={(e) => { e.target.onerror = null; e.target.src = DefaultImage }} /></TableCell>
                        <TableCell>{items.video_status}</TableCell>
                        <TableCell>
                          {(items.ready_for_stream == true) ? 'true' : 'false'}
                        </TableCell>
                        <TableCell>
                          <Button variant="contained" color="primary" round onClick={(e) => {
                            handleConvertVide(e, items._id)
                          }} >Convert Video</Button>
                        </TableCell>
                        <TableCell>
                          {items.stream_count}
                        </TableCell>
                        <TableCell onClick={(e) => { handleOpenStream(e, items) }}>
                          <img width="40px" height="40px" src={items.thumbnail} alt="Unkown" style={{ cursor: "pointer" }} onError={(e) => { e.target.onerror = null; e.target.src = DefaultImage }} />
                        </TableCell>
                        <TableCell onClick={() => { handleMetaInfo(items.videoMeta) }}>
                          {(items.videoMeta == null || items.videoMeta == null || Object.keys(items.videoMeta).length == 0) ? 'No Meta Found' : 'Click To View'}
                        </TableCell>
                      </TableRow>
                    )
                  })
                ) : <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>}
              </TableBody>
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Video</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <video width="100%" height="100%" controls autoPlay>
            <source src={value} type="video/mp4" />
            Your browser does not support the <code>video</code> element.
          </video>
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={image}
        onClose={handleImage}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Image</Box>
          <Box>
            <IconButton onClick={handleImage}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <img src={img} width="100%" height="100%" alt="Unknown" />
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" onClick={handleImage} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={vid}
        onClose={handleStream}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Video Stream</Box>
          <Box>
            <IconButton onClick={handleStream}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <ReactPlayer src={stream} />
          {/* <Player>
            <NewPlayer
              isVideoChild
              src={stream}
            />
          </Player> */}
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" onClick={handleStream} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={meta}
        onClick={() => {
          setMeta(false)
        }}
        maxWidth="md"
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Video Meta Data</Box>
          <Box>
            <IconButton onClick={() => {
              setMeta(false)
            }}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          {VideoMeta}
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" onClick={() => {
            setMeta(false)
          }} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  )
}


export default Videos