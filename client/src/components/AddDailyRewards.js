import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button, Card, Container, MenuItem, Select, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { CreateDailyReward } from '../redux/actions';

const useStyles = makeStyles({
  ButtonStyles: {
    marginBottom: "5px",
    marginLeft: "7px",
  },
  TypographyStyles: {
    textAlign: "center",
    padding: "5px"
  },
  Select: {
    paddingRight: '150px !important',
    position: 'relative',
    top: '15px',
    right: '-15px'
  },
  ads: {
    position: 'relative',
    right: '-15px'
  }
})


function AddDailyRewards() {

  const dispatch = useDispatch()
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState({
    "rewardsname": "",
    "totalads": "",
    "boostvalue": "",
    "timevalue": "",
    "validityvalue": "",
    "coinvalue": "",
    "cointype": "",
    "ads_type": ""
  });

  const onEnterData = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }


  const LoadingTrue = useSelector(state => state.auth.loader);

  const onsubmit = () => {
    console.log(data)
    const datas = {
      rewards_name: data.rewardsname,
      total_ads: data.totalads,
      boost_value: data.boostvalue,
      coinvalue: data.coinvalue,
      coinunit: data.cointype,
      timevalue: data.timevalue,
      timeunit: data.validityvalue,
      ads_type: data.ads_type
    }
    dispatch(CreateDailyReward(datas))
  }

  const Create = useSelector(state => state.auth.created)

  useEffect(() => {
    if (Create) {
      history.push('/dashboard/dailyrewards')
    }
  }, [Create])


  return (
    <Container component="main" maxWidth="xl" >
      <div className={classes.ContainerStyles} >
        <Card >
          <Typography className={classes.TypographyStyles} component="h1" variant="h5">
            Create Rewards
          </Typography>
          <form id="form-new">
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="rewardsname"
              label="Rewards Name"
              name="rewardsname"
              autoComplete="name"
              autoFocus
              onChange={onEnterData}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="totalads"
              label="Total Ads"
              type="text"
              id="totalads"
              autoComplete="totalads"
              onChange={onEnterData}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="boostvalue"
              label="Boost value"
              type="text"
              id="boostvalue"
              autoComplete="boostvalue"
              onChange={onEnterData}
            />
            <div>
              <h2>Validity:</h2>
              <TextField
                variant="outlined"
                margin="normal"
                required
                name="timevalue"
                label="Time Value"
                type="text"
                id="timevalue"
                autoComplete="value"
                onChange={onEnterData}
              />
              <Select
                variant="outlined"
                margin="none"
                label="validity Time"
                required
                name="validityvalue"
                type="text"
                id="value"
                autoComplete="value"
                className={classes.Select}
                onChange={onEnterData}
              >
                <MenuItem value="Day">Day</MenuItem>
                <MenuItem value="Month">Month</MenuItem>
                <MenuItem value="Year">Year</MenuItem>
              </Select>
            </div>
            <div>
              <h2>Total Ads Coin:</h2>
              <TextField
                variant="outlined"
                margin="normal"
                required
                name="coinvalue"
                label="Coin Value"
                type="text"
                id="coinvalue"
                autoComplete="value"
                onChange={onEnterData}
              />
              <Select
                variant="outlined"
                margin="none"
                label="coin type"
                required
                name="cointype"
                type="text"
                id="coin type"
                autoComplete="coin type"
                className={classes.Select}
                onChange={onEnterData}
              >
                <MenuItem value="hc">hc</MenuItem>
              </Select>
            </div>
            <div style={{ marginBottom: "8px" }}>
              <h1>Ads Type:</h1>
              <Select
                variant="outlined"
                margin="none"
                label="Ads type"
                required
                name="ads_type"
                type="text"
                id="Ads type"
                autoComplete="Ads type"
                className={classes.ads}
                onChange={onEnterData}
              >
                <MenuItem value="Static">Static</MenuItem>
                <MenuItem value="Dynamic">Dynamic</MenuItem>
              </Select>
            </div>
            <Button
              type="button"
              variant="contained"
              color="primary"
              className={classes.ButtonStyles}
              onClick={() => { history.push('/dashboard/dailyrewards') }}
            >
              Back
            </Button>
            <Button
              type="button"
              variant="contained"
              color="primary"
              disabled={LoadingTrue ? true : false}
              className={classes.ButtonStyles}
              onClick={onsubmit}
            >
              Submit
            </Button>
          </form>
        </Card>
      </div>
    </Container>
  );
}

export default AddDailyRewards
