import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { Chip, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getAllAds, GetAllVideo, ChangeAdsVideo } from '../redux/actions';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
import TooltipComp from './UsableComponent/TooltipComp';
import NewDelete from './UsableComponent/NewDelete';
// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import useTable from './UsableComponent/TableSorting/useTable';
import Button from '../newviews/CustomButtons/Button'
import TblPagination from './UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const HeaderArr = [
  { name: 'edit', lcheck: 1 },
]

const headCells = [
  {
    id: "",
    numeric: false,
    disablePadding: false,
    label: "",
    disableSorting: true
  },
  {
    id: "_id",
    numeric: false,
    disablePadding: false,
    label: "Id",
  },
  {
    id: "ads_name",
    numeric: false,
    disablePadding: false,
    label: "Ads Name",
  },
  {
    id: "_id",
    numeric: false,
    disablePadding: false,
    label: "Title",
  },
  {
    id: "video",
    numeric: false,
    disablePadding: false,
    label: "Video Id",
    disableSorting: true
  },
  {
    id: "heading",
    numeric: false,
    disablePadding: false,
    label: "Heading",
  },
  {
    id: "description",
    numeric: false,
    disablePadding: false,
    label: "Description",
  },
  {
    id: "callToAction",
    numeric: false,
    disablePadding: false,
    label: "Call To Action",
  },
  {
    id: "trackingTemplate",
    numeric: false,
    disablePadding: false,
    label: "Tracking Template",
  },
  {
    id: "finalURLSuffix",
    numeric: false,
    disablePadding: false,
    label: "Finale URL Suffix",
  },
  {
    id: "finalURLSuffix",
    numeric: false,
    disablePadding: false,
    label: "Visibility",
  },
]

const Ads = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const classes = useStyles();
  const [ads, setAds] = useState([])
  const [video, setVideo] = useState([])

  const AllAds = useSelector(state => state.auth.adsDetail)
  const totalCount = useSelector(state => state.auth.adsTotal)
  const AllVideo = useSelector(state => state.auth.videoDetail)
  const Updated = useSelector(state => state.auth.updated)
  const [flag, setFlag] = useState(false)
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount != ads.length) {
      dispatch(getAllAds(ads.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }


  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)

  useEffect(() => {

    if (AllAds.length === 0) {
      dispatch(getAllAds(0, 10));
      dispatch(GetAllVideo(0, 10));
    } else {
      setAds(AllAds);
      setVideo(AllVideo);
      setFlag(true)
    }
    if (Updated) {
      dispatch(getAllAds(0, 10));
    }
    return () => { setAds([]); setVideo([]); setId([]) }
  }, [AllAds, Updated, AllVideo])

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(ads, headCells, filterFn)

  const ChangeVideo = (e, data) => {
    const file = {
      vid: e.target.value
    }
    dispatch(ChangeAdsVideo(data._id, file))
  }

  const addWorkId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item != e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const EditButton = () => {
    history.push(`/dashboard/editads`, {
      from: history.location.pathname,
      id: id[0]
    })
  }

  const DeleteButton = () => {
    console.log('Delete Button Clicked')
  }

  const handleRefresh = () => {
    dispatch(getAllAds(0, 10));
    dispatch(GetAllVideo(AllVideo.length, 10));
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12} >
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              <NewDelete name="All Ads Details" length={numSelected} data={HeaderArr} edit={EditButton} click={DeleteButton} />
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              <TableBody>
                {flag ? (
                  (ads.length === 0) ? <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <NoData />
                    </TableCell>
                  </TableRow> : recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((datas) => {
                    return (
                      <TableRow key={datas._id}>
                        <TableCell>
                          <input
                            type="checkbox"
                            className="ageList"
                            checked={id.includes(datas._id)}
                            onChange={(e) => addWorkId(e)}
                            id={datas._id}
                            name={datas._id}
                            value={datas._id}
                          />
                        </TableCell>
                        <TableCell component="th" scope="row">{datas._id}</TableCell>
                        <TableCell>{(!datas.ads_name || datas.ads_name == null || datas.ads_name == undefined) ? datas.video_id.title : datas.ads_name}</TableCell>
                        <TableCell>{datas.video_id.title}</TableCell>
                        <TableCell>
                          <select style={{ margin: '6px', padding: '6px', borderRadius: '11px' }} value={datas.video_id ? datas.video_id._id : null} onChange={(e) => { ChangeVideo(e, datas) }}>
                            {video.map((item) => {
                              if (item.user_id == datas.video_id.user_id._id) {
                                return (
                                  <option key={item._id} value={item._id}>{item.title}</option>
                                )
                              }
                            })}
                          </select>
                        </TableCell>
                        <TableCell>{datas.heading}</TableCell>
                        <TableCell>
                          <TooltipComp value={datas.description}>
                            <Chip label="description"></Chip>
                          </TooltipComp>
                        </TableCell>
                        <TableCell>{datas.callToAction}</TableCell>
                        <TableCell>
                          <Chip href={datas.trackingTemplate} component="a" target="_blanck" label="Click Me" style={{ cursor: 'pointer' }} ></Chip>
                        </TableCell>
                        <TableCell>{datas.finalURLSuffix}</TableCell>
                        <TableCell>{datas.video_id.video_status}</TableCell>
                      </TableRow>
                    )
                  })
                ) : <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>}
              </TableBody>
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default Ads
