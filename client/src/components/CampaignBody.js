import React, { useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Collapse, Box, IconButton, Typography } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { StatusInput } from './utils/TargetStatus';
import ButtonComp from './UsableComponent/ButtonComp';
import { Chip } from '@material-ui/core';
import moment from 'moment';


const CampaignBody = (props) => {

  const { datas, setCheck, ChangeAuth, AuthId } = props

  const [open, setOpen] = useState(false)

  return (
    <React.Fragment>
      <TableRow key={datas._id}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>
          <input
            type="checkbox"
            className="chekboxCampaignList"
            onChange={(e) => setCheck(e, datas)}
            id={datas._id}
            name={datas._id}
            value={datas._id}
            checked={AuthId.includes(datas._id) ? true : false}
          />
        </TableCell>
        <TableCell>{datas.campaign_name}</TableCell>
        <TableCell>{datas.campaign_type}</TableCell>
        <TableCell>
          <ButtonComp value={datas.campaign_status} />
        </TableCell>
        <TableCell>
          <ButtonComp value={datas.campaign_active} />
        </TableCell>
        <TableCell>{datas.campaign_budget}</TableCell>
        <TableCell>
          <select style={{ margin: '6px', padding: '6px', borderRadius: '11px' }} value={datas.auth_id ? datas.status : null} onChange={(e) => { ChangeAuth(e, datas.auth_id) }}>
            {StatusInput.map((item) => {
              return (
                <option key={item.id} value={item.name}>{item.name}</option>
              )
            })}
          </select>
        </TableCell>
        <TableCell>&#8377;{(datas.budget_type == 'Daily') ? datas.budget + '/day' : datas.budget + '(total) ' + datas.start_date + '- ' + datas.end_date}</TableCell>
        <TableCell>{datas.budget_type}</TableCell>
        <TableCell>{datas.schedule}</TableCell>
        <TableCell>{datas.start_date}</TableCell>
        <TableCell>{datas.end_date}</TableCell>
        <TableCell>{moment(datas.date).format('LLLL')}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0, backgroundColor: '#eeeeee' }} colSpan={14}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Target
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Target Name</TableCell>
                    <TableCell>Ads Name</TableCell>
                    <TableCell>Ads Type</TableCell>
                    <TableCell>Video Title</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {datas.target_id.map((cc, i) => {
                    return (<TableRow key={i}>
                      <TableCell>{cc.name}</TableCell>
                      <TableCell>{(!cc.ads_id.ads_name || cc.ads_id.ads_name == null || cc.ads_id.ads_name == undefined) ? cc.ads_id.video_id.title : cc.ads_id.ads_name}</TableCell>
                      <TableCell>{cc.ads_id.ads_type_id.ads_name}</TableCell>
                      <TableCell>{cc.ads_id.video_id.title}</TableCell>
                    </TableRow>)
                  })}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

export default React.memo(CampaignBody)
