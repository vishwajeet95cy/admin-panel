import React, { useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { useSelector, useDispatch } from 'react-redux';
import { Userlogin } from '../redux/actions/index';
import { CssBaseline, makeStyles } from '@material-ui/core';
import Loader from './UsableComponent/Loader';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(25),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const LoadingTrue = useSelector(state => state.auth.loader);

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const login = () => {
    const data = {
      username,
      password
    }
    console.log(data);
    dispatch(Userlogin(data))
  }

  return (
    <Container component="main" maxWidth="sm" >
      <CssBaseline />
      <div className={classes.paper} >
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        {LoadingTrue ? <Loader /> : null}
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(e) => { setUsername(e.target.value) }}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => { setPassword(e.target.value) }}
          />
          <Button
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            onClick={login}
            className={classes.ButtonStyles}
          >
            Sign In
          </Button>
          <Button
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            forgot Password?
          </Button>
        </form>
      </div>
    </Container>
  )
}

export default Login
