import React, { useEffect, useState } from 'react';
import { Grid, Typography, FormControl, FormLabel, TextField, makeStyles, Button } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import BaseUrl from '../utils/BaseUrl';
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { SingleWebUsers, UpdateWebUser } from '../../redux/actions';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

const EditWebUser = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const param = useParams()
  const classes = useStyles()
  const [thumb, setThumb] = useState()
  const [img, setImg] = useState()
  // const [value, setValue] = useState({
  //   name: '',
  //   location: '',
  //   website: '',
  //   photos: '',
  //   email: '',
  //   phone_number: '',
  //   address: '',
  //   GSTIN: '',
  //   google: '',
  //   facebook: ''
  // });

  const [value, setValue] = useState({})
  const [initial, setInitial] = useState(true)

  const WebData = useSelector(state => state.auth.singleWebUser);
  const Updated = useSelector(state => state.auth.updated);

  useEffect(() => {
    if (initial) {
      dispatch(SingleWebUsers(param.id));
      setInitial(false)
    }

    setValue(WebData)

    if (Updated) {
      history.push('/dashboard/users/webuser')
    }
    return () => { setValue({}) }
  }, [WebData, Updated])

  const onChange = (e) => {
    if (e.target.name == "name") {
      setValue({ ...value, profile: { ...value.profile, name: e.target.value } })
    } else if (e.target.name == "website") {
      setValue({ ...value, profile: { ...value.profile, website: e.target.value } })
    } else if (e.target.name == "location") {
      setValue({ ...value, profile: { ...value.profile, location: e.target.value } })
    } else {
      setValue({ ...value, [e.target.name]: e.target.value })
    }
  }

  const onThumb = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;
    const data = e.target.files[0]
    console.log(data)
    if (e.target.files[0].size > 30720000) {
      alert("File size too big! Only files less than 30 MB are allowed");
    } else if (!data.name.match(allowedExtensions)) {
      alert("Only jpeg and png files are allowed!");
    } else {
      console.log(e.target.files[0]);
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImg(fileURL)
      setThumb(e.target.files[0])
    }
  };

  const handleSubmit = () => {
    var data = new FormData();
    data.append('name', value.profile.name)
    data.append('location', value.profile.location)
    data.append('website', value.profile.website)
    data.append('photos', value.photos)
    data.append('email', value.email)
    data.append('google', value.google)
    data.append('facebook', value.facebook)
    data.append('phone_number', value.phone_number)
    data.append('GSTIN', value.GSTIN)
    data.append('address', value.address)
    data.append('thumbnail', thumb)

    dispatch(UpdateWebUser(param.id, data));
  }

  return (
    <React.Fragment>
      <div className={classes.paper}>
        <Typography variant="h5" component="h1">
          Edit Web User Detail
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Google</FormLabel>
                <TextField
                  name="google"
                  onChange={onChange}
                  value={value.google}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Facebook</FormLabel>
                <TextField
                  name="facebook"
                  onChange={onChange}
                  value={value.facebook}
                  variant="outlined" />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Name</FormLabel>
                <TextField
                  name="name"
                  onChange={onChange}
                  value={(!value.profile || value.profile.name == null || value.profile.name == undefined) ? '' : value.profile.name}
                  variant="outlined" />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Email</FormLabel>
                <TextField
                  name="email"
                  onChange={onChange}
                  value={value.email}
                  variant="outlined" />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Website</FormLabel>
                <TextField
                  name="website"
                  onChange={onChange}
                  value={(!value.profile || value.profile.website == null || value.profile.website == undefined) ? '' : value.profile.website}
                  variant="outlined" />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Location</FormLabel>
                <TextField
                  name="location"
                  onChange={onChange}
                  value={(!value.profile || value.profile.location == null || value.profile.location == undefined) ? '' : value.profile.location}
                  variant="outlined" />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>GSTIN</FormLabel>
                <TextField
                  name="GSTIN"
                  onChange={onChange}
                  value={value.GSTIN}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Address</FormLabel>
                <TextField
                  name="address"
                  onChange={onChange}
                  value={value.address}
                  variant="outlined" />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Phone Number</FormLabel>
                <TextField
                  name="phone_number"
                  onChange={onChange}
                  value={value.phone_number}
                  variant="outlined" />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Image</FormLabel>
                <TextField
                  id="image"
                  name="photo"
                  type="file"
                  variant="outlined"
                  onChange={onThumb}
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <div><div>Old Image</div>
                <img src={(!value.profile || value.profile.photos == null || value.profile.photos == undefined) ? '' : value.profile.photos} width="80px" height="80px" alt="Unknown" /></div>
            </Grid>
            <Grid item xs={6}>
              {(img === undefined || img === null || img === "") ? '' : <div><div>New Image</div>
                <img src={img} width="80px" height="80px" alt="Unknown" /></div>}
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => { history.push('/dashboard/users/webuser') }}>Back</Button>

            </Grid>
            <Grid item xs={6}>

              <Button type="button" variant="contained" fullWidth color="primary" onClick={() => { handleSubmit() }}
              >Submit</Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </React.Fragment>
  )
}

export default EditWebUser
