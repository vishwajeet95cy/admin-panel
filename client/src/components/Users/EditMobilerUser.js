import React, { useEffect, useState } from 'react';
import { Grid, Typography, FormControl, FormLabel, TextField, makeStyles, Button } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { SingleMobileUser, GetAllLanguage, GetAllInterest, UpdateMobileUser } from '../../redux/actions';
import Autocomplete from 'react-autocomplete';
import { Close } from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

const EditMobilerUser = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const param = useParams()
  const classes = useStyles()

  const initstate = () => {

    let init = {
      language: [],
      category: [],
      permissions: [],
      number: '',
      name: '',
      dob: '',
      gender: '',
      email: '',
      location: '',
      languages: '',
      categorys: ''
    };
    return init;
  }

  const [data, setData] = useState(initstate())
  const [intial, setInital] = useState(true)
  const [input, setInput] = useState(false)
  const [inp, setInp] = useState(false)

  const AllInterest = useSelector(state => state.auth.interestDetail);
  const AllLanguage = useSelector(state => state.auth.languageDetail)
  const MobileData = useSelector(state => state.auth.singleMobileUser);
  const Updated = useSelector(state => state.auth.updated);

  useEffect(() => {
    console.log("intial ", intial);
    if (intial) {
      dispatch(SingleMobileUser(param.id))
      setInital(false)
    }

    if (AllLanguage.length === 0) {
      dispatch(GetAllLanguage(0, 10));
    }
    if (AllInterest.length === 0) {
      dispatch(GetAllInterest(0, 10));
    }
    // console.log("Updated ", Updated);
    // console.log("MobileData", MobileData)
    // console.log('data', data)

    setData(MobileData)
    if (Updated) {
      history.push('/dashboard/users/mobileuser')
    }
    return () => { setData(initstate()) }
  }, [MobileData, Updated, AllLanguage, AllInterest])

  const onChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const onSelectLanguage = value => {
    console.log(value)
    if (
      value === "All Languages" ||
      data.language.includes("All Languages")
    ) {
      setData({
        ...data,
        language: ["All Languages"],
        languages: ''
      });
    } else {
      setData({
        ...data,
        language: data.language.concat(value),
        languages: ''
      });
    }
    setInput(false);
    console.log('Set Languag', data.languages, data.language)
  };

  const onRemoveLanguage = (e, ind) => {
    setData({
      ...data,
      language: data.language.filter((lan, index) => index !== ind)
    });
  };

  const onSelectInterest = value => {
    setData({
      ...data,
      category: data.category.concat(value),
      categorys: ''
    });
    setInp(false);
    console.log('Set Inve', data.category, data.categorys)
  };

  const onRemoveInventory = (e, ind) => {
    setData({
      ...data,
      category: data.category.filter((lan, index) => index !== ind)
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log('Mobile User Data', data)
    const value = {
      name: data.name,
      gender: data.gender,
      dob: data.dob,
      email: data.email,
      number: data.number,
      language: data.language,
      category: data.category,
      location: data.location
    }
    console.log('Mobile User Data', value)
    dispatch(UpdateMobileUser(param.id, value))
  }

  return (
    <React.Fragment>
      <div className={classes.paper}>
        <Typography variant="h5" component="h1">
          Edit Mobile User Detail
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Name</FormLabel>
                <TextField
                  name="name"
                  value={data.name}
                  onChange={onChange}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Date of Birth</FormLabel>
                <TextField
                  name="dob"
                  value={data.dob}
                  onChange={onChange}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
          </Grid><Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Email</FormLabel>
                <TextField
                  name="email"
                  value={data.email}
                  onChange={onChange}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Phone</FormLabel>
                <TextField
                  name="number"
                  value={data.number}
                  onChange={onChange}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
          </Grid><Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Gender</FormLabel>
                <TextField
                  name="gender"
                  value={data.gender}
                  onChange={onChange}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Location</FormLabel>
                <TextField
                  name="location"
                  value={data.location}
                  onChange={onChange}
                  variant="outlined"
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6} >
              <label htmlFor="Language" style={{ margin: '5px', padding: '5px', fontSize: '15px', marginBottom: '5px' }}>Language</label>
              <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <div><label >Select the language your customer speaks</label>
                  <ul className="languages-list">
                    {(data.language == null || data.language == undefined || data.language.length === 0)
                      ? ""
                      : data.language.map((item, index) => {
                        return AllLanguage.map((data) => {
                          if (item === data._id) {
                            return <li key={index}>
                              {data.name}
                              <IconButton onClick={e =>
                                onRemoveLanguage(e, index)
                              } >
                                <Close />
                              </IconButton>
                            </li>
                          }
                        })
                      })}
                  </ul></div>
                <div><Autocomplete
                  inputProps={{
                    placeholder: "Type your language",
                    name: "language",
                    id: "language",
                    className: "autocomplete",
                    onInput: () => {
                      setInput(true);
                    },
                    onBlur: () => setInput(false)
                  }}
                  items={AllLanguage}
                  menuStyle={{
                    boxShadow: "0 0 5px rgba(0,0,0,0.4)"
                  }}
                  renderItem={(item, isHighlighted) => (
                    <div key={item.name}
                      style={{
                        background: isHighlighted
                          ? "#E0E0E0"
                          : "white"
                      }}
                    >
                      {item.name}
                    </div>
                  )}
                  shouldItemRender={(item, value) =>
                    item.name
                      .toLowerCase()
                      .indexOf(value.toLowerCase()) > -1
                  }
                  getItemValue={item => item._id}
                  value={data.languages}
                  open={input}
                  onChange={e =>
                    setData({
                      ...data,
                      languages: e.target.value
                    })
                  }
                  onSelect={value => onSelectLanguage(value)}
                /></div>
              </div>

            </Grid>
            <Grid item xs={6}>
              <label htmlFor="Catergory" style={{ margin: '5px', padding: '5px', fontSize: '15px', marginBottom: '5px' }}>Category</label>
              <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <div><label >Select the Category</label>
                  <ul className="languages-list">
                    {(data.category == null || data.category == undefined || data.category.length === 0)
                      ? ""
                      : data.category.map((item, index) => {
                        return AllInterest.map((data) => {
                          if (item === data._id) {
                            return <li key={index}>
                              {data.name}
                              <IconButton onClick={e =>
                                onRemoveInventory(e, index)
                              } >
                                <Close />
                              </IconButton>
                            </li>
                          }
                        })
                      })}
                  </ul></div>
                <div><Autocomplete
                  inputProps={{
                    placeholder: "Type your category",
                    name: "category",
                    id: "category",
                    className: "autocomplete",
                    onInput: () => {
                      setInp(true);
                    },
                    onBlur: () => setInp(false)
                  }}
                  items={AllInterest}
                  menuStyle={{
                    boxShadow: "0 0 5px rgba(0,0,0,0.4)"
                  }}
                  renderItem={(item, isHighlighted) => (
                    <div key={item.name}
                      style={{
                        background: isHighlighted
                          ? "#E0E0E0"
                          : "white"
                      }}
                    >
                      {item.name}
                    </div>
                  )}
                  shouldItemRender={(item, value) =>
                    item.name
                      .toLowerCase()
                      .indexOf(value.toLowerCase()) > -1
                  }
                  getItemValue={item => item._id}
                  value={data.categorys}
                  open={inp}
                  onChange={e =>
                    setData({
                      ...data,
                      categorys: e.target.value
                    })
                  }
                  onSelect={value => onSelectInterest(value)}
                /></div>
              </div>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => { history.push('/dashboard/users/mobileuser') }}>Back</Button>

            </Grid>
            <Grid item xs={6}>

              <Button type="button" variant="contained" fullWidth color="primary" onClick={(e) => { handleSubmit(e) }}
              >Submit</Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </React.Fragment>
  )
}

export default EditMobilerUser
