import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TableContainer, TableHead, Table, TableBody, TableCell, TableRow, IconButton, TablePagination } from '@material-ui/core';
import { Edit, Delete } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import { ActivateMobileUser, allMobileCombination, createMobileCombination, GetAllMobileUser, multipleMobileCombination } from '../../redux/actions';
import { useHistory } from 'react-router';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
import LeafletMap from '../UsableComponent/LeafletMap';
import { LayersControl, Marker, Popup } from "react-leaflet";
import NewDelete from '../UsableComponent/NewDelete';
import { toast } from 'react-toastify';


// new views
import GridItem from '../../newviews/Grid/GridItem'
import GridContainer from "../../newviews/Grid/GridContainer.js";
import Card from "../../newviews/Card/Card";
import CardHeader from "../../newviews/Card/CardHeader.js";
import CardBody from "../../newviews/Card/CardBody.js";
import Button from '../../newviews/CustomButtons/Button'
import TblPagination from '../UsableComponent/TableSorting/TblPagination';
import useTable from '../UsableComponent/TableSorting/useTable';


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const HeaderArr = [
  { name: 'multipleComb' },
]

const headCells = [
  { id: '', label: '', disableSorting: true },
  { id: 'auth_id', label: 'Status', disableSorting: true },
  { id: 'btn', label: 'Status Button', disableSorting: true },
  { id: 'operate', label: 'Operations', disableSorting: true },
  { id: '_id', label: 'Id', disableSorting: true },
  { id: 'number', label: 'Mobile', disableSorting: true },
  { id: 'name', label: 'Name', disableSorting: true },
  { id: 'email', label: 'Email', disableSorting: true },
  { id: 'gender', label: 'Gender', disableSorting: true },
  { id: 'dob', label: 'Date of Birth', disableSorting: true },
  { id: 'createdAt', label: 'Date', disableSorting: true },
  { id: 'location', label: 'Location', disableSorting: true },
  { id: 'language', label: 'Language', disableSorting: true },
  { id: 'category', label: 'Category', disableSorting: true },
  { id: 'contact_permission', label: 'Contact Permission', disableSorting: true },
  { id: 'call_permission', label: 'Call Permission', disableSorting: true },
  { id: 'location_permisison', label: 'Location Permission', disableSorting: true },
  { id: 'combination_status', label: 'Combination Status', disableSorting: true },



]

const MobileUser = () => {

  const history = useHistory()
  const dispatch = useDispatch()
  const classes = useStyles();
  const [data, setData] = useState([]);

  const AllMobileData = useSelector(state => state.auth.mobileUserDetail);
  const totalCount = useSelector(state => state.auth.mobileTotal);
  const Updated = useSelector(state => state.auth.updated);
  const [flag, setFlag] = useState(false)
  const [map, setMap] = useState(false)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount != data.length) {
      dispatch(GetAllMobileUser(data.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }


  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)

  useEffect(() => {
    if (AllMobileData.length === 0) {
      dispatch(GetAllMobileUser(0, 10));
    } else {
      setData(AllMobileData);
      setFlag(true)
    }

    if (Updated) {

      //dispatch(GetAllMobileUser(0, 10));
      setId([])
      setnumSelected(0)
    }
    return () => { setData([]); setId([]) }
  }, [AllMobileData, Updated])

  const addMobileId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }



  const handleActivation = (e, data, user) => {
    if (user === true && data.combination_created == false) {
      return toast.error('First Create Combination then Activate')
    }
    dispatch(ActivateMobileUser(data._id, {
      user_status: user
    }))
  }

  const handleCombination = (data) => {
    dispatch(createMobileCombination(data))
  }

  const multipleCombination = () => {
    console.log('id', id)
    dispatch(multipleMobileCombination(id))
  }

  const allCombination = () => {
    dispatch(allMobileCombination())
  }

  const handleRefresh = () => {
    dispatch(GetAllMobileUser(0, 10));
  }

  return (
    <GridContainer>
      {map ? (data.length > 0) ? (
        <LeafletMap >{data.map((cc) => {
          return <LayersControl.Overlay key={cc._id} checked name={cc.name}>
            <Marker position={[cc.lattitude, cc.longitude]}>
              <Popup>
                {cc.name}
              </Popup>
            </Marker>
          </LayersControl.Overlay>
        })}</LeafletMap>
      ) : null : null}
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              <NewDelete name="MobileUser Details" length={numSelected} data={HeaderArr} multipleComb={multipleCombination} />
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
            {data.length > 0 ? <Button color="primary" round onClick={() => { setMap(!map) }} >{map ? 'Hide Map' : 'Show Map'}</Button> : null}
            {(numSelected == 0 && data.length > 0) ? <Button color="info" round style={{ position: 'relative', left: '85%', top: '-53px' }} onClick={() => { allCombination() }}>AllCombination</Button> : null}
          </CardHeader>
          <CardBody>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>Status Button</TableCell>
                    <TableCell>Operations</TableCell>
                    <TableCell>Id</TableCell>
                    <TableCell>Mobile</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Gender</TableCell>
                    <TableCell>Date of Birth</TableCell>
                    <TableCell>Date</TableCell>
                    <TableCell>Location</TableCell>
                    <TableCell>Language</TableCell>
                    <TableCell>Category</TableCell>
                    <TableCell>Fire ID</TableCell>
                    <TableCell>Contact Permission</TableCell>
                    <TableCell>Call Permission</TableCell>
                    <TableCell>Location Permission</TableCell>
                    <TableCell>Combination Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((user) => {
                      return <TableRow key={user._id}>
                        <TableCell>
                          <input
                            type="checkbox"
                            className="mobileList"
                            onClick={(e) => addMobileId(e)}
                            id={user._id}
                            name={user._id}
                            value={user._id}
                          />
                        </TableCell>
                        <TableCell>{(user.auth_id.user_status === true) ? "true" : "false"}</TableCell>
                        <TableCell>
                          {(user.auth_id.user_status === true) ? <Button variant="contained" color="danger" style={{ margin: '5' }} onClick={(e) => { handleActivation(e, user, false) }}>Deactivate</Button> :
                            <Button variant="contained" color="primary" style={{ margin: '5' }} onClick={(e) => { handleActivation(e, user, true) }}>Activate</Button>}
                        </TableCell>
                        <TableCell>
                          <span style={{ display: 'flex' }}>
                            <span title="Edit">
                              <IconButton onClick={(e) => { history.push(`/dashboard/users/mobileuser/${user._id}`) }}>
                                <Edit color="primary"></Edit>
                              </IconButton>
                            </span>
                            <span title="Delete">
                              <IconButton onClick={() => { alert("Delete Button Clicked") }}>
                                <Delete color="error"></Delete>
                              </IconButton>
                            </span>
                            <span title="Combination">
                              <IconButton onClick={() => { handleCombination(user._id) }}>
                                <Edit color="primary"></Edit>
                              </IconButton>
                            </span>
                          </span>
                        </TableCell>

                        <TableCell component="th" scope="row"> {user._id}</TableCell>
                        <TableCell >{user.number}</TableCell>
                        <TableCell >{user.name}</TableCell>
                        <TableCell >{user.email}</TableCell>
                        <TableCell >{user.gender}</TableCell>
                        <TableCell >{user.dob}</TableCell>
                        <TableCell >{user.date}</TableCell>
                        <TableCell >{user.location}</TableCell>
                        <TableCell >{user.language.map((lang) => {
                          return lang.name + ' , '
                        })}</TableCell>
                        <TableCell >{user.category.map((cat) => {
                          return cat.name + ' , '
                        })}</TableCell>
                        <TableCell >{user.fireid}</TableCell>
                        <TableCell >{(user.permissions[0].contact_permission === true) ? "true" : "false"}</TableCell>
                        <TableCell >{(user.permissions[0].call_permission === true) ? "true" : "false"}</TableCell>
                        <TableCell >{(user.permissions[0].location_permission === true) ? "true" : "false"}</TableCell>
                        <TableCell>{(user.combination_created == true) ? "true" : "false"}</TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default MobileUser
