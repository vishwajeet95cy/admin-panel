import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TableContainer, TableHead, Tooltip, Table, TableBody, TableCell, TableRow, IconButton, TablePagination } from '@material-ui/core';
import { Edit, Delete } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { ActivateWebUser, GetAllWebUser } from '../../redux/actions';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';
import useTable from '../UsableComponent/TableSorting/useTable';

// new views
import GridItem from '../../newviews/Grid/GridItem'
import GridContainer from "../../newviews/Grid/GridContainer.js";
import Card from "../../newviews/Card/Card";
import CardHeader from "../../newviews/Card/CardHeader.js";
import CardBody from "../../newviews/Card/CardBody.js";
import Button from '../../newviews/CustomButtons/Button'
import TblPagination from '../UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const WebUser = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const classes = useStyles();
  const [user, setUser] = useState([]);

  const AllWebData = useSelector(state => state.auth.webUserDetail);
  const totalCount = useSelector(state => state.auth.webTotal);
  const Updated = useSelector(state => state.auth.updated);
  const [flag, setFlag] = useState(false)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== user.length) {
      dispatch(GetAllWebUser(user.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }


  useEffect(() => {
    if (AllWebData.length === 0) {
      dispatch(GetAllWebUser(0, 10));
    } else {
      setUser(AllWebData);
      setFlag(true)
    }
    // if (Updated) {

    //   dispatch(GetAllWebUser(0, 10));
    // }
    return () => { setUser([]) }
  }, [AllWebData])


  const handleActivation = (e, data, user) => {
    const datas = {
      user_status: user
    }
    dispatch(ActivateWebUser(data._id, datas))
  }

  const handleRefresh = () => {
    dispatch(GetAllWebUser(0, 10));
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              All Web Users
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer >
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Status</TableCell>
                    <TableCell>Status Button</TableCell>
                    <TableCell>Operations</TableCell>
                    <TableCell>Id</TableCell>
                    <TableCell>Google Id</TableCell>
                    <TableCell>Facebook Id</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Photos</TableCell>
                    <TableCell>Website</TableCell>
                    <TableCell>Kind</TableCell>
                    <TableCell>Location</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (user.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : user.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((data, item) => {
                      return <TableRow key={data._id}>
                        <TableCell>{(data.auth_id.user_status === true) ? "true" : "false"}</TableCell>
                        <TableCell>
                          {(data.auth_id.user_status === true) ? <Button variant="contained" color="danger" round style={{ margin: '5' }} onClick={(e) => { handleActivation(e, data, false) }}>Deactivate</Button> : <Button variant="contained" color="primary" round style={{ margin: '5' }} onClick={(e) => { handleActivation(e, data, true) }}>Activate</Button>}
                        </TableCell>
                        <TableCell>
                          <span style={{ display: 'flex' }}>
                            <Tooltip title="Edit">
                              <IconButton onClick={(e) => { history.push(`/dashboard/users/webuser/${data._id}`) }}>
                                <Edit color="primary"></Edit>
                              </IconButton>
                            </Tooltip>
                            <Tooltip title="Delete">
                              <IconButton onClick={() => { alert("Delete Button Clicked") }}>
                                <Delete color="error"></Delete>
                              </IconButton>
                            </Tooltip>
                          </span>
                        </TableCell>

                        <TableCell component="th" scope="row">
                          {data._id}
                        </TableCell>
                        <TableCell>{data.google}</TableCell>
                        <TableCell>{data.facebook}</TableCell>
                        <TableCell >{data.profile.name}</TableCell>
                        <TableCell >{data.email}</TableCell>
                        <TableCell><img width="40px" height="40px" src={data.profile.photos} alt="Inkown" style={{ cursor: "pointer" }} /></TableCell>
                        <TableCell>{data.profile.website}</TableCell>
                        <TableCell >{data.tokens[0].kind}</TableCell>
                        <TableCell >{(typeof data.profile.location === 'object' && data.profile.location !== null
                        ) ? data.profile.location.name : data.profile.location}</TableCell>
                      </TableRow>
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default WebUser
