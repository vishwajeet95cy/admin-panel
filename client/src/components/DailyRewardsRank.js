import React, { useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { ActiveAllCampaign, AllDailyRanks, StartAllCampaign, UpdateAllAdsScore, UpdateAllAuthBudget, ChangeTargetStatus, } from '../redux/actions';
import { Checkbox, TableBody, TableCell, TableRow, TextField, DialogTitle, DialogContent, DialogActions, Dialog, Box, IconButton, Chip } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { toast } from 'react-toastify';
import { StatusInput } from './utils/TargetStatus';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
import NewDelete from './UsableComponent/NewDelete';
import useData from './UsableComponent/sortTable/customFunction';
import TableHeadComp from './UsableComponent/sortTable/TableHeadComp';
// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import useTable from './UsableComponent/TableSorting/useTable';
import Button from '../newviews/CustomButtons/Button'
import TblPagination from './UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

var AuthId = [];
var ScoreId = [];

const HeaderArr = [
  { name: 'start' },
  { name: 'stop' },
  { name: 'active' },
  { name: 'deactive' },
  { name: 'AdminBudget' },
  { name: 'AdsScore' },
]


const headCells = [
  { id: '', numeric: false, disablePadding: true, label: '', disableSorting: true },
  { id: 'rank', numeric: false, disablePadding: true, label: 'Rank', sort: true },
  { id: 'views', numeric: false, disablePadding: true, label: 'Views', sort: true },
  { id: 'admin_score', numeric: false, disablePadding: true, label: 'Admin Score', sort: true },
  { id: 'ads_score', numeric: false, disablePadding: false, label: 'Ads Score', sort: true },
  { id: 'target_name', numeric: false, disablePadding: false, label: 'Target Name', sort: true },
  { id: 'status', numeric: false, disablePadding: false, label: 'Status', sort: false },
  { id: 'campaign_name', numeric: false, disablePadding: false, label: 'Campaign Name', sort: true },
  { id: 'audience_id', numeric: false, disablePadding: false, label: 'Audience Id', sort: true },
  { id: 'ads_id', numeric: false, disablePadding: false, label: 'Ads Id', sort: true },
  { id: 'title', numeric: false, disablePadding: false, label: 'Video title', sort: false },
  { id: 'url', numeric: false, disablePadding: false, label: 'Display Url', sort: false },
  { id: 'short_id', numeric: false, disablePadding: false, label: 'Short Id', sort: false },
  { id: 'click', numeric: false, disablePadding: false, label: 'Click', sort: true },
];

var NewArray = [];
var ss = "calling";

const DailyRewardsRank = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [element, setElement] = useState([])
  const DailyDetails = useSelector(state => state.auth.allDailyRanks)
  const totalCount = useSelector(state => state.auth.dailyTotal)
  const Updated = useSelector(state => state.auth.updated)
  const [load, setLoad] = useState(false)
  const LoadingTrue = useSelector(state => state.auth.loader);
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  useEffect(() => {

    if (DailyDetails.length === 0) {
      dispatch(AllDailyRanks(0, 10));
    } else {
      setElement(DailyDetails)
      setLoad(true)
    }

    if (Updated) {
      AuthId = [];
      ScoreId = [];
      setCheckedCampaignNum(0);
      setOpen(false);
      setFlag(false);

      dispatch(AllDailyRanks(0, 10));
    }

    return () => { AuthId = []; ScoreId = []; }
  }, [DailyDetails, Updated])

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(element, headCells, filterFn)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== element.length) {
      dispatch(AllDailyRanks(element.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }



  const [checkedCampaign, setCheckedCampaign] = useState([]);
  const [checkedCampaignNum, setCheckedCampaignNum] = useState(0);

  const [data, setData] = useState({
    admin_budget: 0,
    ads_score: 0
  })

  const SetCheck = useCallback((e, cc) => {
    if (e.target.checked === true) {
      AuthId.push(e.target.value);
      ScoreId.push(cc.score_id);
      setData({ ...data, admin_budget: cc.admin_score, ads_score: cc.ads_score })
    } else {
      AuthId = AuthId.filter(function (item) {
        return item !== e.target.value
      });
      ScoreId = ScoreId.filter(function (item) {
        return item !== cc.score_id
      });
    }
    console.log('Auth Id', AuthId);
    console.log('Score Id', ScoreId);
    setCheckedCampaign(AuthId);
    setCheckedCampaignNum(AuthId.length);
    if (AuthId.length === 0) {
      setData({ ...data, admin_score: 0, ads_score: 0 })
    }

    console.log(checkedCampaignNum);
  }, [checkedCampaignNum, data])

  const [open, setOpen] = React.useState(false);
  const [flag, setFlag] = React.useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  };

  const handleAdminOpen = useCallback(() => {
    setOpen(true)
  }, [open])

  const handleAdminClose = useCallback(() => {
    setOpen(false);
  }, [open])

  const handleAdsOpen = useCallback(() => {
    setFlag(true)
  }, [flag])

  const handleAdsClose = useCallback(() => {
    setFlag(false);
  }, [flag])

  const handleAdsSubmit = useCallback(() => {
    const scoreData = {
      ads_score: data.ads_score,
      data: ScoreId
    }

    console.log('Ads Submit', scoreData)

    dispatch(UpdateAllAdsScore(scoreData))

  }, [data])

  const handleAdminSubmit = useCallback(() => {
    const adminData = {
      admin_budget: data.admin_budget,
      data: AuthId
    }

    console.log('Auth Submit', adminData)

    dispatch(UpdateAllAuthBudget(adminData))
  }, [data])

  const HandleStart = useCallback(() => {
    const startValue = {
      data: AuthId,
      start: true
    }

    dispatch(StartAllCampaign(startValue))

  }, [])

  const HandleStop = useCallback(() => {
    const startValue = {
      data: AuthId,
      start: false
    }

    dispatch(StartAllCampaign(startValue))

  }, [])

  const HandleActive = useCallback(() => {



    const activeValue = {
      data: AuthId,
      active: true
    }

    dispatch(ActiveAllCampaign(activeValue))
  }, [])

  const HandleDeactive = useCallback(() => {



    const activeValue = {
      data: AuthId,
      active: false
    }

    dispatch(ActiveAllCampaign(activeValue))
  }, [])

  const ChangeStatusTarget = useCallback((e, data) => {

    var value
    var statusData

    StatusInput.map((cc) => {
      if (cc.name == e.target.value && cc.name == "Approve") {
        value = true
        statusData = {
          name: cc.name,
          type: cc.type
        }
      } else if (cc.name == e.target.value && cc.name != "Approve") {
        value = false
        statusData = {
          name: cc.name,
          type: cc.type
        }
      }
    })

    if (value) {
      if (data.ads_score == 0) {

        return toast.error('First set Ads Score then it will change')
      }
    }
    console.log('Status Data', {
      status: statusData
    })
    dispatch(ChangeTargetStatus(data.target_id._id, {
      status: statusData
    }))
  }, [])

  const handleRefresh = () => {
    dispatch(AllDailyRanks(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              <NewDelete name="All Daily Rewards Ranks" length={checkedCampaignNum} data={HeaderArr} start={(LoadingTrue) ? null : HandleStart} stop={(LoadingTrue) ? null : HandleStop} active={(LoadingTrue) ? null : HandleActive} deactive={(LoadingTrue) ? null : HandleDeactive} AdminBudget={(LoadingTrue) ? null : handleAdminOpen} AdsScore={(LoadingTrue) ? null : handleAdsOpen} />
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              {load ? (element.length > 0 ? (<TableBody>
                {recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage)
                  .map((row, index) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={index}
                      >
                        <TableCell align="center" padding="checkbox">
                          <Checkbox
                            onClick={(e) => { SetCheck(e, row) }}
                            id={row.auth_id._id}
                            name={row.auth_id._id}
                            value={row.auth_id._id}
                            checked={AuthId.includes(row.auth_id._id) ? true : false}
                          />
                        </TableCell>
                        <TableCell align="center" component="th" scope="row">{row.rank}</TableCell>
                        <TableCell>{row.views}</TableCell>
                        <TableCell>{row.admin_score}</TableCell>
                        <TableCell>{row.ads_score}</TableCell>
                        <TableCell>{row.target_name}</TableCell>
                        <TableCell><select style={{ margin: '6px', padding: '6px', borderRadius: '11px' }} value={row.target_id ? row?.target_id?.tstatus.name : null} onChange={(e) => { ChangeStatusTarget(e, row) }}>
                          {StatusInput.map((item) => {
                            return (
                              <option key={item.id} value={item.name}>{item.name}</option>
                            )
                          })}
                        </select>
                        </TableCell>
                        <TableCell>{row.campaign_name}</TableCell>
                        <TableCell>{row.audience_id}</TableCell>
                        <TableCell>{row.ads_id}</TableCell>
                        <TableCell>{row.title}</TableCell>
                        <TableCell>
                          <Chip component="a" href={row.displayUrl} target="_blank" label="Click Me" />
                        </TableCell>
                        <TableCell>{row.short}</TableCell>
                        <TableCell>{row.count}</TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>) : (<TableBody>
                <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <NoData />
                  </TableCell>
                </TableRow>
              </TableBody>)) : (<TableBody>
                <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>
              </TableBody>)}
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog open={open} fullWidth onClose={handleAdminClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Add Admin Budget</Box>
          <Box>
            <IconButton onClick={handleAdminClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="admin_budget"
            label="Admin Budget"
            name="admin_budget"
            type="number"
            value={data.admin_budget}
            onChange={handleChange}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAdminClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleAdminSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={flag} fullWidth onClose={handleAdsClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Add Ads Score</Box>
          <Box>
            <IconButton onClick={handleAdsClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="ads_score"
            label="Ads Score"
            name="ads_score"
            type="number"
            value={data.ads_score}
            onChange={handleChange}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAdsClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleAdsSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  )
}

export default DailyRewardsRank


{/* <TableContainer component={Paper} className={classes.container}>
<Table className={classes.table} size="small" aria-label="simple table">
  <TableHead>
    <TableRow>
      <TableCell></TableCell>
      <TableCell>Rank</TableCell>
      <TableCell>Views</TableCell>
      <TableCell>Admin Score</TableCell>
      <TableCell>Ads Score</TableCell>
      <TableCell>Target Name</TableCell>
      <TableCell>Status</TableCell>
      <TableCell>Campaign Name</TableCell>
      <TableCell>Audience Id</TableCell>
      <TableCell>Ads Id</TableCell>
      <TableCell>Video Title</TableCell>
      <TableCell>Display Url</TableCell>
      <TableCell>Short Id</TableCell>
    </TableRow>
  </TableHead>
  <TableBody>
    {load ? (
      (element.length === 0) ? <TableRow>
        <TableCell align="center" colSpan={15}>
          <NoData />
        </TableCell>
      </TableRow> : element.map((ex) => {
        return <TableRow key={ex._id}>
          <TableCell>
            <input
              type="checkbox"
              className="chekboxCampaignList"
              onClick={(e) => setCheck(e, ex)}
              id={ex.auth_id._id}
              name={ex.auth_id._id}
              value={ex.auth_id._id}
            />
          </TableCell>
          <TableCell>{ex.rank}</TableCell>
          <TableCell>{ex.views}</TableCell>
          <TableCell>{ex.admin_score}</TableCell>
          <TableCell>{ex.ads_score}</TableCell>
          <TableCell>{ex.target_id.name}</TableCell>
          <TableCell>
            <select style={{ margin: '6px', padding: '6px', borderRadius: '11px' }} value={ex.target_id ? ex.target_id.tstatus.name : null} onChange={(e) => { ChangeStatusTarget(e, ex) }}>
              {StatusInput.map((item) => {
                return (
                  <option key={item.id} value={item.name}>{item.name}</option>
                )
              })}
            </select>
          </TableCell>
          <TableCell>{ex.auth_id.campaign_id.campaign_name}</TableCell>
          <TableCell>{ex.audience_id}</TableCell>
          <TableCell>{ex.target_id.ads_id._id}</TableCell>
          <TableCell>{ex.target_id.ads_id.video_id.title}</TableCell>
          <TableCell>{ex.target_id.ads_id.trackingTemplate}</TableCell>
          <TableCell>{(!ex.short_id || ex.short_id == null || ex.short_id == undefined) ? null : ex.short_id.short_id}</TableCell>
        </TableRow>
      })
    ) : <TableRow>
      <TableCell align="center" colSpan={15}>
        <Loader />
      </TableCell>
    </TableRow>}
  </TableBody>
</Table>
</TableContainer> */}

{/* <TableHeadComp alldata={NewArray} order={order} orderBy={orderBy} handleSelectAllClick={handleSelectAllClick} handleRequestSort={handleRequestSort} handleChangePage={handleChangePage} handleChangeRowsPerPage={handleChangeRowsPerPage} rowsPerPage={rowsPerPage} page={page} head={headCells} ss={ss}>
          {load ? (element.length > 0 ? (<TableBody>
            {value
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                return (
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={index}
                  >
                    {ss ? <TableCell align="center" padding="checkbox">
                      <Checkbox
                        onClick={(e) => { SetCheck(e, row) }}
                        id={row.auth_id}
                        name={row.auth_id}
                        value={row.auth_id}
                        checked={AuthId.includes(row.auth_id) ? true : false}
                      />
                    </TableCell> : <TableCell></TableCell>}
                    <TableCell align="center" component="th" scope="row">{row.rank}</TableCell>
                    <TableCell>{row.views}</TableCell>
                    <TableCell>{row.admin_score}</TableCell>
                    <TableCell>{row.ads_score}</TableCell>
                    <TableCell>{row.target_name}</TableCell>
                    <TableCell><select style={{ margin: '6px', padding: '6px', borderRadius: '11px' }} value={row.target_id ? row.status : null} onChange={(e) => { ChangeStatusTarget(e, row) }}>
                      {StatusInput.map((item) => {
                        return (
                          <option key={item.id} value={item.name}>{item.name}</option>
                        )
                      })}
                    </select>
                    </TableCell>
                    <TableCell>{row.campaign_name}</TableCell>
                    <TableCell>{row.audience_id}</TableCell>
                    <TableCell>{row.ads_id}</TableCell>
                    <TableCell>{row.title}</TableCell>
                    <TableCell>{row.url}</TableCell>
                    <TableCell>{(!row.short_id || row.short_id == null || row.short_id == undefined) ? null : row.short_id}</TableCell>
                    <TableCell>{(!row.short_id || row.short_id == null || row.short_id == undefined) ? null : row.click}</TableCell>
                  </TableRow>
                );
              })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>) : (<TableBody>
            <TableRow>
              <TableCell align="center" colSpan={15}>
                <NoData />
              </TableCell>
            </TableRow>
          </TableBody>)) : (<TableBody>
            <TableRow>
              <TableCell align="center" colSpan={15}>
                <Loader />
              </TableCell>
            </TableRow>
          </TableBody>)}
        </TableHeadComp> */}