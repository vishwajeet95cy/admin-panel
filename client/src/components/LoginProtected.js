import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const LoginProtected = ({ component: Component, ...rest }) => {
  const selector = useSelector(state => state.auth.isAuthenticated);
  return (
    <Route {...rest} render={(props) => {
      if (selector) {
        return <Redirect to={{
          pathname: "/dashboard",
          state: {
            from: props.location
          }
        }} />
      } else {
        return <Component {...props} />
      }
    }} />)
}

export default LoginProtected