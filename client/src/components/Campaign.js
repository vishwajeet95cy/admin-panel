import React, { useEffect, useState, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Dialog, DialogActions, DialogContent, DialogTitle, TextField, TableBody, TableCell, TableRow, Box, Switch, IconButton, Typography, Grid } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllCampaign, GetAllCampaignTypes, UpdateCampaignCredential, FilterCampaignByType, ChangeAuthStatus, AllCallingRanks, AllDailyRanks } from '../redux/actions';
import { toast } from 'react-toastify';
import { StatusInput } from './utils/TargetStatus';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
import NewDelete from './UsableComponent/NewDelete';
import CampaignBody from './CampaignBody';
// import TableHeadComp from './UsableComponent/sortTable/TableHeadComp';
// import useData from './UsableComponent/sortTable/customFunction';
import useTable from './UsableComponent/TableSorting/useTable';

// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card.js";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import Button from '../newviews/CustomButtons/Button'
import TblPagination from './UsableComponent/TableSorting/TblPagination';

// const useStyles = makeStyles({
//   table: {
//     minWidth: 400,
//   },
// });

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};

const useStyles = makeStyles(styles);

var AuthId = [];
var BudgetId = [];

const HeaderArr = [
  { name: 'form', lcheck: 1 },
  { name: 'editBudget', lcheck: 1 },
]

// var headCells = [
//   {
//     id: "campaign_name",
//     numeric: false,
//     disablePadding: false,
//     label: "Campaign Name",
//     sort: true,
//   },
//   {
//     id: "campaign_type",
//     numeric: false,
//     disablePadding: false,
//     label: "Campaign Type",
//     sort: true,
//   },
//   {
//     id: "campaign_status",
//     numeric: false,
//     disablePadding: false,
//     label: "Campaign Status",
//     sort: true,
//   },
//   {
//     id: "campaign_active",
//     numeric: false,
//     disablePadding: false,
//     label: "Campaign Active",
//     sort: true,
//   },
//   {
//     id: "campaign_budget",
//     numeric: false,
//     disablePadding: false,
//     label: "Campaign Budget",
//     sort: true,
//   },
//   {
//     id: "status",
//     numeric: false,
//     disablePadding: false,
//     label: "Status",
//     sort: true,
//   },
//   {
//     id: "budget",
//     numeric: false,
//     disablePadding: false,
//     label: "Budget",
//     sort: true,
//   },
//   {
//     id: "budget_type",
//     numeric: false,
//     disablePadding: false,
//     label: "Budget Type",
//     sort: true,
//   },
//   {
//     id: "schedule",
//     numeric: false,
//     disablePadding: false,
//     label: "AdSchedule",
//     sort: true,
//   },
//   {
//     id: "start_date",
//     numeric: false,
//     disablePadding: false,
//     label: "Start Date",
//     sort: true,
//   },
//   {
//     id: "end_date",
//     numeric: false,
//     disablePadding: false,
//     label: "End Date",
//     sort: true,
//   },
//   {
//     id: "date",
//     numeric: false,
//     disablePadding: false,
//     label: "Created Date",
//     sort: true,
//   },
// ]

var headCells = [
  {
    id: "",
    numeric: false,
    disablePadding: false,
    label: "",
    disableSorting: true,
  }, {
    id: "s",
    numeric: false,
    disablePadding: false,
    label: "",
    disableSorting: true,
  },
  {
    id: "campaign_name",
    numeric: false,
    disablePadding: false,
    label: "Campaign Name",
  },
  {
    id: "campaign_type",
    numeric: false,
    disablePadding: false,
    label: "Campaign Type",
  },
  {
    id: "campaign_status",
    numeric: false,
    disablePadding: false,
    label: "Campaign Status",
  },
  {
    id: "campaign_active",
    numeric: false,
    disablePadding: false,
    label: "Campaign Active",
  },
  {
    id: "campaign_budget",
    numeric: false,
    disablePadding: false,
    label: "Campaign Budget",
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "Status",
  },
  {
    id: "budget",
    numeric: false,
    disablePadding: false,
    label: "Budget",
  },
  {
    id: "budget_type",
    numeric: false,
    disablePadding: false,
    label: "Budget Type",
  },
  {
    id: "schedule",
    numeric: false,
    disablePadding: false,
    label: "AdSchedule",
  },
  {
    id: "start_date",
    numeric: false,
    disablePadding: false,
    label: "Start Date",
  },
  {
    id: "end_date",
    numeric: false,
    disablePadding: false,
    label: "End Date",
  },
  {
    id: "date",
    numeric: false,
    disablePadding: false,
    label: "Created Date",
  },
]

var NewArray = [];

const Campaign = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const history = useHistory()
  const [campaign, setCamapign] = useState([])
  const [campaign_type, setCampaignType] = useState([])
  const [open, setOpen] = useState(false);
  const [data, setData] = useState({
    start: false,
    active: false,
    admin_budget: '',
    id: '',
    campaign_type: '',
    campaign_name: '',
    campaign_type_id: '',
    ads_type_id: ''
  })
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  const handleClose = () => {
    setOpen(false);
  };
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  };

  const handleSwitch = (e) => {
    setData({ ...data, [e.target.name]: e.target.checked })
  }

  const handleOpen = () => {
    setOpen(true)
  }

  const AllCampaign = useSelector(state => state.auth.campaignDetail)
  const AllCampaignTypes = useSelector(state => state.auth.campaignTypeDetail);
  const Updated = useSelector(state => state.auth.updated)
  const [flag, setFlag] = useState(false)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    // if (newPage >= 1) {
    //   dispatch(getAllAds(ads.length, 10))
    // }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  // sort Tab

  // const [order, setOrder] = React.useState("asc");
  // const [orderBy, setOrderBy] = React.useState("");
  // const [selected, setSelected] = React.useState([]);
  // const [page, setPage] = React.useState(0);
  // const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleArrayData = useCallback((array) => {
    NewArray = [];
    array.map((datas) => {
      NewArray.push({
        _id: datas._id,
        campaign_name: datas.campaign_name,
        campaign_type: datas.campaign_type_id.campaign_name,
        campaign_status: datas.auth_id.start,
        campaign_active: datas.auth_id.active,
        campaign_budget: datas.auth_id.admin_budget,
        status: datas.auth_id.astatus.name,
        budget: parseInt(datas.budget_id.budget),
        budget_type: datas.budget_id.budgetType,
        schedule: datas.budget_id.adSchedule.name,
        start_date: datas.budget_id.startDate,
        end_date: datas.budget_id.endDate,
        auth_id: datas.auth_id,
        target_id: datas.target_id,
        budget_id: datas.budget_id,
        date: datas.createdAt
      });
    });
    return NewArray;
  }, []);

  // const [value, func, newFunction] = useData(
  //   handleArrayData(campaign),
  //   order,
  //   orderBy
  // );



  const [checkedCampaign, setCheckedCampaign] = useState([]);
  const [checkedCampaignNum, setCheckedCampaignNum] = useState(0);

  useEffect(() => {
    if (AllCampaign.length === 0) {
      dispatch(GetAllCampaign());
      // dispatch(AllCallingRanks())
      // dispatch(AllDailyRanks())
    } else {
      setCamapign(AllCampaign);
      setFlag(true)
    }

    if (AllCampaignTypes.length === 0) {
      dispatch(GetAllCampaignTypes(0, 10));
    }
    setCampaignType(AllCampaignTypes);

    if (Updated) {
      //dispatch(GetAllCampaign());
      setOpen(false)
      AuthId = [];
      BudgetId = [];
      setCheckedCampaignNum(0)
    }

    return () => { AuthId = []; BudgetId = []; }
  }, [AllCampaign, Updated])

  const handleSubmit = () => {
    dispatch(UpdateCampaignCredential(data));
  }


  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(handleArrayData(campaign), headCells, filterFn)

  // sorting
  // const campaignReqFunc = () => {
  //   newFunction(handleArrayData(campaign), order, orderBy);
  // };


  const DataType = () => {
    var type
    if (data.campaign_type === "") {
      type = null
    } else {
      type = data.campaign_type
    }
    dispatch(FilterCampaignByType(type))
  }

  const ChangeAuth = (e, data) => {
    var value
    var statusData

    StatusInput.map((cc) => {
      if (cc.name == e.target.value && cc.name == "Approve") {
        value = true
        statusData = {
          name: cc.name,
          type: cc.type
        }
      } else if (cc.name == e.target.value && cc.name != "Approve") {
        value = false
        statusData = {
          name: cc.name,
          type: cc.type
        }
      }
    })

    if (value) {
      if (data.admin_budget == 0) {
        return toast.error('First set Campaign Budget then it will change')
      }
    }

    const file = {
      status: statusData
    }
    console.log('Status Selected', file)
    dispatch(ChangeAuthStatus(data._id, file))
  }


  const setCheck = (e, cc) => {
    if (e.target.checked === true) {
      AuthId.push(e.target.value);
      BudgetId.push(cc.budget_id._id);
      setData({ start: cc.auth_id.start, active: cc.auth_id.active, admin_budget: cc.auth_id.admin_budget, id: cc._id })
    } else {
      AuthId = AuthId.filter(function (item) {
        return item !== e.target.value
      });
      BudgetId = BudgetId.filter(function (item) {
        return item !== cc.budget_id._id
      });
    }
    setCheckedCampaign(AuthId);
    setCheckedCampaignNum(AuthId.length);
  }

  const handlBudgetEdit = () => {
    history.push('/dashboard/editbudget', {
      from: history.location.pathname,
      id: BudgetId[0]
    })
  }

  const handleRefresh = () => {
    dispatch(GetAllCampaign());
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              <NewDelete name="All Campaign Details" length={checkedCampaignNum} data={HeaderArr} form={handleOpen} editBudget={handlBudgetEdit} />
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              {flag ? (
                campaign.length > 0 ? (
                  <TableBody>
                    {recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage)
                      .map((row, index) => {
                        return <CampaignBody key={row._id} datas={row} setCheck={setCheck} ChangeAuth={ChangeAuth} AuthId={AuthId} />;
                      })}
                  </TableBody>
                ) : (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
              ) : (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={campaign.length}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
      <Dialog open={open} fullWidth onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Edit Campaign Form</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="admin_budget"
            label="Budget"
            name="admin_budget"
            type="text"
            value={data.admin_budget}
            onChange={handleChange}
            fullWidth
          />
          <Typography component="div">
            <Grid component="label" container alignItems="center" spacing={1}>
              <Grid item>Deactive</Grid>
              <Grid item>
                <Switch checked={data.active} onChange={handleSwitch} name="active" />
              </Grid>
              <Grid item>Active</Grid>
            </Grid>
          </Typography>
          <Typography component="div">
            <Grid component="label" container alignItems="center" spacing={1}>
              <Grid item>Stop</Grid>
              <Grid item>
                <Switch checked={data.start} onChange={handleSwitch} name="start" />
              </Grid>
              <Grid item>Start</Grid>
            </Grid>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </GridContainer>
  )
}

export default Campaign


{/* <Container>
      <h1>Campaign Details</h1>
      <div style={{ display: 'flex', margin: '5px' }}>
        <label>Select Campaign Type</label>
        <select name="campaign_type" value={data.campaign_type} onChange={handleChange}>
          <option value="">Select Campaign Type</option>
          {(campaign_type.length === 0) ? null : campaign_type.map((camp) => {
            return (<React.Fragment key={camp._id}>
              <option value={camp._id}>{camp.campaign_name}</option>
            </React.Fragment>)
          })}
        </select>
        <Button type="button" variant="contained" color="primary" onClick={DataType} >Search</Button>
      </div>
      <Card>
        <CardHeader>
          <NewDelete name="All Campaign Details" length={checkedCampaignNum} data={HeaderArr} form={handleOpen} editBudget={handlBudgetEdit} />
        </CardHeader>
        <CardContent>
          <TblContainer>
            <TblHead />
            {flag ? (
              campaign.length > 0 ? (
                <TableBody>
                  {recordsAfterPagingAndSorting()
                    .map((row, index) => {
                      return <CampaignBody key={row._id} datas={row} setCheck={setCheck} ChangeAuth={ChangeAuth} AuthId={AuthId} />;
                    })}
                </TableBody>
              ) : (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <NoData />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )
            ) : (
              <TableBody>
                <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>
              </TableBody>
            )}
          </TblContainer>
          <TblPagination />
        </CardContent>
      </Card>
      <Dialog open={open} fullWidth onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Edit Campaign Form</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="admin_budget"
            label="Budget"
            name="admin_budget"
            type="text"
            value={data.admin_budget}
            onChange={handleChange}
            fullWidth
          />
          <Typography component="div">
            <Grid component="label" container alignItems="center" spacing={1}>
              <Grid item>Deactive</Grid>
              <Grid item>
                <Switch checked={data.active} onChange={handleSwitch} name="active" />
              </Grid>
              <Grid item>Active</Grid>
            </Grid>
          </Typography>
          <Typography component="div">
            <Grid component="label" container alignItems="center" spacing={1}>
              <Grid item>Stop</Grid>
              <Grid item>
                <Switch checked={data.start} onChange={handleSwitch} name="start" />
              </Grid>
              <Grid item>Start</Grid>
            </Grid>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </Container> */}