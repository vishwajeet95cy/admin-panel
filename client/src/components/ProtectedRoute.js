import React from 'react';
import { Redirect, Route, useLocation } from 'react-router-dom';
import routes from './Routes/Routes';
import Users from './Users';
import { useSelector } from 'react-redux';
import { AddData } from './utils/BackFunction';
import { useEffect } from 'react';
import { useContext } from 'react';
import { BackContext } from './utils/LocationContext';

var arrayNav = []

function usePageViews(newVal, val) {
  let location = useLocation();
  React.useEffect(() => {
    console.log('Protescted Routes Location', location.pathname)
    if (Array[Array.length - 1] != location.pathname) {
      Array.push(location.pathname)
      newVal(Array)
      console.log('Protected Route Array', Array)
      console.log('Protected Route App Array', val)
    }
  }, [location]);
}

const ProtectedRoute = ({ component: Component, ...rest }) => {

  const context = useContext(BackContext)
  let location = useLocation();
  const selector = useSelector(state => state.auth.isAuthenticated);
  // usePageViews(context.handleValue, context.value)

  //AddData(setValue, value, Array)

  // useEffect(() => {
  //   console.log('Private Route use Effect', window.location.pathname)
  //   arrayNav=context.value;
  //   console.log('Private arrayNav use Effect',arrayNav)
  //   context.handleValue([location.pathname])
  // }, [])


  useEffect(() => {
    if (arrayNav[arrayNav.length - 1] != location.pathname) {
      arrayNav.push(location.pathname)
      context.handleValue(arrayNav)
      console.log('Protected Route arrayNav', arrayNav)
      console.log('Protected Route App Array', context.value)
    }

  }, [location])

  return (
    <Route {...rest}
      render={
        (props) => {
          if (selector) {
            return <Component {...props} ></Component>
          }
          else {
            return <Redirect to={
              {
                pathname: "/",
                state: {
                  from: props.location
                }
              }
            } />
          }
        }

      } />)
}

export default ProtectedRoute;