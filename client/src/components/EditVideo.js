import React, { useState, useEffect } from 'react';
import { Grid, makeStyles, TextField, FormControl, FormLabel, Select, MenuItem, Typography, Button } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { UpdateVideo } from '../redux/actions';
import Loader from './UsableComponent/Loader';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

var FormId

const EditVideo = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const history = useHistory();
  const [video, setVideo] = useState('')
  const [img, setImage] = useState('')
  const [videoFiles, setVideofiles] = useState()
  const [imageFiles, setImagefiles] = useState()
  const [data, setData] = useState({
    title: '',
    video_status: '',
    thumbnail: '',
    video: ''
  })

  const [flag, setFlag] = useState(false)
  const AllData = useSelector(state => state.auth.videoDetail)
  const Updated = useSelector(state => state.auth.updated);
  const LoadingTrue = useSelector(state => state.auth.loader);

  const [btnD, setBtnD] = useState(true)

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
    setBtnD(false)
  };


  useEffect(() => {
    if (!history.location.state || history.location.state == null || history.location.state == undefined || AllData.length == 0) {
      history.push('/dashboard/videos')
    } else {
      FormId = history.location.state.id
      AllData.map((cc) => {
        if (cc._id == FormId) {
          setData(cc)
          setFlag(true)
        }
      })
    }

    if (Updated) {
      history.push(history.location.state.from)
    }

  }, [Updated])

  const onThumb = e => {
    let allowedExtensions = /\.(jpg|jpeg|png)$/;
    const data = e.target.files[0]
    console.log(data)
    if (e.target.files[0].size > 30720000) {
      alert("File size too big! Only files less than 30 MB are allowed");
    } else if (!data.name.match(allowedExtensions)) {
      alert("Only jpeg and png files are allowed!");
    } else {
      console.log(e.target.files[0]);
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setImage(fileURL)
      setImagefiles(e.target.files[0])
      setBtnD(false)
    }
  };

  const onUploadFile = e => {
    let allowedExtensions = /\.(mp4|mkv)$/;
    const data = e.target.files[0]
    console.log(data)
    if (e.target.files[0].size > 30720000) {
      alert("File size too big! Only files less than 30 MB are allowed");
    } else if (!data.name.match(allowedExtensions)) {
      alert("Only mp4 and mkv files are allowed!");
    } else {
      console.log(e.target.files[0]);
      let fileURL = URL.createObjectURL(e.target.files[0]);
      setVideo(fileURL);
      setVideofiles(e.target.files[0]);
      setBtnD(false)
    }
  };

  const handleSubmit = () => {
    var viddata = new FormData();
    viddata.append('title', data.title)
    viddata.append('video_status', data.video_status)
    viddata.append('thumbnail', data.thumbnail)
    viddata.append('video', data.video)
    viddata.append('thumb', videoFiles)
    viddata.append('thumb', imageFiles)

    dispatch(UpdateVideo(FormId, viddata));
  }

  return (
    <React.Fragment>
      <div className={classes.paper}>
        <Typography variant="h5" component="h1">
          Edit Video Detail
        </Typography>
        {flag ? <form className={classes.form} noValidate>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Title</FormLabel>
                <TextField
                  variant="outlined"
                  id="title"
                  value={data.title}
                  name="title"
                  fullWidth
                  onChange={handleChange}
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth
                variant="outlined">
                <FormLabel>Video Status</FormLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={(data.video_status == undefined) ? '' : data.video_status}
                  onChange={handleChange}
                  name="video_status"
                >
                  <MenuItem value=''></MenuItem>
                  <MenuItem value="Private">Private</MenuItem>
                  <MenuItem value="Unlisted">Unlisted</MenuItem>
                  <MenuItem value="Public">Public</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Image</FormLabel>
                <TextField
                  variant="outlined"
                  id="image"
                  name="photo"
                  type="file"
                  onChange={onThumb}
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Video</FormLabel>
                <TextField
                  variant="outlined"
                  id="video"
                  name="video"
                  type="file"
                  onChange={onUploadFile}
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6} >
              <div style={{ display: 'flex', margin: '5px', justifyContent: 'space-between' }}>
                <div >
                  <div>Old Image</div>
                  <img src={data.thumbnail} width="250px" height="170px" alt="Unknown" />
                </div>
                <div>
                  {(data.video === '') ? "" : <React.Fragment><div>Old Video</div>
                    <video width="250px" height="170px" controls>
                      <source src={data.video} type="video/mp4" />
                      Your browser does not support the <code>video</code> element.
                    </video></React.Fragment>}
                </div>
              </div>
            </Grid>
            <Grid item xs={6}>
              <div style={{ display: 'flex', margin: '5px', justifyContent: 'space-between' }}>
                {(img === undefined || img === null || img === "") ? '' : <div>
                  <div>New Image</div>
                  <img src={img} width="250px" height="170px" alt="Unknown" />
                </div>}
                {(video === undefined || video === null || video === "") ? '' : <div>
                  <div>New Video</div>
                  <video width="250px" height="170px" controls>
                    <source src={video} type="video/mp4" />
                    Your browser does not support the <code>video</code> element.
                  </video>
                </div>}
              </div>
            </Grid>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => { history.push('/dashboard/videos') }}>Back</Button>

              </Grid>
              <Grid item xs={6}>

                <Button type="button" variant="contained" disabled={(btnD || LoadingTrue) ? true : false} fullWidth color="primary" onClick={() => { handleSubmit() }}
                >Submit</Button>
              </Grid>
            </Grid>
          </Grid>
        </form> : <Loader />}
      </div>
    </React.Fragment>
  )
}

export default EditVideo
