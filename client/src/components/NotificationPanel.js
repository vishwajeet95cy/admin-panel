import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Chip, Table, TableBody, TableCell, TableRow } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import NewDelete from './UsableComponent/NewDelete';
import Loader from './UsableComponent/Loader';
import NoData from './UsableComponent/NoData';
import { Delete_Notification, GetAllNotifications } from '../redux/actions';
// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import useTable from './UsableComponent/TableSorting/useTable';
import TooltipComp from './UsableComponent/TooltipComp';
import ButtonComp from './UsableComponent/ButtonComp';
import Button from '../newviews/CustomButtons/Button'
import TblPagination from './UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const headCells = [
  { id: '', numeric: false, disablePadding: true, label: '', disableSorting: true },
  { id: 'campaign_id', numeric: false, disablePadding: true, label: 'Campaign Id', sort: true },
  { id: 'target_id', numeric: false, disablePadding: true, label: 'Target Id', sort: true },
  { id: 'user_id', numeric: false, disablePadding: true, label: 'User Id', sort: true },
  { id: 'message', numeric: false, disablePadding: true, label: 'Message', sort: true },
  { id: 'status', numeric: false, disablePadding: true, label: 'Status', sort: true },
]

const NotificationPanel = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [data, setData] = useState([])
  const [id, setId] = useState([])
  const [numSelected, setnumSelected] = useState(0)
  const [flag, setFlag] = useState(false)

  const AllNotify = useSelector(state => state.auth.notificationDetail)
  const totalCount = useSelector(state => state.auth.notificationTotal)
  const Updates = useSelector(state => state.auth.updated)
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== data.length) {
      dispatch(GetAllNotifications(data.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }


  useEffect(() => {
    if (AllNotify.length === 0) {
      dispatch(GetAllNotifications(0, 10))
    } else {
      setData(AllNotify)
      setFlag(true)
    }

    if (Updates) {
      dispatch(GetAllNotifications(0, 10))
      setId([]);
      setnumSelected(0)
    }

    return () => { setId([]); }
  }, [AllNotify, Updates])

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(data, headCells, filterFn)

  const addNotificationId = (e) => {
    var AllId = [...id]
    if (e.target.checked === true) {
      AllId.push(e.target.value);
      setId(AllId)
    } else {
      AllId = AllId.filter(function (item) {
        return item !== e.target.value;
      });
      setId(AllId)
    }
    setnumSelected(AllId.length)
  }

  const handleDeleteButton = () => {
    dispatch(Delete_Notification({ id: id }))
  }

  const handleRefresh = () => {
    dispatch(GetAllNotifications(0, 10))
  }


  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              <NewDelete name="All Notifications" length={numSelected} click={handleDeleteButton} />
            </h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} > Refresh</Button>
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              <TableBody>
                {flag ? (
                  (data.length === 0) ? <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <NoData />
                    </TableCell>
                  </TableRow> : recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((item) => {
                    return <TableRow key={item._id}>
                      <TableCell>
                        <input
                          type="checkbox"
                          className="notifyList"
                          checked={id.includes(item._id)}
                          onChange={(e) => addNotificationId(e)}
                          id={item._id}
                          name={item._id}
                          value={item._id}
                        />
                      </TableCell>
                      <TableCell>{item.campaign_id}</TableCell>
                      <TableCell>{item.target_id.map((cc) => {
                        return <Chip label={cc} />
                      })}</TableCell>
                      <TableCell>{item.user_id}</TableCell>
                      <TableCell>
                        <TooltipComp value={item.message}>
                          <Chip label="message" />
                        </TooltipComp>
                      </TableCell>
                      <TableCell>
                        <ButtonComp value={item.status} />
                      </TableCell>
                    </TableRow>
                  })
                ) : <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>}
              </TableBody>
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default NotificationPanel
