import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom';
import { Button, Container, CssBaseline, TextField, Typography, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { SingleAds, UpdateAds } from '../redux/actions';
import Loader from './UsableComponent/Loader';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

var FormId

const EditAds = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const params = useParams()
  const [data, setData] = useState({
    trackingTemplate: '',
    callToAction: '',
    finalURLSuffix: '',
    heading: '',
    description: '',
    ads_type_id: ''
  })
  // const [data, setData] = useState({})

  const classes = useStyles()
  const [flag, setFlag] = useState(false)
  const [btnD, setBtnD] = useState(true)
  const AllAds = useSelector(state => state.auth.adsDetail)
  const Updated = useSelector(state => state.auth.updated);
  const LoadingTrue = useSelector(state => state.auth.loader);

  useEffect(() => {

    if (!history.location.state || history.location.state == null || history.location.state == undefined || AllAds.length == 0) {
      history.push('/dashboard/ads')
    } else {
      FormId = history.location.state.id
      AllAds.map((cc) => {
        if (cc._id == FormId) {
          setData(cc)
          setFlag(true)
        }
      })
    }

    if (Updated) {
      history.push(history.location.state.from)
    }
    return () => {
      console.log("exit edit ads");
      setData({})
    }
  }, [Updated])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setBtnD(false)
  }

  const handleSubmit = () => {
    const value = {
      heading: data.heading, trackingTemplate: data.trackingTemplate, finalURLSuffix: data.finalURLSuffix, callToAction: data.callToAction, description: data.description, ads: data.ads_type_id.ads_type
    }

    dispatch(UpdateAds(FormId, value));
  }

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Edit Ads Detail
          </Typography>
          {flag ? <form className={classes.form} noValidate>
            {(data.ads_type_id.ads_type === 'Calling Video Ads') ? <React.Fragment>
              <TextField label="Heading" disabled value={data.heading} name="heading" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="Description" value={data.description} name="description" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="TrackinTemplate" value={data.trackingTemplate} name="trackingTemplate" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="Call To Action" disabled value={data.callToAction} name="callToAction" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="Final URL Suffix" disabled value={data.finalURLSuffix} name="finalURLSuffix" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => { history.push('/dashboard/ads') }}>Back</Button>
              <Button type="button" variant="contained" disabled={(btnD || LoadingTrue) ? true : false} fullWidth color="primary"
                className={classes.submit} onClick={() => { handleSubmit() }} >Submit</Button>
            </React.Fragment> : <React.Fragment>
              <TextField label="Heading" value={data.heading} name="heading" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="Description" value={data.description} name="description" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="TrackinTemplate" value={data.trackingTemplate} name="trackingTemplate" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="Call To Action" value={data.callToAction} name="callToAction" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <TextField label="Final URL Suffix" value={data.finalURLSuffix} name="finalURLSuffix" variant="outlined"
                margin="normal"
                required
                fullWidth onChange={handleChange} />
              <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => { history.push('/dashboard/ads') }}>Back</Button>
              <Button type="button" variant="contained" fullWidth color="primary" disabled={(btnD || LoadingTrue) ? true : false}
                className={classes.submit} onClick={() => { handleSubmit() }} >Submit</Button>
            </React.Fragment>}
          </form> : <Loader />}

        </div>
      </Container>
    </React.Fragment>
  )
}

export default EditAds
