import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllStats } from '../redux/actions';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';
// new views
import GridItem from '../newviews/Grid/GridItem'
import GridContainer from "../newviews/Grid/GridContainer.js";
import Card from "../newviews/Card/Card";
import CardHeader from "../newviews/Card/CardHeader.js";
import CardBody from "../newviews/Card/CardBody.js";
import Button from '../newviews/CustomButtons/Button'

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const Stats = () => {

  const dispatch = useDispatch()
  const classes = useStyles();
  const [data, setStat] = useState([])

  const AllStats = useSelector(state => state.auth.statDetail)
  const [flag, setFlag] = useState(false)

  useEffect(() => {

    if (AllStats.length === 0) {
      dispatch(GetAllStats());
    } else {
      setStat(AllStats);
      setFlag(true)
    }
    return () => { setStat([]); }
  }, [AllStats])

  const handleRefresh = () => {
    dispatch(GetAllStats());
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>All Stats Details</h4>
            <Button color="primary" round onClick={() => { handleRefresh() }} >Refresh</Button>
          </CardHeader>
          <CardBody>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Ip</TableCell>
                    <TableCell >Method</TableCell>
                    <TableCell>Host</TableCell>
                    <TableCell>Ip Detail</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (data.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : data.map((datas) => {
                      return (
                        <TableRow key={datas._id}>
                          <TableCell>{datas.ip}</TableCell>
                          <TableCell>{datas.method}</TableCell>
                          <TableCell>{datas.header.host}</TableCell>
                          <TableCell>{(!datas.ipDetail || datas.ipDetail == null || datas.ipDetail == undefined) ? null : datas.ipDetail.city + ' ' + datas.ipDetail.region + ' ' + datas.ipDetail.country + ' ' + datas.ipDetail.postal}</TableCell>
                        </TableRow>
                      )
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default Stats
