import React, { useEffect, useState } from 'react';
import { Grid, Paper, Typography } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

const Card = () => {

  const dispatch = useDispatch()
  const [data, setData] = useState({
    ads_count: 0,
    view_time_count: 0,
    req_view: {},
    users: {},
    campaign: {},
    orders: {},
  })

  const CountDatas = useSelector(state => state.auth.dashboardCount)

  useEffect(() => {
    setData({
      ...data, ads_count: CountDatas.ads_count,
      view_time_count: CountDatas.total_chunk,
      req_view: CountDatas.req_view,
      users: CountDatas.users,
      campaign: CountDatas.campaign,
      orders: CountDatas.orders
    })
    return () => { setData({ campaign_count: '', ads_count: '' }) }
  }, [CountDatas])

  return (
    <Grid container spacing={3}>
      <Grid item xs={4} sm={4} md={4}>
        <Paper>
          <Typography align="center" gutterBottom variant="subtitle1">
            Campaigns
          </Typography>
          <Typography align="center" gutterBottom variant="subtitle1">
            <div>Total Campaign: {data?.campaign?.total}</div>
            <div> Total Active Campaign : {data?.campaign?.active}</div>
            <div> Total Pending Campaign: {data?.campaign?.pending}</div>
            <div>Total Expired Campaign: {data?.campaign?.expired}</div>
            <div> CPC: {data?.campaign?.CPC}</div>
            <div>CPV: {data?.campaign?.CPV}</div>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <div>
                <div>Total Calling Campaign: {data?.campaign?.calling}</div>
                <div> Total Active Calling: {data?.campaign?.ActiveCalling}</div>
                <div> Total Not Active Calling: {data?.campaign?.NotActiveCalling}</div>
              </div>
              <div>
                <div>Total Daily Campaign : {data?.campaign?.daily}</div>
                <div>Total Active Daily: {data?.campaign?.ActiveDaily}</div>
                <div>Total Not Active Daily: {data?.campaign?.NotActivedaily}</div>
              </div>
            </div>
          </Typography>
        </Paper>
      </Grid>
      <Grid item xs={4} sm={4} md={4}>
        <Paper>
          <Typography align="center" gutterBottom variant="subtitle1">
            Total Ads
          </Typography>
          <Typography align="center" gutterBottom variant="subtitle1">
            {data.ads_count}
          </Typography>
        </Paper>
      </Grid>
      <Grid item xs={4} sm={4} md={4}>
        <Paper>
          <Typography align="center" gutterBottom variant="subtitle1">
            Total Views Chunks
          </Typography>
          <Typography align="center" gutterBottom variant="subtitle1">
            {data.view_time_count}
          </Typography>
        </Paper>
      </Grid>
      <Grid item xs={4} sm={4} md={4}>
        <Paper>
          <Typography align="center" gutterBottom variant="subtitle1">
            Orders
          </Typography>
          <Typography align="center" gutterBottom variant="subtitle1">
            <div>Total Orders : {data?.orders?.count}</div>
            <div>Total Order Amount : {data?.orders?.amount}</div>
            <div> Total Consumed: {data?.orders?.consumed}</div>
          </Typography>
        </Paper>
      </Grid>
      <Grid item xs={4} sm={4} md={4}>
        <Paper>
          <Typography align="center" gutterBottom variant="subtitle1">
            Request
          </Typography>
          <Typography align="center" gutterBottom variant="subtitle1">
            <div>Total Request : {data?.req_view?.calling_req + data?.req_view?.daily_req}</div>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <div>
                <div>Total Calling Request : {data?.req_view?.calling_req}</div>
                <div>Total Calling Completed: {data?.req_view?.calling_completed}</div>
                <div> Total Calling Chunk: {data?.req_view?.calling_chunk}</div>
                <div>Total Calling Click: {data?.req_view?.calling_click}</div>
              </div>
              <div>
                <div> Total Daily Request : {data?.req_view?.daily_req}</div>
                <div>Total Daily Completed : {data?.req_view?.daily_completed}</div>
                <div>Total Daily Chunk: {data?.req_view?.daily_chunk}</div>
                <div>Total Daily Click: {data?.req_view?.daily_click}</div>
              </div>
            </div>
          </Typography>
        </Paper>
      </Grid>
      <Grid item xs={4} sm={4} md={4}>
        <Paper>
          <Typography align="center" gutterBottom variant="subtitle1">
            Users
          </Typography>
          <Typography align="center" gutterBottom variant="subtitle1">
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <div>
                <div>Total Mobile User : {data?.users?.totalMobile}</div>
                <div>Total Active Mobile: {data?.users?.activeMobile}</div>
                <div>Total Not Active Mobile : {data?.users?.notactiveMobile}</div>
              </div>
              <div>

                <div> Total Web User : {data?.users?.totalWebUser}</div>
                <div> Total Active Web User: {data?.users?.activeWebUser}</div>
                <div>Total Not Active Web User: {data?.users?.notactiveWebUser}</div>
              </div>
            </div>
          </Typography>
        </Paper>
      </Grid>
    </Grid>
  )
}

export default Card
