import { Container, Grid, Paper } from '@material-ui/core';
import React, { useContext, useEffect } from 'react';
import Card from './Card';
import Chart from './Chart';
import Deposits from './Deposits';
import Orders from './Orders';
import SocketContext from '../utils/SocketConnection';
import { AdminCount, ConvertVideoUpdate, Socket_Connection, UpdateCallingRank, UpdateDailyRank } from '../../redux/actions/index'
import { useDispatch } from 'react-redux';

function Home() {

  // const dispatch = useDispatch()
  // const context = useContext(SocketContext)


  // useEffect(() => {

  //   context.socket.on('AdminCount', data => {
  //     dispatch(AdminCount(data))
  //   })

  //   context.socket.on('CallingRank', data => {
  //     dispatch(UpdateCallingRank(data))
  //   })

  //   context.socket.on('DailyRank', data => {
  //     dispatch(UpdateDailyRank(data))
  //   })

  //   context.socket.on('videoDataUpdate', data => {
  //     console.log('Request DOne', data)
  //     //dispatch(ConvertVideoUpdate(data))
  //   })

  //   context.socket.on('disconnect', function () {
  //     console.log('Disconnect from server')
  //     dispatch(Socket_Connection(false))
  //   });

  //   return () => {
  //     console.log('ondistroy')
  //     context.socket.off('connect');
  //     //context.socket.off('AdminCount')
  //     //  context.socket.off();
  //   }
  // }, [])

  return (
    <Container maxWidth="lg">
      <Card />
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={9}>
          <Paper>
            <Chart />
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Paper >
            <Deposits />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper >
            <Orders />
          </Paper>
        </Grid>
      </Grid>
    </Container>
  )
}

export default Home
