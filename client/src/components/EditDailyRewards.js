import React, { useEffect, useState } from 'react';
import { Typography, MenuItem, Select, TextField, InputAdornment, makeStyles, FormControl, Button, Grid, InputLabel } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { updateReward } from '../redux/actions';
import Loader from './UsableComponent/Loader';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

var FormId

const EditDailyRewards = () => {

  const initstate = () => {

    let init = {
      rewards_name: "",
      total_ads: "",
      boost_value: "",
      validity: {
        value: "",
        unit: "",
      },
      total_ads_coin: {
        value: "",
        unit: "",
      },
      ads_type: "",
      createdAt: "",
      updatedAt: "",
    };
    // console.log("init", init);
    return init;
  }
  const dispatch = useDispatch()
  const Updated = useSelector(state => state.auth.updated)

  const AllRewards = useSelector(state => state.auth.rewardDetail);

  const LoadingTrue = useSelector(state => state.auth.loader);

  const history = useHistory()
  const classes = useStyles()
  const [data, setData] = useState(initstate())

  const onEnterData = (e) => {
    if ([e.target.name] == "timevalue") {
      setData({ ...data, 'validity': { ...data.validity, value: e.target.value } })
    } else if (e.target.name == "validityvalue") {
      setData({ ...data, 'validity': { ...data.validity, unit: e.target.value } })
    } else if (e.target.name == "coinvalue") {
      setData({ ...data, 'total_ads_coin': { ...data.total_ads_coin, value: e.target.value } })
    } else if (e.target.name == "cointype") {
      setData({ ...data, 'total_ads_coin': { ...data.total_ads_coin, unit: e.target.value } })
    } else {
      setData({ ...data, [e.target.name]: e.target.value })
    }
    console.log('Onchange', [e.target.name], e.target.value)
    setBtnD(false)

  }

  const [flag, setFlag] = useState(false)
  const [btnD, setBtnD] = useState(true)

  useEffect(() => {

    console.log('history.location.state', history.location.state)
    if (!history.location.state || history.location.state == null || history.location.state == undefined || AllRewards.length == 0) {
      history.push('/dashboard/dailyrewards')
    } else {
      FormId = history.location.state.id
      AllRewards.map((cc) => {
        if (cc._id == FormId) {
          setData(cc)
          setFlag(true)
        }
      })
    }

    if (Updated) {
      history.push(history.location.state.from)
    }
    return () => { setData(initstate()) }
  }, [Updated])

  const handleSubmit = () => {
    const datas = {
      rewards_name: data.rewards_name,
      total_ads: data.total_ads,
      boost_value: data.boost_value,
      coinvalue: data.total_ads_coin.value,
      coinunit: data.total_ads_coin.unit,
      timevalue: data.validity.value,
      timeunit: data.validity.unit,
      ads_type: data.ads_type
    }
    console.log('Daily Rewards', datas)
    dispatch(updateReward(FormId, datas))
  }

  return (
    <React.Fragment>
      <div className={classes.paper}>
        <Typography variant="h5" component="h1">
          Edit Daily Rewards
        </Typography>
        {flag ? <form className={classes.form} noValidate>
          <Grid container spacing={3}>
            <Grid item xs={4}>
              <TextField
                variant="outlined"

                required
                fullWidth
                id="rewardsname"
                label="Rewards Name"
                name="rewards_name"
                autoComplete="rewardsname"
                autoFocus
                value={data.rewards_name}
                onChange={onEnterData}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                variant="outlined"

                required
                fullWidth
                name="total_ads"
                label="Total Ads"
                type="text"
                id="totalads"
                autoComplete="totalads"
                value={data.total_ads}
                onChange={onEnterData}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                variant="outlined"

                required
                fullWidth
                name="boost_value"
                label="Boost value"
                type="text"
                id="boostvalue"
                autoComplete="boostvalue"
                value={data.boost_value}
                onChange={onEnterData}
              />
            </Grid>
          </Grid>
          <div style={{ margin: '5px', padding: '5px' }}>Validity</div>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="outlined"
                value={(data.validity == undefined) ? '' : data.validity.value}
                required
                name="timevalue"
                label="Time Value"
                type="text"
                id="timevalue"
                autoComplete="timevalue"

                onChange={onEnterData}
              />
            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth
                variant="outlined">
                <InputLabel>Validity Time</InputLabel>
                <Select
                  value={(data.validity == undefined) ? '' : data.validity.unit}
                  variant="outlined"
                  label="validity Time"
                  required
                  name="validityvalue"
                  type="text"
                  id="time"
                  autoComplete="time"
                  onChange={onEnterData}
                >
                  <MenuItem value=""></MenuItem>
                  <MenuItem value="Day">Day</MenuItem>
                  <MenuItem value="Month">Month</MenuItem>
                  <MenuItem value="Year">Year</MenuItem>
                </Select>
              </FormControl>

            </Grid>
          </Grid>
          <div style={{ margin: '5px', padding: '5px' }}>Total Ads Coin</div>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <TextField
                fullWidth
                variant="outlined"
                value={(data.total_ads_coin == undefined) ? '' : data.total_ads_coin.value}
                required
                name="coinvalue"
                label="Coin Value"
                type="text"
                id="coinvalue"
                autoComplete="coinvalue"
                InputProps={{
                  startAdornment: <InputAdornment position="start">$</InputAdornment>,
                }}

                onChange={onEnterData}
              />

            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth
                variant="outlined">
                <InputLabel>Coin Type</InputLabel>
                <Select
                  value={(data.total_ads_coin == undefined) ? '' : data.total_ads_coin.unit}
                  variant="outlined"
                  label="coin type"
                  required
                  name="cointype"
                  type="text"
                  id="coin type"
                  autoComplete="coin type"
                  onChange={onEnterData}
                >

                  <MenuItem value=""></MenuItem>
                  <MenuItem value="hc">hc</MenuItem>
                </Select>
              </FormControl>
            </Grid>

          </Grid>
          <div style={{ margin: '5px', padding: '5px' }}>Ads Type</div>
          <Grid container spacing={3}>
            <Grid item xs={6} >
              <FormControl fullWidth
                variant="outlined">
                <InputLabel>Ads Type</InputLabel>
                <Select
                  value={(data.ads_type == undefined) ? '' : data.ads_type}
                  variant="outlined"
                  label="Ads type"
                  required
                  name="ads_type"
                  type="text"
                  id="Ads type"
                  autoComplete="Ads type"
                  onChange={onEnterData}
                >
                  <MenuItem value=""></MenuItem>
                  <MenuItem value="Static">Static</MenuItem>
                  <MenuItem value="Dynamic">Dynamic</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => {
                if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                  history.push('/dashboard/dailyrewards')
                } else {
                  history.push(history.location.state.from)
                }
              }}>Back</Button>

            </Grid>
            <Grid item xs={6}>

              <Button type="button" variant="contained" disabled={(btnD || LoadingTrue) ? true : false} fullWidth color="primary" onClick={() => { handleSubmit() }}
              >Submit</Button>
            </Grid>
          </Grid>
        </form> : <Loader />}

      </div>
    </React.Fragment>
  )
}

export default EditDailyRewards
