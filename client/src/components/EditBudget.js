import React, { useEffect, useState } from 'react';
import { Typography, MenuItem, Select, TextField, InputAdornment, makeStyles, FormControl, FormLabel, Button, Grid } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { UpdateBudget, GetAllSchedules } from '../redux/actions';
import Loader from './UsableComponent/Loader';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

var FormId

const EditBudget = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const classes = useStyles()
  const [data, setData] = useState({
    budget: '',
    budgetType: '',
    adSchedule: '',
    adScheduleCT: '',
    adScheduleOT: '',
    startDate: '',
    endDate: ''
  })

  const onChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setBtnD(false)
  }

  const [flag, setFlag] = useState(false)
  const [btnD, setBtnD] = useState(true)
  const AllCampaign = useSelector(state => state.auth.campaignDetail)
  const Updated = useSelector(state => state.auth.updated);
  const AllSchedules = useSelector(state => state.auth.scheduleDetail);
  const LoadingTrue = useSelector(state => state.auth.loader);

  useEffect(() => {

    if (!history.location.state || history.location.state == null || history.location.state == undefined || AllCampaign.length == 0) {
      history.push('/dashboard/campaign')
    } else {
      FormId = history.location.state.id
      AllCampaign.map((cc) => {
        if (cc.budget_id._id == FormId) {
          setData(cc.budget_id)
          setFlag(true)
        }
      })
    }

    if (AllSchedules.length === 0) {
      dispatch(GetAllSchedules(0, 10));
    }

    if (Updated) {
      history.push(history.location.state.from)
    }
    return () => { }
  }, [Updated])

  const handleSubmit = () => {
    const value = {
      budget: data.budget, budgetType: data.budgetType, adSchedule: data.adSchedule, adScheduleCT: data.adScheduleCT, adScheduleOT: data.adScheduleOT, startDate: data.startDate, endDate: data.endDate
    }
    dispatch(UpdateBudget(FormId, value));
  }

  return (
    <React.Fragment>
      <div className={classes.paper}>
        <Typography variant="h5" component="h1">
          Edit Budget Detail
        </Typography>
        {flag ? <form className={classes.form} noValidate>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth
                variant="outlined">
                <FormLabel>Budget</FormLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={(data.budgetType == undefined) ? '' : data.budgetType}
                  onChange={onChange}
                  name="budgetType"
                >
                  <MenuItem value=''></MenuItem>
                  <MenuItem value="Campaign Total">Campaign Total</MenuItem>
                  <MenuItem value="Daily">Daily</MenuItem>
                </Select>
              </FormControl>

            </Grid>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <FormLabel>Amount</FormLabel>
                <TextField
                  id="outlined-start-adornment"
                  name="budget"
                  value={data.budget}
                  onChange={onChange}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                  variant="outlined"
                />
              </FormControl>

            </Grid>
          </Grid>
          {data.budgetType === 'Daily' ? ("") :
            (<React.Fragment>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <FormControl fullWidth>
                    <FormLabel>Start Date</FormLabel>
                    <TextField
                      variant="outlined"
                      type="date"
                      value={data.startDate}
                      onChange={onChange}
                      name="startDate"
                    />
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <FormControl fullWidth>
                    <FormLabel>End Date</FormLabel>
                    <TextField
                      variant="outlined"
                      type="date"
                      value={data.endDate}
                      onChange={onChange}
                      name="endDate"
                    />
                  </FormControl>
                </Grid>
              </Grid>
            </React.Fragment>
            )}
          <Grid container spacing={3}>
            <Grid item xs={4}>
              <FormControl fullWidth
                variant="outlined">
                <FormLabel>Ad Schedule</FormLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={(data.adSchedule == undefined) ? '' : data.adSchedule}
                  onChange={onChange}
                  name="adSchedule"
                >
                  {(AllSchedules.length > 0 || AllSchedules.scheduleData != undefined) ? AllSchedules.scheduleData.map((cc) => {
                    return <MenuItem key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                  }) : null}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <FormControl fullWidth
                variant="outlined">
                <FormLabel>Ad Schedule Start Time</FormLabel>
                <TextField
                  variant="outlined"
                  type="time"
                  value={(data.adScheduleOT == undefined) ? '' : data.adScheduleOT}
                  onChange={onChange}
                  name="adScheduleOT" />
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <FormControl fullWidth
                variant="outlined">
                <FormLabel>Ad Schedule End Time</FormLabel>
                <TextField
                  variant="outlined"
                  type="time"
                  value={(data.adScheduleOT == undefined) ? '' : data.adScheduleOT}
                  onChange={onChange}
                  name="adScheduleOT" />
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => {
                if (!history.location.state || history.location.state == null || history.location.state == undefined) {
                  history.push('/dashboard/campaign')
                } else {
                  history.push(history.location.state.from)
                }
              }}>Back</Button>

            </Grid>
            <Grid item xs={6}>

              <Button type="button" variant="contained" disabled={(btnD || LoadingTrue) ? true : false} fullWidth color="primary" onClick={() => { handleSubmit() }}
              >Submit</Button>
            </Grid>
          </Grid>
        </form> : <Loader />}
      </div>
    </React.Fragment>
  )
}

export default EditBudget