import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { useDispatch, useSelector } from 'react-redux';
import NoData from '../UsableComponent/NoData';
import Loader from '../UsableComponent/Loader';

// New View
import Button from '../../newviews/CustomButtons/Button'
import GridContainer from '../../newviews/Grid/GridContainer';
import GridItem from '../../newviews/Grid/GridItem';
import Card from '../../newviews/Card/Card';
import CardBody from '../../newviews/Card/CardBody';
import CardHeader from '../../newviews/Card/CardHeader';
import { getAllUserStatusList } from '../../redux/actions';
import ButtonComp from '../UsableComponent/ButtonComp';
import useTable from '../UsableComponent/TableSorting/useTable';
import moment from 'moment';
import TblPagination from '../UsableComponent/TableSorting/TblPagination';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const headCells = [
  { id: '', label: '' },
  { id: 'user_id', label: 'User Id' },
  { id: 'name', label: 'Name' },
  { id: 'email', label: 'Email' },
  { id: 'number', label: 'Mobile Number' },
  { id: 'type', label: 'User Type' },
  { id: 'status', label: 'Socket Status' },
  { id: 'createdAt', label: 'createdAt' },
];

const UserSocketStatus = () => {

  const classes = useStyles()
  const dispatch = useDispatch()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const userDataStatus = useSelector((state) => state.auth.userSocketStatus)
  const totalCount = useSelector((state) => state.auth.userSocketTotal)
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== element.length) {
      dispatch(getAllUserStatusList(element.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }


  useEffect(() => {

    if (Object.keys(userDataStatus).length == 0) {
      dispatch(getAllUserStatusList(0, 10))
    } else {
      setElement(userDataStatus.socket_status)
      setFlag(true)
    }

  }, [userDataStatus])

  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(element, headCells, filterFn)

  const handleRefresh = () => {
    dispatch(getAllUserStatusList(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite} >All Online Users</h4>
            <Button color="primary" round onClick={() => { handleRefresh() }}>Refresh</Button>
          </CardHeader>
          <CardBody>
            <TblContainer>
              <TblHead />
              <TableBody>
                {flag ? (element.length > 0) ? recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((cc) => {
                  return <TableRow key={cc._id}>
                    <TableCell></TableCell>
                    <TableCell>{cc.user_id}</TableCell>
                    <TableCell>{(cc.type == 'web') ? (cc.user.length > 0) ? cc.user[0].profile.name : '' : (cc.type == 'mobile') ? (cc.mobile.length > 0) ? cc.mobile[0].name : '' : (cc.type == "admin") ? (cc.admin.length > 0) ? cc.admin[0].role : '' : ''}</TableCell>
                    <TableCell>{(cc.type == 'web') ? (cc.user.length > 0) ? cc.user[0].email : '' : (cc.type == 'mobile') ? (cc.mobile.length > 0) ? cc.mobile[0].email : '' : (cc.type == "admin") ? (cc.admin.length > 0) ? cc.admin[0].username : '' : ''}</TableCell>
                    <TableCell>{(cc.type == 'web') ? (cc.user.length > 0) ? cc.user[0].phone_number : '' : (cc.type == 'mobile') ? (cc.mobile.length > 0) ? cc.mobile[0].number : '' : (cc.type == "admin") ? '' : ''}</TableCell>
                    <TableCell>{cc.type}</TableCell>
                    <TableCell>
                      <ButtonComp value={cc.status} />
                    </TableCell>
                    <TableCell>{moment(cc.createdAt).format('LLLL')}</TableCell>
                  </TableRow>
                }) : <TableRow>
                  <TableCell align="center" colSpan={15}><NoData /></TableCell>
                </TableRow> : <TableRow>
                  <TableCell align="center" colSpan={15}>
                    <Loader />
                  </TableCell>
                </TableRow>}
              </TableBody>
            </TblContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default UserSocketStatus
