import React, { useState } from "react";

export const BackContext = React.createContext()


const LocationContext = props => {
  var [value, setValue] = useState([])

  var handleValue = (data) => {
    console.log('Context Valeus', data)
    console.log('Context Original Valeus', value)
    if (data.length == 0) {

    } else {
      setValue(data)
    }
  }

  return (
    <BackContext.Provider
      value={{
        value, handleValue
      }}
    >
      {props.children}
    </BackContext.Provider>
  );
};

export default LocationContext;


