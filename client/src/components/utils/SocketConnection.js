import React from 'react';
import io from 'socket.io-client';
import { store } from '../../redux';
import { Socket_Connection } from '../../redux/actions';
import BaseUrl from './BaseUrl';

var socket = io({ reconnectionDelayMax: 10000, autoConnect: false });

var socketAuth = () => {
  console.log('request to connect');
  socket = io(BaseUrl, {
    reconnection: false,
    transports: ["websocket"],
    upgrade: false,
    query: 'token=' + localStorage.getItem('auth')
  });
  // socket.on('connect', () => {
  //   console.log('Socket', socket)
  //   console.log('Connected to Server connect')
  // });
  store.dispatch(Socket_Connection(true))
  return socket;
};

export { socket, socketAuth };
var SocketContext = React.createContext({ socket, socketAuth });
export default SocketContext;