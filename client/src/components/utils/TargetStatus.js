export const StatusInput = [
  {
    id: 0,
    name: 'Pending',
    type: 'color0pen'
  },
  {
    id: 1,
    name: 'Approve',
    type: 'color0app'
  },
  {
    id: 2,
    name: 'Enable',
    type: 'color0ena'
  },
  {
    id: 3,
    name: 'Disable',
    type: 'color0dis'
  },
  {
    id: 4,
    name: 'Waiting for Review',
    type: 'color0pen'
  },
  {
    id: 5,
    name: 'Rejected',
    type: 'color0dis'
  }
]

export const ExchangeStatus = {
  Pending: 'Pending',
  Success: 'Success',
  Failed: 'Failed'
}