class Auth {
  constructor() {
    this.authenticated = false
  }

  login(cb) {
    this.authenticated = true;
    cb()
  }

  logout(cb) {
    this.authenticated = false;
    cb()
  }

  isAuthenticated() {
    return this.authenticated
  }

}
const auth = new Auth();

export default auth;


// //store
// import {createStore} from 'redux';


// //init state

// const defaultState = {
//   loggedIn: false,
//   user: {}
// }

// //actions

// const login=()=>{
//   return {
//     type:"login"
//   }
// }

// const logout=()=>{
//   return {
//     type:"logout"
//   }
// }

// const isLoggedIn=()=>{}

// //reducer
// const userReducer = (state = defaultState, action) => {
//   switch(action.type){
//       case "SET_USER":
//           return {
//               loggedIn: true,
//               user: {...action.payload}
//           }
//       case "LOG_OUT":
//           localStorage.clear()
//           return {
//               loggedIn: false,
//               user: {}
//           }
//       default: return state
//   }
// }
//