import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getAllRewards } from '../redux/actions';
import NoData from './UsableComponent/NoData';
import Loader from './UsableComponent/Loader';


// New View
import Button from '../newviews/CustomButtons/Button'
import GridContainer from '../newviews/Grid/GridContainer';
import GridItem from '../newviews/Grid/GridItem';
import Card from '../newviews/Card/Card';
import CardBody from '../newviews/Card/CardBody';
import CardHeader from '../newviews/Card/CardHeader';
import TblPagination from './UsableComponent/TableSorting/TblPagination';


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    maxWidth: '100%',
  },
  media: {
    height: 366,
    cursor: 'pointer',
    backgroundSize: 'contain',
  },
  cardContent: {
    paddingBottom: 0,
  },
  description: {
    marginTop: '0.5em',
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
  table: {
    minWidth: 400,
  },
};

const useStyles = makeStyles(styles)

const Rewards = () => {

  const history = useHistory()
  const classes = useStyles();
  const [rewards, setRewards] = useState([])
  const dispatch = useDispatch();

  const AllRewards = useSelector(state => state.auth.rewardDetail);
  const totalCount = useSelector(state => state.auth.rewardTotal)
  const Create = useSelector(state => state.auth.created)
  const Updated = useSelector(state => state.auth.updated);
  const [flag, setFlag] = useState(false)

  ////  
  const pages = [8, 16, 24]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  const handleChangePage = (event, newPage) => {
    if (newPage >= 1 && totalCount !== rewards.length) {
      dispatch(getAllRewards(rewards.length, 10))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0);
  }

  useEffect(() => {
    if (AllRewards.length === 0) {
      dispatch(getAllRewards(0, 10));
    } else {
      setRewards(AllRewards)
      setFlag(true)
    }
    if (Updated || Create) {
      dispatch(getAllRewards(0, 10));
    }
    return () => { setRewards([]) }
  }, [AllRewards, Updated, Create])


  const handleRefresh = () => {
    dispatch(getAllRewards(0, 10))
  }

  return (
    <GridContainer>
      <GridItem xs={12} md={12} sm={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite} >
              All Rewards
            </h4>
            <Button color="primary" round variant="contained" onClick={() => { handleRefresh() }}>Refresh</Button>
            <Button color="primary" round variant="contained" onClick={() => { history.push('/dashboard/adddailyrewards') }}>Create</Button>
          </CardHeader>
          <CardBody>
            <TableContainer component={Paper}>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell >ID</TableCell>
                    <TableCell>Rewards Name</TableCell>
                    <TableCell>Ads Type</TableCell>
                    <TableCell>Total Ads</TableCell>
                    <TableCell>Boost Value</TableCell>
                    <TableCell>Validity</TableCell>
                    <TableCell>Total Ads Coin</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? (
                    (rewards.length === 0) ? <TableRow>
                      <TableCell align="center" colSpan={15}>
                        <NoData />
                      </TableCell>
                    </TableRow> : rewards.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((datas) => {
                      return (
                        <TableRow key={datas._id} onClick={() => {
                          history.push(`/dashboard/editrewards`, {
                            from: history.location.pathname,
                            id: datas._id
                          })
                        }}>
                          <TableCell component="th" scope="row">{datas._id}</TableCell>
                          <TableCell>{datas.rewards_name}</TableCell>
                          <TableCell>{datas.ads_type}</TableCell>
                          <TableCell>{datas.total_ads}</TableCell>
                          <TableCell>{datas.boost_value}</TableCell>
                          <TableCell>{datas.validity.value + ' ' + datas.validity.unit}</TableCell>
                          <TableCell>{datas.total_ads_coin.value + ' ' + datas.total_ads_coin.unit}</TableCell>
                        </TableRow>
                      )
                    })
                  ) : <TableRow>
                    <TableCell align="center" colSpan={15}>
                      <Loader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            <TblPagination
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={totalCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  )
}

export default Rewards
