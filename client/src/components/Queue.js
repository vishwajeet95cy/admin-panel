import { Button } from '@material-ui/core'
import React from 'react'
import BaseUrl from './utils/BaseUrl'

const Queue = () => {

  return (
    <div>
      <Button href={BaseUrl + 'admin/queues'} target="_blank" color="primary" variant="contained" >Click Me</Button>
    </div>
  )
}

export default Queue
