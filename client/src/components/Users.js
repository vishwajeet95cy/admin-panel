import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import EditButton from '@material-ui/icons/Edit';
import DeleteButton from '@material-ui/icons/Delete';
import UpdateButton from '@material-ui/icons/Update';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import BaseUrl from './utils/BaseUrl';

const useStyles = makeStyles({
  table: {
    minWidth: 150,
  }
});

const Users = () => {
  const classes = useStyles();
  const [user, setUser] = useState([]);
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const userData = () => {
    axios.get(BaseUrl + 'server/admin/getwebuser').then((response) => {
      setUser(response.data.data);
    }).catch((error) => {
      console.log(error)
    })
  }

  useEffect(() => {
    userData()
  }, [])

  const handleEdit = (e, data) => {
    e.preventDefault()
    console.log(data._id);
    setOpen(true)
    console.log("Edit Button Clicked")
  }


  return (
    <div>
      <h1>User Details</h1>
      <TableContainer component={Paper}>
        <Table className={classes.table} size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>Google Id</TableCell>
              <TableCell>Facebook Id</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Photos</TableCell>
              <TableCell>Website</TableCell>
              <TableCell>Kind</TableCell>
              <TableCell>Location</TableCell>
              <TableCell>Operations</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user.map((data, item) => {
              return <TableRow key={data._id}>
                <TableCell component="th" scope="row">
                  {data._id}
                </TableCell>
                <TableCell>{data.google}</TableCell>
                <TableCell>{data.facebook}</TableCell>
                <TableCell >{data.profile.name}</TableCell>
                <TableCell >{data.email}</TableCell>
                <TableCell>{data.profile.photos}</TableCell>
                <TableCell>{data.profile.website}</TableCell>
                <TableCell >{data.tokens[0].kind}</TableCell>
                <TableCell >{data.profile.location}</TableCell>
                <TableCell>
                  <Tooltip title="Edit">
                    <IconButton onClick={(e) => { handleEdit(e, data) }}>
                      <EditButton color="primary"></EditButton>
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Delete">
                    <IconButton onClick={() => { alert("Delete Button Clicked") }}>
                      <DeleteButton color="warning"></DeleteButton>
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Update">
                    <IconButton onClick={() => { alert("Update Button Clicked") }}>
                      <UpdateButton color="secondary"></UpdateButton>
                    </IconButton>
                  </Tooltip>
                </TableCell>
              </TableRow>
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Edit Form</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occasionally.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
          />
          <TextField
            autoFocus
            margin="dense"
            id="website"
            label="Website"
            type="text"
            fullWidth
          />
          <TextField
            autoFocus
            margin="dense"
            id="website"
            label="location"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default Users
