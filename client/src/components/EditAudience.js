import React, { useEffect, useState } from 'react'
import { Grid, TextField, Typography, makeStyles, FormControl, FormLabel, Checkbox, FormControlLabel, Button } from '@material-ui/core'
import Autocomplete from 'react-autocomplete';
import { Close } from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';
import { GetAllAge, GetAllGender, GetAllLanguage, GetAllOccupation, GetAllInterest, SingleAudience, UpdateAudience } from '../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import '../App.css'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}))

const EditAudience = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const param = useParams()
  const classes = useStyles()
  const [input, setInput] = useState(false)
  const [inp, setInp] = useState(false)
  const [occ, setOcc] = useState(false)
  const [data, setData] = useState({})
  const [initial, setInitial] = useState(true)

  const AllInterest = useSelector(state => state.auth.interestDetail);
  const AllOccupation = useSelector(state => state.auth.occupationDetail);
  const AllAge = useSelector(state => state.auth.ageDetail)
  const AllGender = useSelector(state => state.auth.genderDetail);
  const AllLanguage = useSelector(state => state.auth.languageDetail);
  const Singleaudience = useSelector(state => state.auth.audienceDetail);
  const Updated = useSelector(state => state.auth.updated);


  useEffect(() => {

    if (initial) {
      dispatch(SingleAudience(param.id))
      setInitial(false)
    }

    if (AllLanguage.length === 0) {
      dispatch(GetAllLanguage(0, 10));
    }
    if (AllInterest.length === 0) {
      dispatch(GetAllInterest(0, 10));
    }
    if (AllGender.length === 0) {
      dispatch(GetAllGender(0, 10));
    }
    if (AllAge.length === 0) {
      dispatch(GetAllAge(0, 10));
    }
    if (AllOccupation.length === 0) {
      dispatch(GetAllOccupation(0, 10));
    }

    setData(Singleaudience);

    if (Updated) {
      history.push('/dashboard/campaign')
    }

    return () => { setData({}) }
  }, [Singleaudience, Updated])

  const handleChange = (e) => {
    if (e.target.name === "gender") {
      if (e.target.checked) {
        setData({ ...data, gender: data.gender.concat(e.target.value) })
      } else if (!e.target.checked) {
        setData({
          ...data, gender: data.gender.filter((el) => el !== e.target.value)
        })
      }
    } else if (e.target.name === "age") {
      if (e.target.checked) {
        setData({ ...data, age: data.age.concat(e.target.value) })
      } else if (!e.target.checked) {
        setData({
          ...data, age: data.age.filter((el) => el !== e.target.value)
        })
      }

    } else {

      setData({ ...data, [e.target.name]: e.target.value })
    }
    console.log('form', data)
  }

  const onSelectLanguage = value => {
    console.log(value)
    if (
      data.languages.includes(value)
    ) {
      setData({
        ...data,
        language: ''
      });
    } else {
      setData({
        ...data,
        languages: data.languages.concat(value),
        language: ''
      });
    }
    setInput(false);
    console.log('Set Languag', data.languages, data.language)
  };

  const onRemoveLanguage = (e, ind) => {
    setData({
      ...data,
      languages: data.languages.filter((lan, index) => index !== ind)
    });
  };

  const onSelectInterest = value => {
    if (
      data.inventory.includes(value)
    ) {
      setData({
        ...data,
        inventor: ''
      });
    } else {
      setData({
        ...data,
        inventory: data.inventory.concat(value),
        inventor: ''
      });
    }
    setInp(false);
    console.log('Set Inve', data.inventory, data.inventor)
  };

  const onRemoveInventory = (e, ind) => {
    setData({
      ...data,
      inventory: data.inventory.filter((lan, index) => index !== ind)
    });
  };

  const onSelectOccupation = value => {
    console.log(value)
    if (
      data.occupation.includes(value)
    ) {
      setData({
        ...data,
        occupations: ''
      });
    } else {
      setData({
        ...data,
        occupation: data.occupation.concat(value),
        occupations: ''
      });
    }
    setOcc(false);
  };

  const onRemoveOccupation = (e, ind) => {
    setData({
      ...data,
      occupation: data.occupation.filter((lan, index) => index !== ind)
    });
  };

  const handleSubmit = () => {
    const value = {
      location: data.location,
      languages: data.languages,
      age: data.age,
      gender: data.gender,
      inventory: data.inventory,
      occupation: data.occupation
    }
    // console.log('Old Data', data);
    // console.log('New Data', value)
    dispatch(UpdateAudience(param.id, value))
  }

  return (
    <React.Fragment>
      <div className={classes.paper}>
        <Typography variant="h5" component="h1">
          Edit Audience Details
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <FormControl fullWidth
                variant="outlined">
                <FormLabel>Location</FormLabel>
                <TextField value={data.location} variant="outlined" fullWidth name="location" onChange={handleChange} />
              </FormControl>
            </Grid>
            <Grid item xs={6}>

              <label htmlFor="Occupation" style={{ margin: '5px', padding: '5px', fontSize: '15px', marginBottom: '5px' }}>Occupation</label>
              <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <div><label >Select the Occupation</label>
                  <ul className="languages-list">
                    {!data.occupation || data.occupation == null || data.occupation == undefined || data.occupation.length === 0
                      ? ""
                      : data.occupation.map((item, index) => {
                        return AllOccupation.map((data) => {
                          if (item === data._id) {
                            return <li key={index}>
                              {data.name}
                              <IconButton onClick={e =>
                                onRemoveOccupation(e, index)
                              } >
                                <Close />
                              </IconButton>
                            </li>
                          }
                        })
                      })}
                  </ul></div>
                <div><Autocomplete
                  inputProps={{
                    placeholder: "Type your occupation",
                    name: "occupation",
                    id: "occupation",
                    className: "autocomplete",
                    onInput: () => {
                      setOcc(true);
                    },
                    onBlur: () => setOcc(false)
                  }}
                  items={AllOccupation}
                  menuStyle={{
                    boxShadow: "0 0 5px rgba(0,0,0,0.4)"
                  }}
                  renderItem={(item, isHighlighted) => (
                    <div key={item.name}
                      style={{
                        background: isHighlighted
                          ? "#E0E0E0"
                          : "white"
                      }}
                    >
                      {item.name}
                    </div>
                  )}
                  shouldItemRender={(item, value) =>
                    item.name
                      .toLowerCase()
                      .indexOf(value.toLowerCase()) > -1
                  }
                  getItemValue={item => item._id}
                  value={data.occupations}
                  open={occ}
                  onChange={e =>
                    setData({
                      ...data,
                      occupations: e.target.value
                    })
                  }
                  onSelect={value => onSelectOccupation(value)}
                /></div>
              </div>


            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6} >
              <label htmlFor="Language" style={{ margin: '5px', padding: '5px', fontSize: '15px', marginBottom: '5px' }}>Language</label>
              <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <div><label >Select the language your customer speaks</label>
                  <ul className="languages-list">
                    {!data.languages || data.languages == null || data.languages == undefined || data.languages.length === 0
                      ? ""
                      : data.languages.map((item, index) => {
                        return AllLanguage.map((data) => {
                          if (item === data._id) {
                            return <li key={index}>
                              {data.name}
                              <IconButton onClick={e =>
                                onRemoveLanguage(e, index)
                              } >
                                <Close />
                              </IconButton>
                            </li>
                          }
                        })
                      })}
                  </ul></div>
                <div><Autocomplete
                  inputProps={{
                    placeholder: "Type your language",
                    name: "language",
                    id: "language",
                    className: "autocomplete",
                    onInput: () => {
                      setInput(true);
                    },
                    onBlur: () => setInput(false)
                  }}
                  items={AllLanguage}
                  menuStyle={{
                    boxShadow: "0 0 5px rgba(0,0,0,0.4)"
                  }}
                  renderItem={(item, isHighlighted) => (
                    <div key={item.name}
                      style={{
                        background: isHighlighted
                          ? "#E0E0E0"
                          : "white"
                      }}
                    >
                      {item.name}
                    </div>
                  )}
                  shouldItemRender={(item, value) =>
                    item.name
                      .toLowerCase()
                      .indexOf(value.toLowerCase()) > -1
                  }
                  getItemValue={item => item._id}
                  value={data.language}
                  open={input}
                  onChange={e =>
                    setData({
                      ...data,
                      language: e.target.value
                    })
                  }
                  onSelect={value => onSelectLanguage(value)}
                /></div>
              </div>

            </Grid>
            <Grid item xs={6}>
              <label htmlFor="Language" style={{ margin: '5px', padding: '5px', fontSize: '15px', marginBottom: '5px' }}>Inventory</label>
              <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <div><label >Select the inventory</label>
                  <ul className="languages-list">
                    {!data.inventory || data.inventory == null || data.inventory == undefined || data.inventory.length === 0
                      ? ""
                      : data.inventory.map((item, index) => {
                        return AllInterest.map((data) => {
                          if (item === data._id) {
                            return <li key={index}>
                              {data.name}
                              <IconButton onClick={e =>
                                onRemoveInventory(e, index)
                              } >
                                <Close />
                              </IconButton>
                            </li>
                          }
                        })
                      })}
                  </ul></div>
                <div><Autocomplete
                  inputProps={{
                    placeholder: "Type your inventory",
                    name: "inventory",
                    id: "inventory",
                    className: "autocomplete",
                    onInput: () => {
                      setInp(true);
                    },
                    onBlur: () => setInp(false)
                  }}
                  items={AllInterest}
                  menuStyle={{
                    boxShadow: "0 0 5px rgba(0,0,0,0.4)"
                  }}
                  renderItem={(item, isHighlighted) => (
                    <div key={item.name}
                      style={{
                        background: isHighlighted
                          ? "#E0E0E0"
                          : "white"
                      }}
                    >
                      {item.name}
                    </div>
                  )}
                  shouldItemRender={(item, value) =>
                    item.name
                      .toLowerCase()
                      .indexOf(value.toLowerCase()) > -1
                  }
                  getItemValue={item => item._id}
                  value={data.inventor}
                  open={inp}
                  onChange={e =>
                    setData({
                      ...data,
                      inventor: e.target.value
                    })
                  }
                  onSelect={value => onSelectInterest(value)}
                /></div>
              </div>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <div htmlFor="Age" style={{ margin: '5px', padding: '5px', fontSize: '15px', marginBottom: '5px' }}>Age</div>
              {AllAge.map((ag) => {
                return <FormControlLabel key={ag._id}
                  control={
                    <Checkbox
                      onChange={handleChange}
                      checked={(!data.age || data.age == null || data.age == undefined) ? false : (data.age.includes(ag._id)) ? true : false}
                      value={ag._id}
                      name="age"
                      color="primary"
                    />
                  }
                  label={ag.name}
                />
              })}
            </Grid>
            <Grid item xs={6}>
              <div htmlFor="Gender" style={{ margin: '5px', padding: '5px', fontSize: '15px', marginBottom: '5px' }}>Gender</div>
              {AllGender.map((gen) => {
                return <FormControlLabel key={gen._id}
                  control={
                    <Checkbox
                      onChange={handleChange}
                      checked={(!data.gender || data.gender == null || data.gender == undefined) ? false : (data.gender.includes(gen._id)) ? true : false}
                      value={gen._id}
                      name="gender"
                      color="primary"
                    />
                  }
                  label={gen.name}
                />
              })}
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Button type="button" variant="contained" fullWidth color="secondary" onClick={() => { history.push('/dashboard/campaign') }}>Back</Button>

            </Grid>
            <Grid item xs={6}>

              <Button type="button" variant="contained" fullWidth color="primary" onClick={() => { handleSubmit() }}
              >Submit</Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </React.Fragment>
  )
}

export default EditAudience
