import React, { useContext, useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import VideoIcon from '@material-ui/icons/VideoLabel';
import { Link, useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import UserIcon from '@material-ui/icons/MoveToInbox';
import AdIcon from '@material-ui/icons/Notifications';
import HomeIcon from '@material-ui/icons/Home';
import { useDispatch, useSelector } from 'react-redux';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import SettingsIcon from '@material-ui/icons/Settings';
import WcIcon from '@material-ui/icons/Wc';
import StarsIcon from '@material-ui/icons/Stars';
import WorkIcon from '@material-ui/icons/Work';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import AllOutIcon from '@material-ui/icons/AllOut';
import RedeemIcon from '@material-ui/icons/Redeem';
import EditAttributesIcon from '@material-ui/icons/EditAttributes';
import WebIcon from '@material-ui/icons/Web';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';
import ReceiptIcon from '@material-ui/icons/Receipt';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import AndroidIcon from '@material-ui/icons/Android';
import LanguageIcon from '@material-ui/icons/Language';
import PagesIcon from '@material-ui/icons/Pages';
import ViewListIcon from '@material-ui/icons/ViewList';
import SocketContext from './utils/SocketConnection';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
import AssistantIcon from '@material-ui/icons/Assistant';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import CompassCalibrationIcon from '@material-ui/icons/CompassCalibration';
import { Badge } from '@material-ui/core';
import AllInboxIcon from '@material-ui/icons/AllInbox';
import ScheduleIcon from '@material-ui/icons/Schedule';
import CategoryIcon from '@material-ui/icons/Category';
import { AddSharp } from '@material-ui/icons';
import LinkIcon from '@material-ui/icons/Link';
import SquareFootIcon from '@material-ui/icons/SquareFoot';
import FlashOnIcon from '@material-ui/icons/FlashOn';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import BugReportIcon from '@material-ui/icons/BugReport';
import ContactsIcon from '@material-ui/icons/Contacts';
import ApiLoader from './UsableComponent/ApiLoader';
import SwapCallsIcon from '@material-ui/icons/SwapCalls';
import CallSplitIcon from '@material-ui/icons/CallSplit';
import SpeedIcon from '@material-ui/icons/Speed';
import AllInclusiveIcon from '@material-ui/icons/AllInclusive';
import { AdminCount, ConvertVideoUpdate, Socket_Connection, UpdateCallingRank, UpdateDailyRank, Logout, simpleCampaign, requestCampaign, MobileDataUpdate, webUserDataUpdate } from '../redux/actions/index'
import styles from "../assets/jss/material-dashboard-react/layouts/adminStyle.js";


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    grow: {
        flexGrow: 1,
    },
}));

const childrenStyle = makeStyles(styles)

export default function MiniDrawer({ children }) {
    const classes = useStyles();
    const classb = childrenStyle();
    const theme = useTheme();
    const history = useHistory();
    const dispatch = useDispatch()
    const [selectedIndex, setSelectedIndex] = useState("")

    const context = useContext(SocketContext)
    const socketInfo = useSelector(state => state.auth.socket_info)

    useEffect(() => {
        if (context.socket.connected === false) {
            context.socket = context.socketAuth()
        }
        context.socket.on('connect', () => {
            console.log('Connected to Server connect')
        });

        context.socket.on('AdminCount', data => {
            dispatch(AdminCount(data))
        })

        context.socket.on('CallingRank', data => {
            dispatch(UpdateCallingRank(data))
        })

        context.socket.on('DailyRank', data => {
            dispatch(UpdateDailyRank(data))
        })

        context.socket.on('videoDataUpdate', data => {
            //console.log('Request DOne', data)
            dispatch(ConvertVideoUpdate(data))
        })

        context.socket.on('updateCamp', data => {
            dispatch(requestCampaign(data))

        })

        context.socket.on('updateMobile', data => {
            dispatch(MobileDataUpdate(data))
        })

        context.socket.on('updateWeb', data => {
            dispatch(webUserDataUpdate(data))
        })

        context.socket.on('simpleCamp', data => {
            dispatch(simpleCampaign(data))
        })

        context.socket.on('disconnect', () => {
            console.log('Socket', context.socket)
            dispatch(Socket_Connection(false))
            console.log('Disconnect from server')
        });
    }, [])

    const handleClick = index => {
        if (selectedIndex === index) {
            setSelectedIndex("")
        } else {
            setSelectedIndex(index)
        }
    }

    const [open, setOpen] = React.useState(true);

    const logout = () => {
        dispatch(Logout())
        history.push('/')
    }


    const itemsList = [{
        text: "Home",
        icon: <HomeIcon />,
        path: "/dashboard/"
    },
    {
        text: "Users",
        icon: <UserIcon />,
        path: "/dashboard/users",
        children: [
            {
                text: "WebUser",
                icon: <WebIcon />,
                path: "/dashboard/users/webuser"
            },
            {
                text: "MobileUser",
                icon: <PhoneAndroidIcon />,
                path: "/dashboard/users/mobileuser"
            },
            {
                text: "User Status",
                icon: <PhoneAndroidIcon />,
                path: "/dashboard/users/userStatus"
            },
        ]
    },
    {
        text: 'Campaigns',
        icon: <AddSharp />,
        path: '/dashboard/camp',
        children: [
            {
                text: "All Campaign",
                icon: <EditAttributesIcon />,
                path: "/dashboard/campaign"
            },
            {
                text: "Ads",
                icon: <AdIcon />,
                path: "/dashboard/ads"
            },
            {
                text: "Videos",
                icon: <VideoIcon />,
                path: "/dashboard/videos"
            },
            {
                text: "Active Target",
                icon: <TrackChangesIcon />,
                path: "/dashboard/target"
            },
            {
                text: "All Target",
                icon: <TrackChangesIcon />,
                path: "/dashboard/allTarget"
            },
            {
                text: "Landing Page",
                icon: <PagesIcon />,
                path: "/dashboard/landing"
            },
            {
                text: "Stats",
                icon: <ViewListIcon />,
                path: "/dashboard/stats"
            },
            {
                text: "Score",
                icon: <LanguageIcon />,
                path: '/dashboard/score'
            },
            {
                text: "Calling Rewards Rank",
                icon: <TrendingUpIcon />,
                path: '/dashboard/callingrank'
            },
            {
                text: "Daily Rewards Rank",
                icon: <TrendingUpIcon />,
                path: '/dashboard/dailyrank'
            },
        ]
    },
    {
        text: "Settings",
        icon: <SettingsIcon />,
        path: "/dashboard/",
        children: [
            {
                text: 'Request Exchange',
                icon: <AssistantIcon />,
                path: '/dashboard/exchange',
            },
            {
                text: "TransactionHistory",
                icon: <AccountBalanceWalletIcon />,
                path: '/dashboard/history'
            },
            {
                text: "Generate Invoice",
                icon: <ReceiptIcon />,
                path: '/dashboard/invoice'
            },
            {

                text: "Notifications",
                icon: <ReceiptIcon />,
                path: '/dashboard/notify',
            },
            {

                text: "Admin Settings",
                icon: <ReceiptIcon />,
                path: '/dashboard/adminSetting',
            }
        ]
    },
    {
        text: "More",
        icon: <CategoryIcon />,
        path: "/dashboard/ss",
        children: [
            {
                text: "Gender",
                icon: <WcIcon />,
                path: "/dashboard/gender"
            },
            {
                text: "Interest",
                icon: <StarsIcon />,
                path: "/dashboard/interest"
            },
            {
                text: "Occupation",
                icon: <WorkIcon />,
                path: "/dashboard/occupation"
            },
            {
                text: "CampaignType",
                icon: <AcUnitIcon />,
                path: "/dashboard/campaigntype"
            },
            {
                text: "AdsType",
                icon: <AllOutIcon />,
                path: "/dashboard/adstype"
            },
            {
                text: "Rewards",
                icon: <RedeemIcon />,
                path: "/dashboard/dailyrewards"
            },
            {
                text: "Age",
                icon: <AndroidIcon />,
                path: "/dashboard/ages"
            },
            {
                text: "Language",
                icon: <LanguageIcon />,
                path: "/dashboard/language"
            },
            {
                text: "Status",
                icon: <AllInboxIcon />,
                path: '/dashboard/status'
            },
            {
                text: "Schedules",
                icon: <ScheduleIcon />,
                path: '/dashboard/schedules'
            },
            {
                text: "WebUserUpdates",
                icon: <AllInclusiveIcon />,
                path: '/dashboard/webUpdate'
            },
        ]
    }, {
        text: "WebFront",
        icon: <WebAssetIcon />,
        path: "/dashboard/webfront",
        children: [
            {
                text: "Online Marketing",
                icon: <BusinessCenterIcon />,
                path: "/dashboard/online"
            },
            {
                text: "Ads Work",
                icon: <FlashOnIcon />,
                path: "/dashboard/new"
            },
            {
                text: "Network List",
                icon: <LinkIcon />,
                path: "/dashboard/network"
            },
            {
                text: "Footer List",
                icon: <SquareFootIcon />,
                path: "/dashboard/footer"
            },
            {
                text: "Contact Issue List",
                icon: <BugReportIcon />,
                path: "/dashboard/issue"
            },
            {
                text: "User Contact request",
                icon: <ContactsIcon />,
                path: "/dashboard/userContact"
            },
        ]
    }, {
        text: "Indexings",
        icon: <WebAssetIcon />,
        path: "/dashboard/indexi",
        children: [
            {
                text: "Combinations",
                icon: <BusinessCenterIcon />,
                path: "/dashboard/pages"
            },
        ]
    }, {
        text: "Requests Tables",
        icon: <SpeedIcon />,
        path: "/dashboard/rrs",
        children: [
            {
                text: "Calling Rewards Request",
                icon: <SwapCallsIcon />,
                path: "/dashboard/callingTable"
            },
            {
                text: "Daily Rewards Request",
                icon: <CallSplitIcon />,
                path: "/dashboard/dailyTable"
            },
            {
                text: "Campaign Request",
                icon: <AddSharp />,
                path: '/dashboard/campaignTable'
            }
        ]
    }, {
        text: "Queues",
        icon: <SpeedIcon />,
        path: "/dashboard/rrst",
        children: [
            {
                text: "Queues",
                icon: <SwapCallsIcon />,
                path: '/dashboard/queue'
            },
        ]
    }];

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };


    const LoadingTrue = useSelector(state => state.auth.loader);

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap className={classes.title}>
                        HeyCalls Admin
                    </Typography>
                    <div className={classes.grow} />
                    <div className={classes.sectionDesktop}>
                        {(socketInfo) ? <IconButton aria-label="show 17 new notifications" color="inherit">
                            <Badge color="secondary" badgeContent={0}>
                                <CompassCalibrationIcon style={{ color: 'green' }} />
                            </Badge>
                            <Typography style={{ color: 'white' }} variant="h6" noWrap>Online</Typography>
                        </IconButton> : <IconButton aria-label="show 17 new notifications" color="inherit">
                            <Badge color="secondary" badgeContent={0}>
                                <CompassCalibrationIcon style={{ color: 'red' }} />
                            </Badge>
                            <Typography style={{ color: 'red' }} variant="h6" noWrap>Offline</Typography>
                        </IconButton>}
                        <Button
                            type="button"
                            variant="contained"
                            color="primary"
                            onClick={logout}
                        >
                            Logout
                        </Button>
                    </div>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
            >
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </div>
                <Divider />

                <List>
                    {itemsList.map((item, index) => {
                        if (!item.children) {
                            return (

                                <Link key={index} to={item.path} style={{ textDecoration: 'none' }} >
                                    <ListItem>
                                        {item.icon && <ListItemIcon>{item.icon}</ListItemIcon>}
                                        <ListItemText primary={item.text} />
                                    </ListItem>

                                </Link>
                            )
                        }
                        return (
                            <div>
                                <ListItem key={item.text} button onClick={() => { handleClick(item.text) }}>
                                    {item.icon && <ListItemIcon>{item.icon}</ListItemIcon>}
                                    <ListItemText primary={item.text} />
                                    {item.text === selectedIndex ? <ExpandLess /> : <ExpandMore />}
                                </ListItem>
                                <Collapse in={item.text === selectedIndex} timeout="auto" unmountOnExit>
                                    {item.children.map((child, i) => {
                                        return (
                                            <Link key={i} to={child.path} style={{ textDecoration: 'none' }}>
                                                <ListItem>
                                                    {child.icon && <ListItemIcon>{child.icon}</ListItemIcon>}
                                                    <ListItemText primary={child.text} />
                                                </ListItem>
                                            </Link>
                                        )
                                    })}
                                </Collapse>
                            </div>

                        )
                    })}
                </List>
                <Divider />

            </Drawer>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {LoadingTrue ? <ApiLoader /> : null}
                {children}
            </main>

        </div>
    );
}
